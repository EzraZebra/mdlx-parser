# MDLX Parser

A parser for .mdl and .mdx files (WC3 models), reimplemented in C++ from
[GhostWolf's TypeScript code](https://github.com/flowtsohg/mdx-m3-viewer/tree/master/src/parsers/mdlx).