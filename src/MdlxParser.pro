CONFIG -= qt

TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    animatedobject.cpp \
    animation.cpp \
    animation_edit.cpp \
    attachment.cpp \
    attachment_edit.cpp \
    binarystream.cpp \
    bone.cpp \
    camera.cpp \
    camera_edit.cpp \
    collisionshape.cpp \
    collisionshape_edit.cpp \
    errors.cpp \
    eventobject.cpp \
    extent.cpp \
    genericobject.cpp \
    genericobject_edit.cpp \
    geoset.cpp \
    geoset_edit.cpp \
    geosetanimation.cpp \
    geosetanimation_edit.cpp \
    helper.cpp \
    layer.cpp \
    layer_edit.cpp \
    light.cpp \
    light_edit.cpp \
    material.cpp \
    material_edit.cpp \
    model.cpp \
    model_edit.cpp \
    particleemitter.cpp \
    particleemitter2.cpp \
    particleemitter2_edit.cpp \
    particleemitter_edit.cpp \
    particleemitterpopcorn.cpp \
    particleemitterpopcorn_edit.cpp \
    ribbonemitter.cpp \
    sequence.cpp \
    sequence_edit.cpp \
    soundemitter.cpp \
    soundemitter_edit.cpp \
    streambase.cpp \
    texture.cpp \
    texture_edit.cpp \
    textureanimation.cpp \
    textureanimation_edit.cpp \
    tokenstream.cpp \
    unknownchunk.cpp \
    util.cpp

HEADERS += \
    animatedobject.h \
    animatedobject_edit.h \
    animation.h \
    animation_edit.h \
    animationmap.h \
    attachment.h \
    attachment_edit.h \
    binarystream.h \
    binarystream.tcc \
    bone.h \
    bone_edit.h \
    camera.h \
    camera_edit.h \
    collisionshape.h \
    collisionshape_edit.h \
    errors.h \
    eventobject.h \
    eventobject_edit.h \
    extent.h \
    genericobject.h \
    genericobject_edit.h \
    geoset.h \
    geoset_edit.h \
    geosetanimation.h \
    geosetanimation_edit.h \
    globalsequence_edit.h \
    helper.h \
    helper_edit.h \
    layer.h \
    layer_edit.h \
    light.h \
    light_edit.h \
    material.h \
    material_edit.h \
    model.h \
    model_edit.h \
    model_edit.tcc \
    model_options.h \
    model_tags.h \
    particleemitter.h \
    particleemitter2.h \
    particleemitter2_edit.h \
    particleemitter_edit.h \
    particleemitterpopcorn.h \
    particleemitterpopcorn_edit.h \
    ribbonemitter.h \
    ribbonemitter_edit.h \
    sequence.h \
    sequence_edit.h \
    soundemitter.h \
    soundemitter_edit.h \
    streambase.h \
    texture.h \
    texture_edit.h \
    textureanimation.h \
    textureanimation_edit.h \
    tokenstream.h \
    tokenstream.tcc \
    unknownchunk.h \
    util.h \
    util.tcc

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
