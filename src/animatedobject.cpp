#include "animation.h"
#include "animatedobject.h"
#include "animationmap.h"
#include "binarystream.h"
#include "tokenstream.h"

#include "model_edit.h"
#include "animation_edit.h"

namespace mdlx
{
template<class T>
void AnimatedObject::readAnimation(T &stream, uint32_t tag)
{
    switch (animationMap[tag].second) {
    case AnimType::Uint: animations.emplace_back(std::make_shared<Animation<uint32_t>>(Animation<uint32_t>(stream, tag)));
        break;
    case AnimType::Float: animations.emplace_back(std::make_shared<Animation<float>>(Animation<float>(stream, tag)));
        break;
    case AnimType::Vec3: animations.emplace_back(std::make_shared<Animation<vec3>>(Animation<vec3>(stream, tag)));
        break;
    case AnimType::Vec4: animations.emplace_back(std::make_shared<Animation<vec4>>(Animation<vec4>(stream, tag)));
        break;
    default:
        stream.errors.fail(Errors::UnknownTag, "Animation", {
                                static_cast<char>(tag & 0xFF),
                                static_cast<char>((tag >> 8) & 0xFF),
                                static_cast<char>((tag >> 16) & 0xFF),
                                static_cast<char>((tag >> 24) & 0xFF)
                            });
    }
}

template void AnimatedObject::readAnimation(BinaryStream &stream, uint32_t tag);
template void AnimatedObject::readAnimation(TokenStream &stream, uint32_t tag);

AnimatedObject::AnimatedObject(BinaryStream &stream, size_t size)
{
    readAnimations(stream, size);
}

void AnimatedObject::readAnimations(BinaryStream &stream, size_t size)
{
    const size_t end = stream.position() + size;
    while (stream.position() < end) {
        readAnimation(stream, stream.read<uint32_t>());
    }
}

void AnimatedObject::writeAnimations(BinaryStream &stream) const
{
    for (const auto &animation : animations) {
        animation->write(stream);
    }
}

size_t AnimatedObject::getByteLength() const
{
    size_t size = 0;

    for (const auto &animation : animations) {
        size += animation->getByteLength();
    }

    return size;
}

std::string AnimatedObject::iterAnimatedBlock(TokenStream &stream)
{
    std::string token = stream.iterBlock();
    return token == "static" ? "static " + stream.iterBlock() : token;
}

bool AnimatedObject::readAnimation(TokenStream &stream, const std::string &block, const std::string &token)
{
    const auto blockIt = animToMdx.find(block);
    if (blockIt != animToMdx.end()) {
        const auto tagIt = blockIt->second.find(token);
        if (tagIt != blockIt->second.end()) {
            readAnimation(stream, tagIt->second);
            return true;
        }
    }

    return false;
}

bool AnimatedObject::writeAnimation(TokenStream &stream, uint32_t tag) const
{
    for (const auto &animation : animations) {
        if (animation->tag() == tag) {
            animation->write(stream);
            return true;
        }
    }

    return false;
}

AnimatedObject::AnimatedObject(const std::shared_ptr<AnimatedObjectEdit> &animatedObject)
{
    animations.reserve(animatedObject->animations.size());
    for (const auto &animationEdit : animatedObject->animations) {
        switch (animationMap[animationEdit->tag()].second)
        {
        case AnimType::Uint: {
            auto anim = *dynamic_cast<AnimationEdit<uint32_t>*>(animationEdit.get());
            animations.emplace_back(std::make_shared<Animation<uint32_t>>(anim));
            break;
        }
        case AnimType::Float: {
            auto anim = *dynamic_cast<AnimationEdit<float>*>(animationEdit.get());
            animations.emplace_back(std::make_shared<Animation<float>>(anim));
            break;
        }
        case AnimType::Vec3: {
            auto anim = *dynamic_cast<AnimationEdit<vec3>*>(animationEdit.get());
            animations.emplace_back(std::make_shared<Animation<vec3>>(anim));
            break;
        }
        case AnimType::Vec4: {
            auto anim = *dynamic_cast<AnimationEdit<vec4>*>(animationEdit.get());
            animations.emplace_back(std::make_shared<Animation<vec4>>(anim));
            break;
        }
        }
    }
}

animEditVec AnimatedObject::editAnimations(Errors *errors, ModelEdit &model)
{
    animEditVec animationsEdit;

    animationsEdit.reserve(animations.size());
    for (const auto &animation : animations) {
        switch (animationMap[animation->tag()].second)
        {
        case AnimType::Uint: {
           auto animationEdit = dynamic_cast<Animation<uint32_t>*>(animation.get())->edit(errors, model);
           animationsEdit.emplace_back(std::make_shared<AnimationEdit<uint32_t>>(animationEdit));
           break;
        }
        case AnimType::Float: {
           auto animationEdit = dynamic_cast<Animation<float>*>(animation.get())->edit(errors, model);
           animationsEdit.emplace_back(std::make_shared<AnimationEdit<float>>(animationEdit));
           break;
        }
        case AnimType::Vec3: {
           auto animationEdit = dynamic_cast<Animation<vec3>*>(animation.get())->edit(errors, model);
           animationsEdit.emplace_back(std::make_shared<AnimationEdit<vec3>>(animationEdit));
           break;
        }
        case AnimType::Vec4: {
           auto animationEdit = dynamic_cast<Animation<vec4>*>(animation.get())->edit(errors, model);
           animationsEdit.emplace_back(std::make_shared<AnimationEdit<vec4>>(animationEdit));
           break;
        }
        }
    }

    return animationsEdit;
}
}
