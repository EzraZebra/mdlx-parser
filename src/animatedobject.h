#ifndef ANIMATEDOBJECT_H
#define ANIMATEDOBJECT_H

#include <vector>
#include <memory>

namespace mdlx
{
/** Forward declare */
class BinaryStream;
class TokenStream;
class AnimBase;
class ModelEdit;
class AnimEditBase;
class AnimatedObjectEdit;
class Errors;

using animVec = std::vector<std::shared_ptr<AnimBase>>;
using animEditVec = std::vector<std::shared_ptr<AnimEditBase>>;

/**
 * @brief The parent class for all objects that have animated data in them.
 */
class AnimatedObject
{
    template<class T>
    void readAnimation(T &stream, uint32_t tag);

protected:
    animVec animations;

    AnimatedObject() = default;

    AnimatedObject(BinaryStream &stream, size_t size);
    void readAnimations(BinaryStream &stream, size_t size);
    void writeAnimations(BinaryStream &stream) const;
    size_t getByteLength() const;

    /**
     * @brief A wrapper around iterBlock() which merges static tokens.
     * E.g.: static Color
     * This makes the condition blocks in the parent objects linear and simple.
     */
    std::string iterAnimatedBlock(TokenStream &stream);
    bool readAnimation(TokenStream &stream, const std::string &block, const std::string &token);
    bool writeAnimation(TokenStream &stream, uint32_t tag) const;

    AnimatedObject(const std::shared_ptr<AnimatedObjectEdit> &animatedObject);

public:
    animEditVec editAnimations(Errors *errors, ModelEdit &model);
};
}

#endif // ANIMATEDOBJECT_H
