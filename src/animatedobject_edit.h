#ifndef ANIMATEDOBJECT_EDIT_H
#define ANIMATEDOBJECT_EDIT_H

#include "animatedobject.h"

#include <memory>
#include <vector>

namespace mdlx
{
struct AnimatedObjectEdit
{
    animEditVec animations;

    AnimatedObjectEdit(Errors *errors, ModelEdit &model, AnimatedObject *animatedObject)
        : animations(animatedObject->editAnimations(errors, model)),
          errors(errors) {}

protected:
    Errors *errors;
};
}

#endif // ANIMATEDOBJECT_EDIT_H
