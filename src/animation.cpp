#include "animation.h"
#include "animationmap.h"
#include "binarystream.h"
#include "tokenstream.h"

#include "model_edit.h"
#include "animation_edit.h"

namespace mdlx
{
template<typename T>
Track<T>::Track(BinaryStream &stream, uint32_t interpolationType)
    : frame(stream.read<int32_t>()),
      value(stream.read<T>())
{
    if (interpolationType > 1) {
        inTan = stream.read<T>();
        outTan = stream.read<T>();
    }
}

template<typename T>
Track<T>::Track(TokenStream &stream, uint32_t interpolationType)
    : frame(stream.read<int32_t>()),
      value(stream.read<T>())
{
    if (interpolationType > 1) {
        stream.readToken(); // InTan
        inTan = stream.read<T>();
        stream.readToken(); // OutTan
        outTan = stream.read<T>();
    }
}

template<typename T>
bool Track<T>::operator==(const Track<T> &rhs) const
{
    if (frame == rhs.frame) {
        if constexpr (std::is_same<T, float>() || std::is_same<T, vec3>() || std::is_same<T, vec4>()) {
            return compf(value, rhs.value) && compf(inTan, rhs.inTan) && compf(outTan, rhs.outTan);
        }

        return value == rhs.value && inTan == rhs.inTan && outTan == rhs.outTan;
    }

    return false;
}

template class Track<uint32_t>;
template class Track<float>;
template class Track<vec3>;
template class Track<vec4>;

template<typename T>
Animation<T>::Animation(BinaryStream &stream, uint32_t tag)
    : AnimBase(tag)
{
    auto tracksCount = stream.read<uint32_t>();

    interpolationType = stream.read<uint32_t>();
    globalSequenceId = stream.read<int32_t>();

    tracks.reserve(tracksCount);
    for (uint32_t i=0; i<tracksCount; i++) {
        tracks.emplace_back(Track<T>(stream, interpolationType));
    }
}

template<typename T>
void Animation<T>::write(BinaryStream &stream) const
{
    stream.write(tag_);
    stream.write<uint32_t>(tracks.size());
    stream.write(interpolationType);
    stream.write(globalSequenceId);

    for (const auto &track : tracks) {
        stream.write(track.frame);
        stream.write(track.value);

        if (interpolationType > Linear) {
            stream.write(track.inTan);
            stream.write(track.outTan);
        }
    }
}

template<typename T>
size_t Animation<T>::getByteLength() const
{
    return baseByteLength + tracks.size() * (4 + (interpolationType > Linear ? 3 : 1) * byteLength<T>());
}

template<typename T>
Animation<T>::Animation(TokenStream &stream, uint32_t tag)
    : AnimBase(tag)
{
    auto tracksCount = stream.read<uint32_t>();

    stream.readToken(); // {

    std::string token = stream.readToken();
    if (token == "Linear") interpolationType = Linear;
    else if (token == "Hermite") interpolationType = Hermite;
    else if (token == "Bezier") interpolationType = Bezier;
    else interpolationType = DontInterp; // DontInterp

    // GlobalSeqId only exists if it's not -1.
    if (stream.peekToken() == "GlobalSeqId") {
        stream.readToken();
        globalSequenceId = stream.read<int32_t>();
    }

    tracks.reserve(tracksCount);
    for (uint32_t i=0; i<tracksCount; i++) {
        tracks.emplace_back(Track<T>(stream, interpolationType));
    }

    stream.readToken(); // }
}

template<typename T>
void Animation<T>::write(TokenStream &stream) const
{
    stream.startBlock(animationMap[tag_].first, tracks.size());

    if (interpolationType == DontInterp) stream.writeFlag("DontInterp");
    else if (interpolationType == Linear) stream.writeFlag("Linear");
    else if (interpolationType == Hermite) stream.writeFlag("Hermite");
    else if (interpolationType == Bezier) stream.writeFlag("Bezier");

    if (globalSequenceId != -1) stream.writeAttr("GlobalSeqId", globalSequenceId);

    for (const auto &track : tracks) {
        stream.writeAttr(std::to_string(track.frame) + ":", track.value);

        if (interpolationType > Linear) {
            stream.indent();
            stream.writeAttr("InTan", track.inTan);
            stream.writeAttr("OutTan", track.outTan);
            stream.indent(false);
        }
    }

    stream.endBlock();
}

template<typename T>
Animation<T>::Animation(const AnimationEdit<T> &animation)
    : AnimBase(animation.tag())
{
    interpolationType = animation.interpolationType();

    if (animation.globalSequence) {
        globalSequenceId = animation.globalSequence->index;
    }

    tracks = animation.tracks;
}

template<typename T>
AnimationEdit<T> Animation<T>::edit(Errors *errors, ModelEdit &model)
{
    AnimationEdit<T> animation(errors, tag_);

    animation.interpolationType(interpolationType);

    if (globalSequenceId >= 0 && globalSequenceId < static_cast<int32_t>(model.globalSequences.size())) {
        animation.globalSequence = model.globalSequences[globalSequenceId];
    }

    animation.tracks = tracks;

    return animation;
}

template class Animation<uint32_t>;
template class Animation<float>;
template class Animation<vec3>;
template class Animation<vec4>;
}
