#ifndef ANIMATION_H
#define ANIMATION_H

#include <cstdint>
#include <vector>
#include <memory>

namespace mdlx
{
/** Forward declare */
class BinaryStream;
class TokenStream;
class ModelEdit;
template<typename T>
class AnimationEdit;
class Errors;

/**
 * @brief A track.
 */
template<typename T>
struct Track
{
    int32_t frame = 0;
    T       value, inTan, outTan;

    Track(BinaryStream &stream, uint32_t interpolationType);
    Track(TokenStream &stream, uint32_t interpolationType);

    bool operator==(const Track<T> &rhs) const;
    bool operator!=(const Track<T> &rhs) const { return !operator==(rhs); }
};

/**
 * @brief The parent class for animations.
 * This is enables handling animations of anonymous types in AnimatedObject.
 */
struct AnimBase
{
    enum InterpolationType {
        DontInterp  = 0,
        Linear      = 1,
        Hermite     = 2,
        Bezier      = 3,
    };

    AnimBase(uint32_t tag)
        : tag_(tag) {}

    uint32_t tag() const { return tag_; };

    virtual void write(BinaryStream&) const {}
    virtual size_t getByteLength() const { return 0; }

    virtual void write(TokenStream&) const {}

protected:
    static const size_t baseByteLength = 16;

    uint32_t    tag_;
    uint32_t    interpolationType = 0;
    int32_t     globalSequenceId = -1;
};

/**
 * @brief An animation.
 */
template<typename T>
class Animation : public AnimBase
{
public:
    std::vector<Track<T>> tracks;

    Animation(BinaryStream &stream, uint32_t tag_);
    void write(BinaryStream &stream) const override;
    size_t getByteLength() const override;

    Animation(TokenStream &stream, uint32_t tag_);
    void write(TokenStream &stream) const override;

    Animation(const AnimationEdit<T> &animation);
    AnimationEdit<T> edit(Errors *errors, ModelEdit &model);
};
}

#endif // ANIMATION_H
