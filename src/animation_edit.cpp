#include "animation_edit.h"
#include "animation.h"
#include "animationmap.h"
#include "errors.h"
#include "util.h"

namespace mdlx
{
bool AnimEditBase::operator==(const AnimEditBase &rhs) const
{
    if (tag_ == rhs.tag() && globalSequence == rhs.globalSequence && interpolationType_ == rhs.interpolationType()) {
        switch (animationMap[tag_].second)
        {
        case AnimType::Uint:
            return dynamic_cast<const AnimationEdit<uint32_t>*>(this)->tracks == dynamic_cast<const AnimationEdit<uint32_t>&>(rhs).tracks;
        case AnimType::Float:
            return dynamic_cast<const AnimationEdit<float>*>(this)->tracks == dynamic_cast<const AnimationEdit<float>&>(rhs).tracks;
        case AnimType::Vec3:
            return dynamic_cast<const AnimationEdit<vec3>*>(this)->tracks == dynamic_cast<const AnimationEdit<vec3>&>(rhs).tracks;
        case AnimType::Vec4:
            return dynamic_cast<const AnimationEdit<vec4>*>(this)->tracks == dynamic_cast<const AnimationEdit<vec4>&>(rhs).tracks;
        }
    }

    return false;
}

bool operator==(const std::vector<std::shared_ptr<AnimEditBase>> &lhs, const std::vector<std::shared_ptr<AnimEditBase>> &rhs)
{
    if (lhs.size() == rhs.size()) {
        for (size_t i=0; i<lhs.size(); i++) {
            if (*lhs[i] != *rhs[i]) {
                return false;
            }
        }

        return true;
    }

    return false;
}

bool operator!=(const std::vector<std::shared_ptr<AnimEditBase>> &lhs, const std::vector<std::shared_ptr<AnimEditBase>> &rhs)
{
    return !operator==(lhs, rhs);
}

bool AnimEditBase::interpolationType(uint32_t type)
{
    switch (type)
    {
    case AnimBase::DontInterp: case AnimBase::Linear: case AnimBase::Hermite: case AnimBase::Bezier:
        interpolationType_ = type;
        return true;
    default:
        errors->add(Errors::Value, "Interpolation Type", std::to_string(type));
        return false;
    }
}

template<typename T>
bool AnimationEdit<T>::operator==(const AnimationEdit<T> &rhs) const
{
    return  tag_ == rhs.tag() && interpolationType_ == rhs.interpolationType() && globalSequence == rhs.globalSequence
            && tracks == rhs.tracks;
}

template class AnimationEdit<uint32_t>;
template class AnimationEdit<float>;
template class AnimationEdit<vec3>;
template class AnimationEdit<vec4>;
}
