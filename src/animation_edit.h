#ifndef ANIMATIONEDIT_H
#define ANIMATIONEDIT_H

#include "util.h"

#include <memory>

namespace mdlx
{
/** Forward declare */
class Errors;
template<typename T>
class Track;
class GlobalSequenceEdit;

struct AnimEditBase
{
    std::shared_ptr<GlobalSequenceEdit> globalSequence;

    virtual uint32_t    tag() const { return tag_; }
    virtual uint32_t    interpolationType() const { return interpolationType_; }
    virtual bool        interpolationType(uint32_t type);

    AnimEditBase(Errors *errors, uint32_t tag)
        : errors(errors), tag_(tag) {}

    bool operator==(const AnimEditBase &rhs) const;
    bool operator!=(const AnimEditBase &rhs) const { return !operator==(rhs); }

protected:
    Errors *errors;

    uint32_t    tag_;
    uint32_t    interpolationType_ = 0;
};

extern bool operator==(const std::vector<std::shared_ptr<AnimEditBase>> &lhs, const std::vector<std::shared_ptr<AnimEditBase>> &rhs);
extern bool operator!=(const std::vector<std::shared_ptr<AnimEditBase>> &lhs, const std::vector<std::shared_ptr<AnimEditBase>> &rhs);

template<typename T>
struct AnimationEdit : public AnimEditBase
{
    std::vector<Track<T>> tracks;

    AnimationEdit(Errors *errors, uint32_t tag)
        : AnimEditBase(errors, tag) {}

    bool operator==(const AnimationEdit<T> &rhs) const;
    bool operator!=(const AnimationEdit<T> &rhs) const { return !operator==(rhs); }
};
}

#endif // ANIMATIONEDIT_H
