#ifndef ANIMATIONMAP_H
#define ANIMATIONMAP_H

#include <unordered_map>

namespace mdlx
{
/** Layer */
const uint32_t KMTF = 0x46544d4b;
const uint32_t KMTA = 0x41544d4b;
const uint32_t KMTE = 0x45544d4b; // EmissiveGain 900
const uint32_t KFC3 = 0x3343464b; // Fresnel 1000
const uint32_t KFCA = 0x4143464b; // "
const uint32_t KFTC = 0x4354464b; // "
/** TextureAnimation */
const uint32_t KTAT = 0x5441544b;
const uint32_t KTAR = 0x5241544b;
const uint32_t KTAS = 0x5341544b;
/** GeosetAnimation */
const uint32_t KGAO = 0x4f41474b;
const uint32_t KGAC = 0x4341474b;
/** GenericObject */
const uint32_t KGTR = 0x5254474b;
const uint32_t KGRT = 0x5452474b;
const uint32_t KGSC = 0x4353474b;
/** Light */
const uint32_t KLAS = 0x53414c4b;
const uint32_t KLAE = 0x45414c4b;
const uint32_t KLAC = 0x43414c4b;
const uint32_t KLAI = 0x49414c4b;
const uint32_t KLBI = 0x49424c4b;
const uint32_t KLBC = 0x43424c4b;
const uint32_t KLAV = 0x56414c4b;
/** Attachment */
const uint32_t KATV = 0x5654414b;
/** ParticleEmitter */
const uint32_t KPEE = 0x4545504b;
const uint32_t KPEG = 0x4745504b;
const uint32_t KPLN = 0x4e4c504b;
const uint32_t KPLT = 0x544c504b;
const uint32_t KPEL = 0x4c45504b;
const uint32_t KPES = 0x5345504b;
const uint32_t KPEV = 0x5645504b;
/** ParticleEmitter2 */
const uint32_t KP2S = 0x5332504b;
const uint32_t KP2R = 0x5232504b;
const uint32_t KP2L = 0x4c32504b;
const uint32_t KP2G = 0x4732504b;
const uint32_t KP2E = 0x4532504b;
const uint32_t KP2N = 0x4e32504b;
const uint32_t KP2W = 0x5732504b;
const uint32_t KP2V = 0x5632504b;
/** RibbonEmitter */
const uint32_t KRHA = 0x4148524b;
const uint32_t KRHB = 0x4248524b;
const uint32_t KRAL = 0x4c41524b;
const uint32_t KRCO = 0x4f43524b;
const uint32_t KRTX = 0x5854524b;
const uint32_t KRVS = 0x5356524b;
/** Camera */
const uint32_t KCTR = 0x5254434b;
const uint32_t KTTR = 0x5254544b;
const uint32_t KCRL = 0x4c52434b;
/** ParticleEmitterPopcorn */
const uint32_t KPPA = 0x4354464b;
const uint32_t KPPC = 0x4350504b;
const uint32_t KPPE = 0x4550504b;
const uint32_t KPPL = 0x4c50504b;
const uint32_t KPPS = 0x5350504b;
const uint32_t KPPV = 0x5650504b;

enum AnimType { Uint, Float, Vec3, Vec4 };

// A map from MDX animation tags to the animation type.
static std::unordered_map<uint32_t, std::pair<std::string, AnimType>> animationMap = {
    /** Layer */
    { KMTF, { "TextureID", AnimType::Uint } }, { KMTA, { "Alpha", AnimType::Float } },
    { KMTE, { "EmissiveGain", AnimType::Float } }, { KFC3, { "FresnelColor", AnimType::Vec3 } },
    { KFCA, { "FresnelOpacity", AnimType::Float } }, { KFTC, { "FresnelTeamColor", AnimType::Float } },
    /** TextureAnimation */
    { KTAT, { "Translation", AnimType::Vec3 } }, { KTAR, { "Rotation", AnimType::Vec4 } }, { KTAS, { "Scaling", AnimType::Vec3 } },
    /** GeosetAnimation */
    { KGAO, { "Alpha", AnimType::Float } }, { KGAC, { "Color", AnimType::Vec3 } },
    /** GenericObject */
    { KGTR, { "Translation", AnimType::Vec3 } }, { KGRT, { "Rotation", AnimType::Vec4 } }, { KGSC, { "Scaling", AnimType::Vec3 } },
    /** Light */
    { KLAS, { "AttenuationStart", AnimType::Float } }, { KLAE, { "AttenuationEnd", AnimType::Float } },
    { KLAC, { "Color", AnimType::Vec3 } }, { KLAI, { "Intensity", AnimType::Float } },
    { KLBI, { "AmbIntensity", AnimType::Float } }, { KLBC, { "AmbColor", AnimType::Vec3 } }, { KLAV, { "Visibility", AnimType::Float } },
    /** Attachment */
    { KATV, { "Visibility", AnimType::Float } },
    /** ParticleEmitter */
    { KPEE, { "EmissionRate", AnimType::Float } }, { KPEG, { "Gravity", AnimType::Float } },
    { KPLN, { "Longitude", AnimType::Float } }, { KPLT, { "Latitude", AnimType::Float } },
    { KPEL, { "LifeSpan", AnimType::Float } }, { KPES, { "InitVelocity", AnimType::Float } }, { KPEV, { "Visibility", AnimType::Float } },
    /** ParticleEmitter2 */
    { KP2S, { "Speed", AnimType::Float } }, { KP2R, { "Variation", AnimType::Float } }, { KP2L, { "Latitude", AnimType::Float } },
    { KP2G, { "Gravity", AnimType::Float } }, { KP2E, { "EmissionRate", AnimType::Float } }, { KP2N, { "Width", AnimType::Float } },
    { KP2W, { "Length", AnimType::Float } }, { KP2V, { "Visibility", AnimType::Float } },
    /** ParticleEmitterPopcorn */
    { KPPA, { "Alpha", AnimType::Float } }, { KPPC, { "Color", AnimType::Vec3 } }, { KPPE, { "EmissionRate", AnimType::Float } },
    { KPPL, { "LifeSpan", AnimType::Float } }, { KPPS, { "Speed", AnimType::Float } }, { KPPV, { "Visibility", AnimType::Float } },
    /** RibbonEmitter */
    { KRHA, { "HeightAbove", AnimType::Float } }, { KRHB, { "HeightBelow", AnimType::Float } }, { KRAL, { "Alpha", AnimType::Float } },
    { KRCO, { "Color", AnimType::Vec3 } }, { KRTX, { "TextureSlot", AnimType::Uint } }, { KRVS, { "Visibility", AnimType::Float } },
    /** Camera */
    { KCTR, { "Translation", AnimType::Vec3 } }, { KTTR, { "Translation", AnimType::Vec3 } }, { KCRL, { "Rotation", AnimType::Uint } },
};

// A map from animation MDL tokens to MDX tags.
static std::unordered_map<std::string, std::unordered_map<std::string, uint32_t>> animToMdx = {
    { "Layer", {
          { "TextureID", KMTF }, { "Alpha", KMTA }, { "EmissiveGain", KMTE }, { "Emissive", KMTE }, // "Emissive": RMS 0.4
          { "FresnelColor", KFC3 }, { "FresnelOpacity", KFCA }, { "FresnelTeamColor", KFTC },
      } },
    { "TextureAnimation", {
          { "Translation", KTAT }, { "Rotation", KTAR }, { "Scaling", KTAS },
      } },
    { "GeosetAnimation", {
          { "Alpha", KGAO }, { "Color", KGAC },
      } },
    { "GenericObject", {
          { "Translation", KGTR }, { "Rotation", KGRT }, { "Scaling", KGSC },
      } },
    { "Light", {
          { "AttenuationStart", KLAS }, { "AttenuationEnd", KLAE }, { "Color", KLAC },
          { "Intensity", KLAI }, { "AmbIntensity", KLBI }, { "AmbColor", KLBC }, { "Visibility", KLAV },
      } },
    { "Attachment", {
          { "Visibility", KATV },
      } },
    { "ParticleEmitter", {
          { "EmissionRate", KPEE }, { "Gravity", KPEG }, { "Longitude", KPLN }, { "Latitude", KPLT },
          { "LifeSpan", KPEL }, { "InitVelocity", KPES }, { "Visibility", KPEV },
      } },
    { "ParticleEmitter2", {
          { "Speed", KP2S }, { "Variation", KP2R }, { "Latitude", KP2L },  { "Gravity", KP2G },
          { "EmissionRate", KP2E }, { "Width", KP2N }, { "Length", KP2W }, { "Visibility", KP2V },
      } },
    { "ParticleEmitterPopcorn", {
          { "Alpha", KPPA }, { "Color", KPPC }, { "EmissionRate", KPPE },
          { "LifeSpan", KPPL }, { "Speed", KPPS }, { "Visibility", KPPV },
      } },
    { "RibbonEmitter", {
          { "HeightAbove", KRHA }, { "HeightBelow", KRHB }, { "Alpha", KRAL },
          { "Color", KRCO }, { "TextureSlot", KRTX }, { "Visibility", KRVS },
      } },
    { "Camera", {                   /** \/ Token is just "Translation", add "Target" to differentiate */
          { "Translation", KCTR }, { "TargetTranslation", KTTR }, { "Rotation", KCRL },
      } },
};
}

#endif // ANIMATIONMAP_H
