#include "attachment.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "animationmap.h"

#include "model_edit.h"

namespace mdlx
{
Attachment::Attachment(BinaryStream &stream)
    : GenericObject(ObjectFlags::Attachment)
{
    const size_t start = stream.position();
    const size_t size = stream.read<uint32_t>();

    read(stream);

    path = stream.readString(strSize);
    attachmentId = stream.read<int32_t>();

    readAnimations(stream, size - (stream.position() - start));
}

void Attachment::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength());

    GenericObject::write(stream);

    stream.writeString(path);
    stream.skip(strSize - path.length());
    stream.write(attachmentId);

    writeNonGenericAnimationChunks(stream);
}

size_t Attachment::getByteLength(uint32_t) const
{
    return baseByteLength + GenericObject::getByteLength();
}

Attachment::Attachment(TokenStream &stream)
    : GenericObject(ObjectFlags::Attachment)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        if (token == "AttachmentID") attachmentId = stream.read<int32_t>();
        else if (token == "Path") path = stream.read<std::string>();
        else if (!readAnimation(stream, "Attachment", token)) {
            stream.errors.fail(Errors::UnknownToken, "Attachment " + name, token);
        }
    }
}

void Attachment::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "Attachment");

    stream.writeAttr("AttachmentID", attachmentId); // Is this needed? MDX supplies it, but MdlxConv does not use it.
    if (!path.empty()) stream.writeAttr("Path", path);
    writeAnimation(stream, KATV);

    writeGenericAnimations(stream);
    stream.endBlock();
}

Attachment::Attachment(const std::shared_ptr<AttachmentEdit> &attachment)
    : GenericObject(attachment)
{
    path = attachment->path();
    attachmentId = attachment->attachmentId;
}

std::shared_ptr<AttachmentEdit> Attachment::edit(Errors *errors, ModelEdit &model)
{
    auto attachment = std::make_shared<AttachmentEdit>(GenericObject::edit(errors, model, this));

    attachment->path(path);
    attachment->attachmentId = attachmentId;

    return attachment;
}
}
