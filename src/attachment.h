#ifndef ATTACHMENT_H
#define ATTACHMENT_H

#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
struct AttachmentEdit;

/**
 * @brief An attachment.
 */
class Attachment : public GenericObject
{
    static const size_t baseByteLength = 268;

    std::string path;
    int32_t     attachmentId = 0;

public:
    Attachment(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    Attachment(TokenStream &stream);
    void write(TokenStream &stream) const;

    Attachment(const std::shared_ptr<AttachmentEdit> &attachment);
    std::shared_ptr<AttachmentEdit> edit(Errors *errors, ModelEdit &model);
};
}

#endif // ATTACHMENT_H
