#include "attachment_edit.h"
#include "errors.h"
#include "util.h"

namespace mdlx
{
bool AttachmentEdit::path(const std::string &path)
{
    if (path.length() <= strSize) {
        path_ = path;
        return true;
    }

    errors->add(Errors::Value, "Attachment Path", path);
    return false;
}
}
