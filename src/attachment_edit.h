#ifndef ATTACHMENTEDIT_H
#define ATTACHMENTEDIT_H

#include "genericobject_edit.h"

namespace mdlx
{
struct AttachmentEdit : public GenericObjectEdit
{
    std::string path() const { return path_; }
    bool        path(const std::string &path);
    int32_t     attachmentId = 0;

    AttachmentEdit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}

private:
    std::string path_;
};
}

#endif // ATTACHMENTEDIT_H
