#include "binarystream.h"

namespace mdlx
{
void BinaryStream::skip(const size_t size)
{
    if (position_ + size > buffer.size()) {
        errors.fail(Errors::Overflow, "StreamBase::skip()");
    }

    position_ += size;
}

BinaryStream::BinaryStream(const std::string &path, Errors &errors)
    : StreamBase(path, errors) {}

uint32_t BinaryStream::peek()
{
    const size_t start = position_;
    const auto ret  = read<uint32_t>();
    position_ = start;
    return ret;
}

std::string BinaryStream::readString(const size_t size)
{
    if (position_ + size > buffer.size()) {
        errors.fail(Errors::Overflow,  "BinaryStream::readString()");
    }

    std::string result = { reinterpret_cast<char*>(&buffer[position_]), static_cast<size_t>(size) };

    const size_t end = result.find_first_of('\0', 0);

    if (end != std::string::npos) {
        result.resize(end);
    }

    position_ += size;

    return result;
}

BinaryStream::BinaryStream(size_t size, Errors &errors, uint32_t version, int options)
    : StreamBase(size, errors, version, options) {}

void BinaryStream::writeString(const std::string &str)
{
    const size_t size = str.length();

    if (position_ + size > buffer.size()) {
        errors.fail(Errors::Overflow, "BinaryStream::write()");
    }

    std::copy(str.c_str(), str.c_str()+size, &buffer[position_]);

    position_ += size;
}
}
