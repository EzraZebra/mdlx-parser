#ifndef BINARYSTREAM_H
#define BINARYSTREAM_H

#include "streambase.h"

namespace mdlx
{
/**
 * @brief A binary stream.
 */
class BinaryStream : public StreamBase
{
public:
    void skip(const size_t size=4);

    BinaryStream(const std::string &path, Errors &errors);

    uint32_t peek();

    std::string readString(const size_t size);
    template<typename T>
    T read();
    template<typename T>
    std::vector<T> readVector(const size_t size);

    BinaryStream(size_t size, Errors &errors, uint32_t version, int options = 0);

    void writeString(const std::string &str);
    template<typename T>
    void write(T val);
    template<typename T>
    void writeVector(std::vector<T> vect);
};
}

#include "binarystream.tcc"

#endif // BINARYSTREAM_H
