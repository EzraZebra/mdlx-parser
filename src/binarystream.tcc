#ifndef BINARYSTREAM_TCC
#define BINARYSTREAM_TCC

#include "util.h"
#include "errors.h"
#include "binarystream.h"

namespace mdlx
{
template<typename T>
T BinaryStream::read()
{
     const size_t bLength = byteLength<T>();

    if (position_ + bLength > buffer.size()) {
        errors.fail(Errors::Overflow, "BinaryStream::read()");
    }

    T value = *reinterpret_cast<T*>(&buffer[position_]);

    position_ += bLength;

    return value;
}

template<typename T>
std::vector<T> BinaryStream::readVector(const size_t size)
{
    const size_t bLength = byteLength<T>() * size;

    if (position_ + bLength > buffer.size()) {
        errors.fail(Errors::Overflow, "BinaryStream::readVector()");
    }

    std::vector<T> result(reinterpret_cast<T*>(&buffer[position_]), reinterpret_cast<T*>(&buffer[position_]) + size);

    position_ += bLength;

    return result;
}

template<typename T>
void BinaryStream::write(T val)
{
    const size_t bLength = byteLength<T>();

    if (position_ + bLength > buffer.size()) {
        errors.fail(Errors::Overflow, "BinaryStream::write()");
    }

    std::copy(reinterpret_cast<char*>(&val), reinterpret_cast<char*>(&val)+bLength, &buffer[position_]);

    position_ += bLength;
}

template<typename T>
void BinaryStream::writeVector(std::vector<T> vect)
{
    const size_t bLength = vectorByteLength(vect);

    if (position_ + bLength > buffer.size()) {
        errors.fail(Errors::Overflow, "BinaryStream::writeVector()");
    }

    std::copy(reinterpret_cast<char*>(vect.data()), reinterpret_cast<char*>(vect.data()+vect.size()), &buffer[position_]);

    position_ += bLength;
}
}

#endif // BINARYSTREAM_TCC
