#include "bone.h"
#include "binarystream.h"
#include "tokenstream.h"

#include "model_edit.h"

namespace mdlx
{
Bone::Bone(BinaryStream &stream)
    : GenericObject(stream, ObjectFlags::Bone),
      geosetId(stream.read<int32_t>()),
      geosetAnimationId(stream.read<int32_t>()) { }

void Bone::write(BinaryStream &stream) const
{
    GenericObject::write(stream);

    stream.write(geosetId);
    stream.write(geosetAnimationId);
}

size_t Bone::getByteLength(uint32_t) const
{
    return baseByteLength + GenericObject::getByteLength();
}

Bone::Bone(TokenStream &stream)
    : GenericObject(ObjectFlags::Bone)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        if (token == "GeosetId") {
            if (stream.peekToken() == "Multiple") {
                stream.readToken();
                geosetId = -1;
            }
            else geosetId = stream.read<int32_t>();
        }
        else if (token == "GeosetAnimId") {
            if (stream.peekToken() == "None") {
                stream.readToken();
                geosetAnimationId = -1;
            }
            else geosetAnimationId = stream.read<int32_t>();
        }
        else stream.errors.fail(Errors::UnknownToken, "Bone " + name, token);
    }
}

void Bone::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "Bone");

    if (geosetId == -1) stream.writeFlag("GeosetId", "Multiple");
    else stream.writeAttr("GeosetId", geosetId);

    if (geosetAnimationId == -1) stream.writeFlag("GeosetAnimId", "None");
    else stream.writeAttr("GeosetAnimId", geosetAnimationId);

    writeGenericAnimations(stream);
    stream.endBlock();
}

Bone::Bone(const std::shared_ptr<BoneEdit> &bone)
    : GenericObject(bone)
{
    if (bone->geoset) {
        geosetId = bone->geoset->index;
    }

    if (bone->geosetAnimation) {
        geosetAnimationId = bone->geosetAnimation->index;
    }
}

std::shared_ptr<BoneEdit> Bone::edit(Errors *errors, ModelEdit &model)
{
    auto bone = std::make_shared<BoneEdit>(GenericObject::edit(errors, model, this));

    if (geosetId > 0 && geosetId < static_cast<int32_t>(model.geosets.size())) {
        bone->geoset = model.geosets[geosetId];
    }

    if (geosetAnimationId > 0 && geosetAnimationId < static_cast<int32_t>(model.geosetAnimations.size())) {
        bone->geosetAnimation = model.geosetAnimations[geosetAnimationId];
    }

    return bone;
}
}
