#ifndef BONE_H
#define BONE_H

#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
class BoneEdit;

/**
 * @brief A bone.
 */
class Bone : public GenericObject
{
    static const size_t baseByteLength = 8;

    int32_t geosetId = -1, geosetAnimationId = -1;

public:
    Bone(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    Bone(TokenStream &stream);
    void write(TokenStream &stream) const;

    Bone(const std::shared_ptr<BoneEdit> &bone);
    std::shared_ptr<BoneEdit> edit(Errors *errors, ModelEdit &model);
};
}

#endif // BONE_H
