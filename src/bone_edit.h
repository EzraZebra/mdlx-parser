#ifndef BONE_EDIT_H
#define BONE_EDIT_H

#include "genericobject_edit.h"

namespace mdlx
{
/** Forward declare */
class GeosetEdit;
class GeosetAnimationEdit;

struct BoneEdit : public GenericObjectEdit
{
    std::shared_ptr<GeosetEdit>          geoset;
    std::shared_ptr<GeosetAnimationEdit> geosetAnimation;

    BoneEdit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}
};

}

#endif // BONE_EDIT_H
