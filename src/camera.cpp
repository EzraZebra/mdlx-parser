#include "camera.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "animationmap.h"

#include "model_edit.h"

namespace mdlx
{
Camera::Camera(BinaryStream &stream)
{
    size_t size = stream.read<uint32_t>();

    name = stream.readString(strSizeSmall);
    position = stream.read<vec3>();
    fieldOfView = stream.read<float>();
    farClippingPlane = stream.read<float>();
    nearClippingPlane = stream.read<float>();
    targetPosition = stream.read<vec3>();

    readAnimations(stream, size-baseByteLength);
}

void Camera::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength());

    stream.writeString(name);
    stream.skip(strSizeSmall - name.length());
    stream.write(position);
    stream.write(fieldOfView);
    stream.write(farClippingPlane);
    stream.write(nearClippingPlane);
    stream.write(targetPosition);

    writeAnimations(stream);
}

size_t Camera::getByteLength(uint32_t) const
{
    return baseByteLength + AnimatedObject::getByteLength();
}

Camera::Camera(TokenStream &stream)
{
    name = stream.read<std::string>();

    for (std::string token; !(token = stream.iterBlock()).empty(); ) {
        if (token == "Position") position = stream.read<vec3>();
        else if (token == "FieldOfView") fieldOfView = stream.read<float>();
        else if (token == "FarClip") farClippingPlane = stream.read<float>();
        else if (token == "NearClip") nearClippingPlane = stream.read<float>();
        else if (token == "Target") {
            while (!(token = stream.iterBlock()).empty()) {
                if (token == "Position") targetPosition = stream.read<vec3>();
                else if (token == "Translation") readAnimation(stream, "Camera", "TargetTranslation");
                else stream.errors.fail(Errors::UnknownToken, "Camera " + name + " Target", token);
            }
        }
        else if (!readAnimation(stream, "Camera", token)) {
            stream.errors.fail(Errors::UnknownToken, "Camera " + name, token);
        }
    }
}

void Camera::write(TokenStream &stream) const
{
    stream.startBlock("Camera", name);

    stream.writeAttr("Position", position);
    writeAnimation(stream, KCTR);
    writeAnimation(stream, KCRL);
    stream.writeAttr("FieldOfView", fieldOfView);
    stream.writeAttr("FarClip", farClippingPlane);
    stream.writeAttr("NearClip", nearClippingPlane);

    stream.startBlock("Target");
    stream.writeAttr("Position", targetPosition);
    writeAnimation(stream, KTTR);
    stream.endBlock();

    stream.endBlock();
}

Camera::Camera(const std::shared_ptr<CameraEdit> &camera)
    : AnimatedObject(camera)
{
    name = camera->name();
    position = camera->position;
    fieldOfView = camera->fieldOfView;
    farClippingPlane = camera->farClippingPlane;
    nearClippingPlane = camera->nearClippingPlane;
    targetPosition = camera->targetPosition;
}

std::shared_ptr<CameraEdit> Camera::edit(Errors *errors, ModelEdit &model)
{
    auto camera = std::make_shared<CameraEdit>(errors, model, this);

    camera->name(name);
    camera->position = position;
    camera->fieldOfView = fieldOfView;
    camera->farClippingPlane = farClippingPlane;
    camera->nearClippingPlane = nearClippingPlane;
    camera->targetPosition = targetPosition;

    return camera;
}
}
