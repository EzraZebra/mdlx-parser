#ifndef CAMERA_H
#define CAMERA_H

#include "util.h"
#include "animatedobject.h"

namespace mdlx
{
/** Forward declare */
struct CameraEdit;
/**
 * @brief A camera.
 */
class Camera : public AnimatedObject
{
    static const size_t baseByteLength = 120;

    std::string name;
    vec3        position{};
    float       fieldOfView = 0.0F, farClippingPlane = 0.0F, nearClippingPlane = 0.0F;
    vec3        targetPosition{};

public:
    Camera(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    Camera(TokenStream &stream);
    void write(TokenStream &stream) const;

    Camera(const std::shared_ptr<CameraEdit> &textureAnimation);
    std::shared_ptr<CameraEdit> edit(Errors* errors, ModelEdit &model);
};
}

#endif // CAMERA_H
