#include "camera_edit.h"
#include "errors.h"

namespace mdlx
{
bool CameraEdit::name(const std::string &name)
{
    if (name.length() <= strSizeSmall) {
        name_ = name;
        return true;
    }

    errors->add(Errors::Value, "Camera Name", name);
    return false;
}
}
