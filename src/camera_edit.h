#ifndef CAMERAEDIT_H
#define CAMERAEDIT_H

#include "animatedobject_edit.h"
#include "util.h"

namespace mdlx
{
struct CameraEdit : public AnimatedObjectEdit
{
    std::string name() const { return name_; }
    bool        name(const std::string &name);
    vec3        position{};
    float       fieldOfView = 0.0F, farClippingPlane = 0.0F, nearClippingPlane = 0.0F;
    vec3        targetPosition{};

    CameraEdit(Errors *errors, ModelEdit &model, AnimatedObject *animatedObject)
        : AnimatedObjectEdit(errors, model, animatedObject) {}

private:
    std::string name_;
};
}

#endif // CAMERAEDIT_H
