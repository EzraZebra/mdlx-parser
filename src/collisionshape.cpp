#include "collisionshape.h"
#include "binarystream.h"
#include "tokenstream.h"

#include "model_edit.h"

namespace mdlx
{
CollisionShape::CollisionShape(BinaryStream &stream)
    : GenericObject(stream, ObjectFlags::CollisionShape)
{
    type = stream.read<uint32_t>();

    vertices.at(0) = stream.read<vec3>();
    if (type != Type::Sphere) vertices.at(1) = stream.read<vec3>();
    if (type == Type::Sphere || type == Type::Cylinder) {
        boundsRadius = stream.read<float>();
    }
}

void CollisionShape::write(BinaryStream &stream) const
{
    GenericObject::write(stream);

    stream.write(type);
    stream.write(vertices.at(0));

    if (type != Type::Sphere) stream.write(vertices.at(1));
    if (type == Type::Sphere || type == Type::Cylinder) {
        stream.write(boundsRadius);
    }
}

size_t CollisionShape::getByteLength(uint32_t) const
{
    size_t size = baseByteLength + GenericObject::getByteLength();

    if (type != Type::Sphere) size += 12;
    if (type == Type::Sphere || type == Type::Cylinder) {
        size += 4;
    }

    return size;
}

CollisionShape::CollisionShape(TokenStream &stream)
    : GenericObject(ObjectFlags::CollisionShape)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        if (token == "Box") type = Type::Cube;
        else if (token == "Plane") type = Type::Plane;
        else if (token == "Sphere") type = Type::Sphere;
        else if (token == "Cylinder") type = Type::Cylinder;
        else if (token == "Vertices") {
            const auto count = stream.read<uint32_t>();

            stream.readToken(); // {

            vertices[0] = stream.read<vec3>();
            if (count == 2) vertices[1] = stream.read<vec3>();

            stream.readToken(); // }
        }
        else if (token == "BoundsRadius") boundsRadius = stream.read<float>();
        else stream.errors.fail(Errors::UnknownToken, "CollisionShape " + name, token);
    }
}

void CollisionShape::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "CollisionShape");

    size_t vertexCount = 2;
    bool writeBoundsRadius = false;

    switch(type) {
    case Type::Cube: stream.writeFlag("Box");
        break;
    case Type::Plane: stream.writeFlag("Plane");
        break;
    case Type::Sphere:
        stream.writeFlag("Sphere");
        vertexCount = 1;
        writeBoundsRadius = true;
        break;
    case Type::Cylinder:
        stream.writeFlag("Cylinder");
        writeBoundsRadius = true;
        break;
    }

    stream.startBlock("Vertices", vertexCount);
    stream.writeValue(vertices[0]);
    if (vertexCount == 2) stream.writeValue(vertices[1]);
    stream.endBlock();

    if (writeBoundsRadius) {
        stream.writeAttr("BoundsRadius", boundsRadius);
    }

    writeGenericAnimations(stream);
    stream.endBlock();
}

CollisionShape::CollisionShape(const std::shared_ptr<CollisionShapeEdit> &collisionShape)
    : GenericObject(collisionShape)
{
    type = collisionShape->type();
    vertices = collisionShape->vertices;
    boundsRadius = collisionShape->boundsRadius;
}

std::shared_ptr<CollisionShapeEdit> CollisionShape::edit(Errors *errors, ModelEdit &model)
{
    auto collisionShape = std::make_shared<CollisionShapeEdit>(GenericObject::edit(errors, model, this));

    collisionShape->type(type);
    collisionShape->vertices = vertices;
    collisionShape->boundsRadius = boundsRadius;

    return collisionShape;
}
}
