#ifndef COLLISIONSHAPE_H
#define COLLISIONSHAPE_H

#include "util.h"
#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
struct CollisionShapeEdit;

/**
 * @brief A collision shape.
 */
class CollisionShape : public GenericObject
{
    static const size_t baseByteLength = 16;

    int32_t type = -1;
    mat2x3  vertices{};
    float   boundsRadius = 0.0F;

public:
    enum Type {
        Cube        = 0,
        Plane       = 1,
        Sphere      = 2,
        Cylinder    = 3
    };

    CollisionShape(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    CollisionShape(TokenStream &stream);
    void write(TokenStream &stream) const;

    CollisionShape(const std::shared_ptr<CollisionShapeEdit> &collisionShape);
    std::shared_ptr<CollisionShapeEdit> edit(Errors *errors, ModelEdit &model);
};
}

#endif // COLLISIONSHAPE_H
