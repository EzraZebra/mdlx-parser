#include "collisionshape_edit.h"
#include "collisionshape.h"
#include "errors.h"

namespace mdlx
{
bool CollisionShapeEdit::type(int32_t type)
{
    if (type >= CollisionShape::Cube && type <= CollisionShape::Cylinder) {
        type_ = type;
        return true;
    }

    errors->add(Errors::Value, "Collision Shape Type", std::to_string(type));
    return false;
}
}
