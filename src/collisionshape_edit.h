#ifndef COLLISIONSHAPEEDIT_H
#define COLLISIONSHAPEEDIT_H

#include "genericobject_edit.h"

namespace mdlx
{
struct CollisionShapeEdit : public GenericObjectEdit
{
    int32_t type() const { return type_; }
    bool    type(int32_t type);
    mat2x3  vertices{};
    float   boundsRadius = 0.0F;

    CollisionShapeEdit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}

private:
    int32_t type_ = -1;
};
}

#endif // COLLISIONSHAPEEDIT_H
