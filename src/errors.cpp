#include "errors.h"

#include <stdexcept>

namespace mdlx
{
std::pair<std::string, Errors::ExceptType> Errors::getMsg(enum Type type, const std::string &s1, const std::string &s2)
{
    std::pair<std::string, Errors::ExceptType> error;

    switch(type) {
    case UnknownTag:
        error.first = "Unknown " + s1 + " tag: " + s2;
        error.second = ExceptType::Logic;
        break;
    case UnknownToken:
        error.first = "Unknown token in " + s1 + ": " + s2;
        error.second = ExceptType::Logic;
        break;
    case UnsupportedBlock:
        error.first = "Unsupported block: " + s1;
        error.second = ExceptType::Logic;
        break;
    case Stream:
        error.first = s1;
        error.second = ExceptType::Logic;
        break;
    case Overflow:
        error.first = "Buffer overflow in " + s1;
        error.second = ExceptType::OOR;
        break;
    case File:
        error.first = s1;
        error.second = ExceptType::Invalid;
        break;
    case FileIO:
        error.first = s1;
        error.second = ExceptType::Runtime;
        break;
    case Type:
        error.first = "Invalid type in " + s1;
        error.second = ExceptType::Logic;
        break;
    case Value:
        error.first = "Invalid " + s1 + " value: " + s2;
        error.second = ExceptType::Runtime;
        break;
    case Index:
        error.first = "Invalid " + s1 + " index: " + s2;
        error.second = ExceptType::Runtime;
        break;
    case ObjectInUse:
        error.first = s1 + "\"" + s2 + "\" is still in use.";
        error.second = ExceptType::Runtime;
        break;
    case Unknown:
        error.first = s1;
        if (!s2.empty()) error.first.append(": " + s2);
        error.second = ExceptType::Runtime;
        break;
    }

    return error;
}

void Errors::add(enum Type type, const std::string &s1, const std::string &s2)
{
    messages_.emplace_back(getMsg(type, s1, s2).first);
}

void Errors::fail(enum Type type, const std::string &s1, const std::string &s2)
{
    std::pair<std::string, Errors::ExceptType> error = getMsg(type, s1, s2);

    exceptions_.emplace_back(error.first);
    failed_ = true;

    switch(error.second) {
    case Logic: throw std::logic_error(error.first);
    case OOR: throw std::out_of_range(error.first);
    case Invalid: throw std::invalid_argument(error.first);
    case Runtime: throw std::runtime_error(error.first);
    }
}

void Errors::clear()
{
    messages_.clear();
    exceptions_.clear();
    failed_ = false;
}

bool Errors::haveMessages() const
{
    return !messages_.empty();
}

bool Errors::haveExceptions() const
{
    return !exceptions_.empty();
}

bool Errors::failed() const
{
    return failed_;
}

const std::vector<std::string>& Errors::messages() const
{
    return messages_;
}

const std::vector<std::string>& Errors::exceptions() const
{
    return exceptions_;
}
}
