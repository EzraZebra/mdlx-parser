#ifndef ERRORS_H
#define ERRORS_H

#include <vector>
#include <string>

namespace mdlx
{
/**
 * @brief Saves error messages and fail state
 */
struct Errors
{
    enum Type { UnknownTag, UnknownToken, UnsupportedBlock, Stream, Overflow, File, FileIO, Type, Value, Index, ObjectInUse, Unknown };

    void add(enum Type type, const std::string &s1, const std::string &s2 = {});
    void fail(enum Type type, const std::string &s1, const std::string &s2 = {});
    void clear();

    bool haveMessages() const;
    bool haveExceptions() const;
    bool failed() const;
    const std::vector<std::string>& messages() const;
    const std::vector<std::string>& exceptions() const;

private:
    enum ExceptType { Logic, OOR, Invalid, Runtime };

    std::vector<std::string>    messages_;
    std::vector<std::string>    exceptions_;
    bool                        failed_ = false;

    std::pair<std::string, ExceptType> getMsg(enum Type type, const std::string &s1, const std::string &s2);
};
}

#endif // ERRORS_H
