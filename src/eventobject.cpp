#include "eventobject.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "model_options.h"

#include "model_edit.h"

namespace mdlx
{
EventObject::EventObject(BinaryStream &stream)
    : GenericObject(stream, ObjectFlags::EventObject)
{
    stream.skip(); // KEVT

    const size_t count = stream.read<uint32_t>();

    globalSequenceId = stream.read<int32_t>();
    tracks = stream.readVector<uint32_t>(count);
}

void EventObject::write(BinaryStream &stream) const
{
    GenericObject::write(stream);
    stream.write(KEVT);
    stream.write<uint32_t>(tracks.size());
    stream.write(globalSequenceId);
    stream.writeVector(tracks);
}

size_t EventObject::getByteLength(uint32_t) const
{
    return baseByteLength + vectorByteLength(tracks) + GenericObject::getByteLength();
}

EventObject::EventObject(TokenStream &stream)
    : GenericObject(ObjectFlags::EventObject)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        if (token == "EventTrack")  {
            const size_t count = stream.read<uint32_t>();

            if (stream.peekToken() == "GlobalSeqId") {
                stream.readToken();
                globalSequenceId = stream.read<int32_t>();
            }

            tracks = stream.readVector<uint32_t>(count);
        }
        else stream.errors.fail(Errors::UnknownToken, "EventObject " + name, token);
    }
}

void EventObject::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "EventObject");

    stream.startBlock("EventTrack", tracks.size());

    if (globalSequenceId != -1 && (stream.version > v800 || !(stream.options & Flag::Magos))) {
        stream.writeAttr("GlobalSeqId", globalSequenceId);
    }

    for (const auto &track : tracks) stream.writeValue(track);

    stream.endBlock();


    writeGenericAnimations(stream);
    stream.endBlock();
}

EventObject::EventObject(const std::shared_ptr<EventObjectEdit> &eventObject)
    : GenericObject(eventObject)
{
    if (eventObject->globalSequence) {
        globalSequenceId = eventObject->globalSequence->index;
    }

    tracks = eventObject->tracks;
}

std::shared_ptr<EventObjectEdit> EventObject::edit(Errors *errors, ModelEdit &model)
{
    auto eventObject = std::make_shared<EventObjectEdit>(GenericObject::edit(errors, model, this));

    if (globalSequenceId >= 0 && globalSequenceId < static_cast<int32_t>(model.globalSequences.size())) {
        eventObject->globalSequence = model.globalSequences[globalSequenceId];
    }

    eventObject->tracks = tracks;

    return eventObject;
}
}
