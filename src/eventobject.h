#ifndef EVENTOBJECT_H
#define EVENTOBJECT_H

#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
struct EventObjectEdit;

/**
 * @brief An event object.
 */
class EventObject : public GenericObject
{
    static const size_t baseByteLength = 12;

    static const uint32_t KEVT = 0x5456454b;

    int32_t                 globalSequenceId = -1;
    std::vector<uint32_t>   tracks;

public:
    EventObject(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    EventObject(TokenStream &stream);
    void write(TokenStream &stream) const;

    EventObject(const std::shared_ptr<EventObjectEdit> &eventObject);
    std::shared_ptr<EventObjectEdit> edit(Errors *errors, ModelEdit &model);
};
}

#endif // EVENTOBJECT_H
