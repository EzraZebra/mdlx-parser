#ifndef EVENTOBJECT_EDIT_H
#define EVENTOBJECT_EDIT_H

#include "genericobject_edit.h"

namespace mdlx
{
/** Forward declare */
struct GlobalSequenceEdit;

struct EventObjectEdit : public GenericObjectEdit
{
    std::shared_ptr<GlobalSequenceEdit> globalSequence;
    std::vector<uint32_t>               tracks;

    EventObjectEdit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}
};
}

#endif // EVENTOBJECT_EDIT_H
