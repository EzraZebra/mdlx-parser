#include "extent.h"
#include "binarystream.h"
#include "tokenstream.h"

namespace mdlx
{
Extent::Extent(BinaryStream &stream)
{
    read(stream);
}

void Extent::read(BinaryStream &stream)
{
    boundsRadius = stream.read<float>();
    min = stream.read<vec3>();
    max = stream.read<vec3>();
}

void Extent::write(BinaryStream &stream) const
{
    stream.write(boundsRadius);
    stream.write(min);
    stream.write(max);
}

Extent::Extent(TokenStream &stream)
{
    for (std::string token; !(token = stream.iterBlock()).empty(); ) {
        if (!read(stream, token)) {
            stream.errors.fail(Errors::Stream, "Extent", token);
        }
    }
}

bool Extent::read(TokenStream &stream, const std::string &token)
{
    if (token == "MinimumExtent") min = stream.read<vec3>();
    else if (token == "MaximumExtent") max = stream.read<vec3>();
    else if (token == "BoundsRadius") boundsRadius = stream.read<float>();
    else return false;

    return true;
}

void Extent::write(TokenStream &stream) const
{
    if (min != vec3{ 0.0F, 0.0F, 0.0F }) {
        stream.writeAttr("MinimumExtent", min);
    }

    if (max != vec3{ 0.0F, 0.0F, 0.0F }) {
        stream.writeAttr("MaximumExtent", max);
    }

    if (boundsRadius != 0) {
        stream.writeAttr("BoundsRadius", boundsRadius);
    }
}
}
