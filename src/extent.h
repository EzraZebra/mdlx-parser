#ifndef EXTENT_H
#define EXTENT_H

#include "util.h"

namespace mdlx
{
/** Forward declare */
class BinaryStream;
class TokenStream;

/**
 * @brief An extent.
 */
struct Extent
{
    static const size_t baseByteLength = 28;

    float   boundsRadius = 0.0F;
    vec3    min = { 0.0F, 0.0F, 0.0F }, max = { 0.0F, 0.0F, 0.0F };

    Extent() = default;

    Extent(BinaryStream &stream);
    void read(BinaryStream &stream);
    void write(BinaryStream &stream) const;

    Extent(TokenStream &stream);
    bool read(TokenStream &stream, const std::string &token);
    void write(TokenStream &stream) const;
};
}

#endif // EXTENT_H
