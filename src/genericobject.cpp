#include "animation.h"
#include "animationmap.h"
#include "genericobject.h"
#include "binarystream.h"
#include "tokenstream.h"

#include "model_edit.h"

namespace mdlx
{
GenericObject::GenericObject(uint32_t flags)
    : flags(flags) {}

GenericObject::GenericObject(BinaryStream &stream, uint32_t flags)
    : flags(flags)
{
    read(stream);
}

void GenericObject::read(BinaryStream &stream)
{
    const size_t size = stream.read<uint32_t>();

    name = stream.readString(strSizeSmall);
    objectId = stream.read<int32_t>();
    parentId = stream.read<int32_t>();
    flags = stream.read<uint32_t>();

    readAnimations(stream, size-baseByteLength);
}

void GenericObject::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getGenericByteLength());
    stream.writeString(name);
    stream.skip(strSizeSmall - name.length());
    stream.write(objectId);
    stream.write(parentId);
    stream.write(flags);

    for (const auto &animation : eachAnimation(true)) animation->write(stream);
}

void GenericObject::writeNonGenericAnimationChunks(BinaryStream &stream) const
{
    for (const auto &animation : eachAnimation(false)) animation->write(stream);
}

animVec GenericObject::eachAnimation(bool wantGeneric) const
{
    animVec returnAnimations;

    for (const auto &animation : animations) {
        uint32_t tag = animation->tag();
        if (wantGeneric == (tag == KGTR || tag == KGRT || tag == KGSC)) {
            returnAnimations.emplace_back(animation);
        }
    }

    return returnAnimations;
}

size_t GenericObject::getGenericByteLength() const
{
    size_t size = baseByteLength;

    for (const auto &animation : eachAnimation(true)) {
        size += animation->getByteLength();
    }

    return size;
}

size_t GenericObject::getByteLength() const
{
    return baseByteLength + AnimatedObject::getByteLength();
}

std::string GenericObject::iterGenericBlock(TokenStream &stream)
{
    if (!iterBlock) {
        name = stream.read<std::string>();
        iterBlock = true;
    }

    for (std::string token; !(token = iterAnimatedBlock(stream)).empty(); ) {
        if (token == "ObjectId") objectId = stream.read<int32_t>();
        else if (token == "Parent") parentId = stream.read<int32_t>();
        else if (token == "BillboardedLockZ" || token == "BillboardLockZ") {
            flags |= GenericFlags::BillboardedLockZ;
        }
        else if (token == "BillboardedLockY" || token == "BillboardLockY") {
            flags |= GenericFlags::BillboardedLockY;
        }
        else if (token == "BillboardedLockX" || token == "BillboardLockX") {
            flags |= GenericFlags::BillboardedLockX;
        }
        else if (token == "Billboarded" || token == "Billboard") {
            flags |= GenericFlags::Billboarded;
        }
        else if (token == "CameraAnchored") flags |= GenericFlags::CameraAnchored;
        else if (token == "DontInherit") {
            while (!(token = stream.iterBlock()).empty()) {
                if (token == "Rotation") flags |= GenericFlags::DontInheritRotation;
                else if (token == "Translation") flags |= GenericFlags::DontInheritTranslation;
                else if (token == "Scaling") flags |= GenericFlags::DontInheritScaling;
            }
        }
        else if (!readAnimation(stream, "GenericObject", token)) {
            return token;
        }
    }

    iterBlock = false;
    return {};
}

void GenericObject::writeGenericHeader(TokenStream &stream, const std::string &objectToken) const
{
    stream.startBlock(objectToken, name);

    stream.writeAttr("ObjectId", objectId);

    if (parentId != -1) stream.writeAttr("Parent", parentId);
    if (flags & GenericFlags::DontInheritTranslation) stream.writeFlag("DontInherit { Translation }");
    if (flags & GenericFlags::DontInheritRotation) stream.writeFlag("DontInherit { Rotation }");
    if (flags & GenericFlags::DontInheritScaling) stream.writeFlag("DontInherit { Scaling }");
    if (flags & GenericFlags::Billboarded) stream.writeFlag("Billboarded");
    if (flags & GenericFlags::BillboardedLockX) stream.writeFlag("BillboardedLockX");
    if (flags & GenericFlags::BillboardedLockY) stream.writeFlag("BillboardedLockY");
    if (flags & GenericFlags::BillboardedLockZ) stream.writeFlag("BillboardedLockZ");
    if (flags & GenericFlags::CameraAnchored) stream.writeFlag("CameraAnchored");
}

void GenericObject::writeGenericAnimations(TokenStream &stream) const
{
    writeAnimation(stream, KGTR);
    writeAnimation(stream, KGRT);
    writeAnimation(stream, KGSC);
}

GenericObject::GenericObject(const std::shared_ptr<GenericObjectEdit> &genericObject)
    : AnimatedObject(genericObject)
{
    objectId = genericObject->objectId;

    if (genericObject->parent) {
        parentId = genericObject->parent->objectId;
    }

    name = genericObject->name();
    flags = genericObject->flags();
}

GenericObjectEdit GenericObject::edit(Errors* errors, ModelEdit &model, AnimatedObject *animatedObject)
{
    GenericObjectEdit genericObject(errors, model, animatedObject);

    genericObject.objectId = objectId;
    // parentId set later in Model::prepareEdit
    genericObject.name(name);
    genericObject.flags(flags);

    return genericObject;
}
}
