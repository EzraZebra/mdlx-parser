#ifndef GENERICOBJECT_H
#define GENERICOBJECT_H

#include "animatedobject.h"

namespace mdlx
{
/** Forward declare */
class GenericObjectEdit;

/**
 * @brief A generic object.
 *
 * The parent class for all objects that exist in the world, and may contain spatial animations.
 * This includes bones, particle emitters, and many other things.
 */
class GenericObject : public AnimatedObject
{
    bool iterBlock = false;

protected:
    static const size_t baseByteLength = 96;

    std::string name;
    int32_t     objectId = -1, parentId = -1;
    uint32_t    flags = 0;

    GenericObject(uint32_t flags=0);

    GenericObject(BinaryStream &stream, uint32_t flags=0);
    void read(BinaryStream &stream);

    void write(BinaryStream &stream) const;
    void writeNonGenericAnimationChunks(BinaryStream &stream) const;
    /** @brief Allows to easily iterate either the GenericObject animations or the parent object animations. */
    animVec eachAnimation(bool wantGeneric) const;

    /**
     * @brief Gets the byte length of the GenericObject part of whatever this object is.
     *
     * This is needed because only the KGTR, KGRT, and KGSC animations actually belong to it.
     */
    size_t getGenericByteLength() const;
    size_t getByteLength() const;

    std::string iterGenericBlock(TokenStream &stream);
    /** @brief Start the object block and write generic properties */
    void writeGenericHeader(TokenStream &stream, const std::string &objectToken) const;
    void writeGenericAnimations(TokenStream &stream) const;

    GenericObject(const std::shared_ptr<GenericObjectEdit> &genericObject);
    GenericObjectEdit edit(Errors* errors, ModelEdit &model, AnimatedObject *animatedObject);

public:
    enum ObjectFlags {
        Bone            = 0x100,
        Light           = 0x200,
        EventObject     = 0x400,
        Attachment      = 0x800,
        ParticleEmitter = 0x1000,
        CollisionShape  = 0x2000,
        RibbonEmitter   = 0x4000,
    };
    enum GenericFlags {
        DontInheritTranslation  = 0x1,
        DontInheritRotation     = 0x2,
        DontInheritScaling      = 0x4,
        Billboarded             = 0x8,
        BillboardedLockX        = 0x10,
        BillboardedLockY        = 0x20,
        BillboardedLockZ        = 0x40,
        CameraAnchored          = 0x80,
    };

    int32_t getParentId() const { return parentId; }
};
}

#endif // GENERICOBJECT_H
