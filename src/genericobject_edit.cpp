#include "util.h"
#include "genericobject_edit.h"
#include "genericobject.h"
#include "errors.h"

namespace mdlx
{
bool GenericObjectEdit::name(const std::string &name)
{
    if (!name.empty() && name.length() <= strSizeSmall) {
        name_ = name;
        return true;
    }

    errors->add(Errors::Value, "Object Name", name);
    return false;
}

bool GenericObjectEdit::flags(uint32_t flags, bool ignoreError)
{
    bool valid = flags == 0 || flags & GenericObject::Bone || flags & GenericObject::Light || flags & GenericObject::EventObject
            || flags & GenericObject::Attachment || flags & GenericObject::ParticleEmitter || flags & GenericObject::CollisionShape
            || flags & GenericObject::RibbonEmitter || flags & GenericObject::DontInheritTranslation || flags & GenericObject::DontInheritScaling
            || flags & GenericObject::DontInheritRotation || flags & GenericObject::Billboarded || flags & GenericObject::BillboardedLockX
            || flags & GenericObject::BillboardedLockY || flags & GenericObject::BillboardedLockZ || flags & GenericObject::CameraAnchored;

    if (valid || ignoreError) flags_ = flags;

    return valid;
}
}
