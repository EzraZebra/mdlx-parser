#ifndef GENERICOBJECTEDIT_H
#define GENERICOBJECTEDIT_H

#include "animatedobject_edit.h"
#include "util.h"

namespace mdlx
{
/** Forward declare */
class ModelEdit;
class Errors;

struct GenericObjectEdit : public AnimatedObjectEdit
{
    vec3 pivotPoint = { 0.0F, 0.0F, 0.0F };

    int32_t objectId = -1, parentId = -1;
    std::shared_ptr<GenericObjectEdit> parent;

    std::string name() const { return name_; }
    bool        name(const std::string &name);
    uint32_t    flags() const { return flags_; }
    bool        flags(uint32_t flags, bool ignoreError=true);

    GenericObjectEdit(Errors* errors, ModelEdit &model, AnimatedObject *animatedObject)
        : AnimatedObjectEdit(errors, model, animatedObject) {}

protected:
    std::string name_;
    uint32_t    flags_ = 0;
};
}

#endif // GENERICOBJECTEDIT_H
