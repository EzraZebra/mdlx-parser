#include "geoset.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "model_options.h"

#include "model_edit.h"

namespace mdlx
{
Geoset::Geoset(BinaryStream &stream)
{
    stream.read<uint32_t>(); // Don't care about the size.

    stream.skip(); // VRTX
    vertices = stream.readVector<vec3>(stream.read<uint32_t>());

    stream.skip(); // NRMS
    normals = stream.readVector<vec3>(stream.read<uint32_t>());

    stream.skip(); // PTYP
    faceTypeGroups = stream.readVector<uint32_t>(stream.read<uint32_t>());

    stream.skip(); // PCNT
    faceGroups = stream.readVector<uint32_t>(stream.read<uint32_t>());

    stream.skip(); // PVTX
    faces = stream.readVector<uint16_t>(stream.read<uint32_t>());

    stream.skip(); // GNDX
    vertexGroups = stream.readVector<uint8_t>(stream.read<uint32_t>());

    stream.skip(); // MTGC
    matrixGroups = stream.readVector<uint32_t>(stream.read<uint32_t>());

    stream.skip(); // MATS
    matrixIndices = stream.readVector<uint32_t>(stream.read<uint32_t>());

    materialId = stream.read<uint32_t>();
    selectionGroup = stream.read<uint32_t>();
    selectionFlags = stream.read<uint32_t>();

    if (stream.version >= v900) {
        lod = stream.read<int32_t>();
        lodName = stream.readString(strSizeSmall);
    }

    extent.read(stream);

    size_t count = stream.read<uint32_t>();
    sequenceExtents.reserve(count);
    for (size_t i=0; i<count; i++) {
        sequenceExtents.emplace_back(Extent(stream));
    }

    // Non-reforged models that come with reforged are saved with version >800, however they don't have TANG and SKIN.
    if (stream.version >= v900) {
        if (stream.peek() == TANG) {
            stream.skip(); // TANG
            tangents = stream.readVector<vec4>(stream.read<uint32_t>());
        }

        if (stream.peek() == SKIN) {
            stream.skip(); // SKIN
            skinWeights = stream.readVector<u8vec8>(stream.read<uint32_t>() / 8);
        }
    }

    stream.skip(); // UVAS
    count = stream.read<uint32_t>(); // UVAS count
    uvSets.reserve(count);
    for (size_t i=0; i<count; i++) {
        stream.skip(); // UVBS
        uvSets.emplace_back(stream.readVector<vec2>(stream.read<uint32_t>()));
    }
}

void Geoset::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength(stream.version));

    stream.write(VRTX);
    stream.write<uint32_t>(vertices.size());
    stream.writeVector(vertices);

    stream.write(NRMS);
    stream.write<uint32_t>(normals.size());
    stream.writeVector(normals);

    stream.write(PTYP);
    stream.write<uint32_t>(faceTypeGroups.size());
    stream.writeVector(faceTypeGroups);

    stream.write(PCNT);
    stream.write<uint32_t>(faceGroups.size());
    stream.writeVector(faceGroups);

    stream.write(PVTX);
    stream.write<uint32_t>(faces.size());
    stream.writeVector(faces);

    stream.write(GNDX);
    stream.write<uint32_t>(vertexGroups.size());
    stream.writeVector(vertexGroups);

    stream.write(MTGC);
    stream.write<uint32_t>(matrixGroups.size());
    stream.writeVector(matrixGroups);

    stream.write(MATS);
    stream.write<uint32_t>(matrixIndices.size());
    stream.writeVector(matrixIndices);

    stream.write(materialId);
    stream.write(selectionGroup);
    stream.write(selectionFlags);

    if (stream.version >= v900) {
        stream.write(lod);
        stream.writeString(lodName);
        stream.skip(strSizeSmall - lodName.length());
    }

    extent.write(stream);

    stream.write<uint32_t>(sequenceExtents.size());
    for (const auto &sequenceExtent : sequenceExtents) sequenceExtent.write(stream);

    if (stream.version >= v900) {
        if (!tangents.empty()) {
            stream.write(TANG);
            stream.write<uint32_t>(tangents.size());
            stream.writeVector(tangents);
        }

        if (!skinWeights.empty()) {
            stream.write(SKIN);
            stream.write<uint32_t>(skinWeights.size() * 8);
            stream.writeVector(skinWeights);
        }
    }

    stream.write(UVAS);
    stream.write<uint32_t>(uvSets.size());
    for (const auto &uvSet : uvSets) {
        stream.write(UVBS);
        stream.write<uint32_t>(uvSet.size());
        stream.writeVector(uvSet);
    }
}

size_t Geoset::getByteLength(uint32_t version) const
{
    size_t size = baseByteLength
            + vectorByteLength(vertices) + vectorByteLength(normals)
            + vectorByteLength(faceTypeGroups) + vectorByteLength(faceGroups) + vectorByteLength(faces)
            + vectorByteLength(vertexGroups) + vectorByteLength(matrixGroups) + vectorByteLength(matrixIndices)
            + sequenceExtents.size()*Extent::baseByteLength;

    if (version >= v900) {
        size += baseByteLengthV900;
        if (!tangents.empty()) size += 8 + vectorByteLength(tangents);
        if (!skinWeights.empty()) size += 8 + vectorByteLength(skinWeights);
    }

    for (const auto &uvSet : uvSets) {
        size += 8 + vectorByteLength(uvSet);
    }

    return size;
}

Geoset::Geoset(TokenStream &stream)
{
    for (std::string token; !(token = stream.iterBlock()).empty(); ) {
        if (token == "Vertices") vertices = stream.readVector<vec3>(stream.read<uint32_t>());
        else if (token == "Normals") normals = stream.readVector<vec3>(stream.read<uint32_t>());
        else if (token == "TVertices") uvSets.emplace_back(stream.readVector<vec2>(stream.read<uint32_t>()));
        else if (token == "VertexGroup") {
            std::vector<int32_t> vGtmp;
            stream.readVectorBlind(vGtmp);
            if (!vGtmp.empty() && vGtmp[0] != -1) { // Support RMS 0.4
                vertexGroups.reserve(vGtmp.size());
                for (const auto &vG : vGtmp) {
                    vertexGroups.emplace_back(static_cast<uint8_t>(vG));
                }
            }
        }
        else if (token == "Tangents") tangents = stream.readVector<vec4>(stream.read<uint32_t>());
        else if (token == "SkinWeights") {
            if (stream.peekToken() == "{") {
                stream.readVectorBlind(skinWeights);
            }
            else skinWeights = stream.readVectorMulti<u8vec8>(stream.read<uint32_t>());
        }
        else if (token == "Skin") { // "Skin": RMS 0.4
            skinWeights = stream.readVector<u8vec8>(stream.read<uint32_t>());
        }
        else if (token == "Faces") {
            // For now hardcoded for triangles, until I see a model with something different.
            faceTypeGroups.emplace_back(4);

            // Number of vectors the indices are spread over.
            const size_t vectors = stream.read<uint32_t>();
            // Total number of indices
            const size_t count = stream.read<uint32_t>();

            faceGroups.emplace_back(count);

            stream.readToken(); // {
            stream.readToken(); // Triangles
            stream.readToken(); // {

            faces.reserve(count);
            for (uint32_t i=0; i<vectors; i++) {
                stream.readVectorBlind(faces);
            }

            stream.readToken(); // }
            stream.readToken(); // }
        }
        else if (token == "Groups") {
            matrixGroups.reserve(stream.read<uint32_t>()); // matrices count
            matrixIndices.reserve(stream.read<uint32_t>()); // total indices

            while (!stream.iterBlock().empty()) {
                const size_t size = matrixIndices.size();
                stream.readVectorBlind(matrixIndices);
                matrixGroups.emplace_back(matrixIndices.size() - size);
            }
        }
        else if (token == "Anim") sequenceExtents.emplace_back(Extent(stream));
        else if (token == "MaterialID") materialId = stream.read<uint32_t>();
        else if (token == "SelectionGroup") selectionGroup = stream.read<uint32_t>();
        else if (token == "Unselectable") selectionFlags |= Unselectable;
        else if (token == "LevelOfDetail") lod = stream.read<int32_t>();
        else if (token == "Name" || token == "LevelOfDetailName") lodName = stream.read<std::string>(); // "LevelOfDetailName": RMS 0.4
        else if (!extent.read(stream, token)) {
            stream.errors.fail(Errors::UnknownToken, "Geoset", token);
        }
    }
}

void Geoset::write(TokenStream &stream) const
{
    stream.startBlock("Geoset");

    stream.writeVectorBlock("Vertices", vertices);
    stream.writeVectorBlock("Normals", normals);

    for (const auto &uvSet : uvSets) {
        stream.writeVectorBlock("TVertices", uvSet);
    }

    if (!vertexGroups.empty()) {
        stream.writeVectorBlock("VertexGroup", vertexGroups, false);
    }

    if (stream.version >= v900) {
        if (!tangents.empty()) {
            stream.writeVectorBlock("Tangents", tangents);
        }

        if (!skinWeights.empty()) {
            stream.writeVectorBlockMulti("SkinWeights", skinWeights);
        }
    }

    // For now hardcoded for triangles, until I see a model with something different.
    stream.startBlock("Faces", 1, faces.size());
    stream.startBlock("Triangles");
    stream.writeValue(faces);
    stream.endBlock(); // Faces
    stream.endBlock(); // Triangles

    stream.startBlock("Groups", matrixGroups.size(), matrixIndices.size());
    uint32_t index = 0;
    for (const auto &groupSize : matrixGroups) {
        std::vector<uint32_t> sub(matrixIndices.begin()+index, matrixIndices.begin()+index+groupSize);
        stream.writeAttr("Matrices", sub);
        index += groupSize;
    }
    stream.endBlock(); // Groups

    extent.write(stream);

    for (const auto &sequenceExtent : sequenceExtents) {
        stream.startBlock("Anim");
        sequenceExtent.write(stream);
        stream.endBlock();
    }

    stream.writeAttr("MaterialID", materialId);
    stream.writeAttr("SelectionGroup", selectionGroup);

    if (selectionFlags & Unselectable) {
        stream.writeFlag("Unselectable");
    }

    if (stream.version >= v900) {
        stream.writeAttr("LevelOfDetail", lod);
        if (!lodName.empty()) {
            stream.writeAttr("Name", lodName);
        }
    }

    stream.endBlock(); // Geosets
}

Geoset::Geoset(const std::shared_ptr<GeosetEdit> &geoset)
{
    // Fill vertex data
    vertices.reserve(geoset->vertices.size());
    normals.reserve(geoset->vertices.size());
    for (const auto &vertex : geoset->vertices) {
        vertices.emplace_back(vertex->vertex);
        normals.emplace_back(vertex->normal);

        if (uvSets.size() < vertex->uvSets.size()) {
            uvSets.resize(vertex->uvSets.size());
        }
        for (size_t i=0; i<vertex->uvSets.size(); i++) {
            uvSets[i].emplace_back(vertex->uvSets[i]);
        }

        if (vertex->vertexGroup) {
            vertexGroups.emplace_back(vertex->vertexGroup->index);
        }

        if (vertex->haveTangent()) {
            tangents.emplace_back(vertex->tangent());
        }
        if (vertex->haveSkinWeight()) {
            const SkinWeightObjects &skinWeightObjects = vertex->skinWeightObjects();
            const u8vec4 &skinWeightWeights = vertex->skinWeightWeights();
            skinWeights.emplace_back(u8vec8 {
                                         static_cast<uint8_t>(skinWeightObjects[0]->objectId),
                                         static_cast<uint8_t>(skinWeightObjects[1]->objectId),
                                         static_cast<uint8_t>(skinWeightObjects[2]->objectId),
                                         static_cast<uint8_t>(skinWeightObjects[3]->objectId),
                                         skinWeightWeights[0], skinWeightWeights[1], skinWeightWeights[2], skinWeightWeights[3],
                                     });
        }
    }

    // Fill faces
    faces.reserve(geoset->triangles.size()*3);
    faceGroups.emplace_back(geoset->triangles.size()*3);
    faceTypeGroups.emplace_back(4);

    for (const auto &triangle : geoset->triangles) {
        for (uint32_t i=0; i<3; i++) {
            faces.emplace_back(triangle.at(i)->index);
        }
    }

    // Fill matrixGroups
    matrixGroups.reserve(geoset->matrixGroups.size());
    for (auto &matrixGroup : geoset->matrixGroups) {
        uint32_t size = matrixGroup->objects.size();

        for (auto &object : matrixGroup->objects) {
            if (object && object->objectId >= 0) {
                matrixIndices.emplace_back(object->objectId);
            }
            else size--;
        }

        matrixGroups.emplace_back(size);
    }

    // Fill rest
    if (geoset->material) {
        materialId = geoset->material->index;
    }
    selectionGroup = geoset->selectionGroup;
    selectionFlags = geoset->selectionFlags();

    lod = geoset->lod;
    lodName = geoset->lodName();

    extent = geoset->extent;
    sequenceExtents = geoset->sequenceExtents;
}

std::shared_ptr<GeosetEdit> Geoset::edit(uint32_t index, Errors *errors, ModelEdit &model)
{
    auto geoset = std::make_shared<GeosetEdit>(index, errors);

    if (materialId < model.materials.size()) {
        geoset->material = model.materials[materialId];
    }

    geoset->selectionGroup = selectionGroup;
    geoset->selectionFlags(selectionFlags);
    geoset->lod = lod;
    geoset->lodName(lodName);
    geoset->extent = extent;
    geoset->sequenceExtents = sequenceExtents;

    // Fill MatrixGroups
    geoset->matrixGroups.reserve(matrixGroups.size());
    for (size_t index=0; index<matrixGroups.size(); index++) {
        geoset->matrixGroups.emplace_back(std::make_shared<MatrixGroup>(index));
        // objects are filled later in Model::prepareEdit()
    }

    // Fill Vertices
    geoset->vertices.reserve(vertices.size());
    for (size_t index=0; index<vertices.size(); index++) {
        std::vector<vec2> uvSubSets;
        for (const auto &uvSet : uvSets) {
            uvSubSets.emplace_back(uvSet[index]);
        }

        auto vertex = geoset->vertices.emplace_back(std::make_shared<Vertex>(index, vertices[index], normals[index], uvSubSets));
        if (index < vertexGroups.size() && vertexGroups[index] < geoset->matrixGroups.size()) {
            vertex->vertexGroup = geoset->matrixGroups[vertexGroups[index]];
        }
        if (index < tangents.size()) vertex->tangent(tangents[index]);
        // SkinWeights inserted later in Model::prepareEdit
    }

    // Fill triangles
    geoset->triangles.reserve(faces.size()/3);
    for (size_t i=0; i+2<faces.size(); i+=3) {
        geoset->triangles.emplace_back(triangle { geoset->vertices[faces[i]], geoset->vertices[faces[i+1]], geoset->vertices[faces[i+2]] });
    }

    return geoset;
}
}
