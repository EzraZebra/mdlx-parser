#ifndef GEOSET_H
#define GEOSET_H

#include "extent.h"

#include <memory>

namespace mdlx
{
/** Forward declare */
class ModelEdit;
class GeosetEdit;
class Errors;

/**
 * @brief A geoset.
 */
class Geoset
{
    static const size_t baseByteLength = 120;
    static const size_t baseByteLengthV900 = 84;

    static const uint32_t VRTX = 0x58545256;
    static const uint32_t NRMS = 0x534d524e;
    static const uint32_t PTYP = 0x50595450;
    static const uint32_t PCNT = 0x544e4350;
    static const uint32_t PVTX = 0x58545650;
    static const uint32_t GNDX = 0x58444e47;
    static const uint32_t MTGC = 0x4347544d;
    static const uint32_t MATS = 0x5354414d;
    static const uint32_t TANG = 0x474e4154;
    static const uint32_t SKIN = 0x4e494b53;
    static const uint32_t UVAS = 0x53415655;
    static const uint32_t UVBS = 0x53425655;

    std::vector<vec3>       vertices{}, normals{};
    std::vector<uint32_t>   faceTypeGroups{}, faceGroups{};
    std::vector<uint16_t>   faces{};
    std::vector<uint8_t>    vertexGroups{};
    std::vector<uint32_t>   matrixGroups{}, matrixIndices{};

    uint32_t materialId = 0, selectionGroup = 0, selectionFlags = 0;

    /** @since 900 */
    int32_t     lod = -1;
    std::string lodName;

    Extent              extent;
    std::vector<Extent> sequenceExtents{};

    /** @since 900 */
    std::vector<vec4> tangents{};
    /**
     * An array of bone indices and weights.
     * Every 8 consecutive elements describe the following:
     *    [B0, B1, B2, B3, W0, W1, W2, W3]
     * Where:
     *     Bn is a bone index.
     *     Wn is a weight, which can be normalized with Wn/255.
     *
     * @since 900
     */
    std::vector<u8vec8> skinWeights{};

    std::vector<std::vector<vec2>> uvSets{};

public:
    enum Flags { Unselectable = 4 };

    Geoset(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t version) const;

    Geoset(TokenStream &stream);
    void write(TokenStream &stream) const;

    Geoset(const std::shared_ptr<GeosetEdit> &geoset);
    std::shared_ptr<GeosetEdit> edit(uint32_t index, Errors *errors, ModelEdit &model);

    std::vector<uint32_t> getMatrixGroups() const { return matrixGroups; }
    std::vector<uint32_t> getMatrixIndices() const { return matrixIndices; }
    std::vector<u8vec8> getSkinWeights() const { return skinWeights; }
};
}

#endif // GEOSET_H
