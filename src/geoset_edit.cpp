#include "geoset_edit.h"
#include "geoset.h"
#include "model_edit.h"
#include "errors.h"

namespace mdlx
{
bool MatrixGroup::operator==(const MatrixGroup &rhs) const
{
    if (objects.size() == rhs.objects.size()) {
        for (size_t i=0; i<objects.size(); i++) {
            if (objects[i] != rhs.objects[i]) {
                return false;
            }
        }

        return true;
    }

    return false;
}

bool Vertex::operator==(const Vertex &rhs) const
{
    return compf(vertex, rhs.vertex) && compf(uvSets, rhs.uvSets) && vertexGroup == rhs.vertexGroup
            && haveTangent_ == rhs.haveTangent() && (!haveTangent_ || compf(tangent_, rhs.tangent()))
            && haveSkinWeight_ == rhs.haveSkinWeight()
            && (!haveSkinWeight_ || (skinWeightObjects() == rhs.skinWeightObjects() && skinWeightWeights() == rhs.skinWeightWeights()));
}

bool GeosetEdit::operator==(const GeosetEdit &rhs) const
{
    return material == rhs.material && selectionGroup == rhs.selectionGroup && selectionFlags_ == rhs.selectionFlags()
            && lod == rhs.lod && lodName_ == rhs.lodName();
}

bool GeosetEdit::selectionFlags(uint32_t flags)
{
    if (flags == 0 || flags & Geoset::Unselectable) {
        selectionFlags_ = flags;
        return true;
    }

    errors->add(Errors::Value, "Geoset Selection Flags", std::to_string(flags));
    return false;
}

bool GeosetEdit::lodName(const std::string &lodName)
{
    if (lodName.length() <= strSizeSmall) {
        lodName_ = lodName;
        return true;
    }

    errors->add(Errors::Value, "Geoset LOD Name", lodName);
    return false;
}

bool GeosetEdit::deleteVertex(uint32_t index, bool skipCheck)
{
    if (index < vertices.size()) {
        if (!skipCheck) {
            // Delete triangles containing the deleted vertex
            for (size_t i=0; i<triangles.size(); i++) {
                if (triangles[i][0] == vertices[index] || triangles[i][1] == vertices[index] || triangles[i][2] == vertices[index]) {
                    triangles.erase(triangles.begin()+i);
                    i--;
                }
            }
        }

        return ModelEdit::deleteIndexedObject(index, vertices);
    }

    errors->add(Errors::Index, "deleteVertex", std::to_string(index));
    return false;
}

bool GeosetEdit::deleteTriangle(uint32_t index)
{
    if (index < triangles.size()) {
        triangles.erase(triangles.begin()+index);
        return true;
    }

    errors->add(Errors::Index, "deleteTriangle", std::to_string(index));
    return false;
}

bool GeosetEdit::deleteMatrixGroup(uint32_t index, bool skipCheck)
{
    if (index < matrixGroups.size()) {
        if (!skipCheck) {
            // Check vertices
            for (auto &vertex : vertices) {
                if (vertex->vertexGroup == matrixGroups[index]) {
                    vertex->vertexGroup = nullptr;
                }
            }
        }

        return ModelEdit::deleteIndexedObject(index, matrixGroups);
    }

    errors->add(Errors::Index, "deleteMatrixGroup", std::to_string(index));
    return false;
}
}
