#ifndef GEOSETEDIT_H
#define GEOSETEDIT_H

#include "extent.h"

#include <memory>

namespace mdlx
{
/** Forward declare */
struct MaterialEdit;
struct GenericObjectEdit;
class Errors;

/**
 * @brief A matrix group.
 */
struct MatrixGroup
{
    uint8_t index;
    std::vector<std::shared_ptr<GenericObjectEdit>> objects;

    MatrixGroup(uint32_t index)
        : index(static_cast<uint8_t>(index)) {}

    bool operator==(const MatrixGroup &rhs) const;
    bool operator!=(const MatrixGroup &rhs) const { return operator==(rhs); }
};

using SkinWeightObjects = std::array<std::shared_ptr<GenericObjectEdit>, 4>;

/**
 * @brief A vertex with its normal, uvSet and matrix group.
 */
class Vertex
{
public:
    uint16_t                        index;
    vec3                            vertex, normal;
    std::vector<vec2>               uvSets;
    std::shared_ptr<MatrixGroup>    vertexGroup;

    const vec4& tangent() const { return tangent_; }
    bool haveTangent() const { return haveTangent_; }
    void tangent(vec4 tangent)
    {
        tangent_ = tangent;
        haveTangent_ = true;
    }

    const SkinWeightObjects&    skinWeightObjects() const { return skinWeightObjects_; }
    const u8vec4&               skinWeightWeights() const { return skinWeightWeights_; }
    bool haveSkinWeight() const { return haveSkinWeight_; }
    void skinWeight(const SkinWeightObjects &objects, const u8vec4 &weights)
    {
        skinWeightObjects_ = objects;
        skinWeightWeights_ = weights;
        haveSkinWeight_ = true;
    }

    Vertex(uint32_t index,
           const vec3 &vertex, const vec3 &normal, const std::vector<vec2> &uvSets)
        : index(static_cast<uint16_t>(index)),
          vertex(vertex), normal(normal), uvSets(uvSets) {}

    bool operator==(const Vertex &rhs) const;
    bool operator!=(const Vertex &rhs) const { return !operator==(rhs); }

private:
    /** @since 900 */
    vec4                tangent_;
    SkinWeightObjects   skinWeightObjects_;
    u8vec4              skinWeightWeights_;

    bool haveTangent_ = false;
    bool haveSkinWeight_ = false;
};


using triangle = std::array<std::shared_ptr<Vertex>, 3>;

struct GeosetEdit
{
    uint32_t index;

    std::vector<std::shared_ptr<Vertex>>        vertices;
    std::vector<triangle>                       triangles;
    std::vector<std::shared_ptr<MatrixGroup>>   matrixGroups;
    std::shared_ptr<MaterialEdit>               material;
    uint32_t        selectionGroup = 0;

    uint32_t selectionFlags() const { return selectionFlags_; }
    bool selectionFlags(uint32_t flags);

    /** @since 900 */
    int32_t     lod = -1;
    std::string lodName() const { return lodName_; }
    bool        lodName(const std::string &lodName);

    Extent              extent;
    std::vector<Extent> sequenceExtents;

    GeosetEdit(uint32_t index, Errors *errors)
        : index(index), errors(errors) {}

    bool operator==(const GeosetEdit &rhs) const;
    bool operator!=(const GeosetEdit &rhs) const { return !operator==(rhs); }

    bool deleteVertex(uint32_t index, bool skipCheck = false);
    bool deleteTriangle(uint32_t index);
    bool deleteMatrixGroup(uint32_t index, bool skipCheck = false);

private:
    Errors *errors;

    uint32_t selectionFlags_ = 0;
    /** @since 900 */
    std::string lodName_;
};
}

#endif // GEOSETEDIT_H
