#include "geosetanimation.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "animationmap.h"

#include "model_edit.h"

namespace mdlx
{
GeosetAnimation::GeosetAnimation(BinaryStream &stream)
{
    const size_t size = stream.read<uint32_t>();

    alpha = stream.read<float>();
    flags = stream.read<uint32_t>();
    color = stream.read<vec3>();
    geosetId = stream.read<int32_t>();

    readAnimations(stream, size-baseByteLength);
}

void GeosetAnimation::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength());
    stream.write(alpha);
    stream.write(flags);
    stream.write(color);
    stream.write(geosetId);

    writeAnimations(stream);
}

size_t GeosetAnimation::getByteLength(uint32_t) const
{
    return baseByteLength + AnimatedObject::getByteLength();
}

GeosetAnimation::GeosetAnimation(TokenStream &stream)
{
    for (std::string token; !(token = iterAnimatedBlock(stream)).empty(); ) {
        if (token == "DropShadow") flags |= Flags::DropShadow;
        else if (token == "static Alpha") alpha = stream.read<float>();
        else if (token == "static Color") {
            flags |= Flags::Color;
            color = stream.readColor();
        }
        else if (token == "GeosetId") geosetId = stream.read<int32_t>();
        else if (readAnimation(stream, "GeosetAnimation", token)) {
            if (token == "Color") flags |= Flags::Color;
        }
        else stream.errors.fail(Errors::UnknownToken, "GeosetAnimation", token);
    }
}

void GeosetAnimation::write(TokenStream &stream) const
{
    stream.startBlock("GeosetAnim");

    if (flags & Flags::DropShadow) stream.writeFlag("DropShadow");

    if (!writeAnimation(stream, KGAO)) {
        stream.writeAttr("static Alpha", alpha);
    }

    if (flags & Flags::Color) {
        if (!writeAnimation(stream, KGAC) && (color[0] != 1.0F || color[1] != 1.0F || color[2] != 1.0F)) {
            stream.writeColor("static Color", color);
        }
    }

    stream.writeAttr("GeosetId", geosetId);

    stream.endBlock();
}

GeosetAnimation::GeosetAnimation(const std::shared_ptr<GeosetAnimationEdit> &geosetAnimation)
    : AnimatedObject(geosetAnimation)
{
    alpha = geosetAnimation->alpha;
    flags = geosetAnimation->flags();
    color = geosetAnimation->color;

    if (geosetAnimation->geoset) {
        geosetId = geosetAnimation->geoset->index;
    }
}

std::shared_ptr<GeosetAnimationEdit> GeosetAnimation::edit(uint32_t index, Errors *errors, ModelEdit &model)
{
    auto geosetAnimation = std::make_shared<GeosetAnimationEdit>(index, errors, model, this);

    geosetAnimation->alpha = alpha;
    geosetAnimation->flags(flags);
    geosetAnimation->color = color;

    if (geosetId >= 0 && geosetId < static_cast<int32_t>(model.geosets.size())) {
        geosetAnimation->geoset = model.geosets[geosetId];
    }

    return geosetAnimation;
}
}
