#ifndef GEOSETANIMATION_H
#define GEOSETANIMATION_H

#include "util.h"
#include "animatedobject.h"

namespace mdlx
{
/** Forward declare */
class GeosetAnimationEdit;

/**
 * @brief A geoset animation.
 */
class GeosetAnimation : public AnimatedObject
{
    static const size_t baseByteLength = 28;

    float       alpha = 1.0F;
    uint32_t    flags = 0;
    vec3        color = { 1.0F, 1.0F, 1.0F };
    int32_t     geosetId = -1;

public:
    enum Flags {
        DropShadow = 0x1,
        Color = 0x2,
    };

    GeosetAnimation(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    GeosetAnimation(TokenStream &stream);
    void write(TokenStream &stream) const;

    GeosetAnimation(const std::shared_ptr<GeosetAnimationEdit> &geosetAnimation);
    std::shared_ptr<GeosetAnimationEdit> edit(uint32_t index, Errors *errors, ModelEdit &model);
};
}

#endif // GEOSETANIMATION_H
