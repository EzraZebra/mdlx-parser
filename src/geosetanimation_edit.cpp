#include "geosetanimation_edit.h"
#include "geosetanimation.h"
#include "animation_edit.h"
#include "errors.h"

namespace mdlx
{
bool GeosetAnimationEdit::operator==(const GeosetAnimationEdit &rhs) const
{
    return alpha == rhs.alpha && color == rhs.color && flags_ == rhs.flags()
            && geoset == rhs.geoset && animations == rhs.animations;
}

bool GeosetAnimationEdit::flags(uint32_t flags)
{
    if (flags == 0 || flags & GeosetAnimation::DropShadow || flags & GeosetAnimation::Color) {
        flags_ = flags;
        return true;
    }

    errors->add(Errors::Value, "Geoset Animation Flags", std::to_string(flags));
    return false;
}
}
