#ifndef GEOSETANIMATIONEDIT_H
#define GEOSETANIMATIONEDIT_H

#include "animatedobject_edit.h"
#include "util.h"

namespace mdlx
{
/** Forward declare */
class GeosetEdit;

struct GeosetAnimationEdit : public AnimatedObjectEdit
{
    uint32_t index;

    float   alpha = 1.0F;
    vec3    color = { 1.0F, 1.0F, 1.0F };

    uint32_t    flags() const { return flags_; }
    bool        flags(uint32_t flags);

    std::shared_ptr<GeosetEdit> geoset;

    GeosetAnimationEdit(uint32_t index, Errors *errors, ModelEdit &model, AnimatedObject *animatedObject)
        : AnimatedObjectEdit(errors, model, animatedObject),
          index(index) {}

    bool operator==(const GeosetAnimationEdit &rhs) const;
    bool operator!=(const GeosetAnimationEdit &rhs) const { return !operator==(rhs); }

private:
    uint32_t flags_ = 0;
};
}

#endif // GEOSETANIMATIONEDIT_H
