#ifndef GLOBALSEQUENCEEDIT_H
#define GLOBALSEQUENCEEDIT_H

#include <cstdint>

namespace mdlx
{
struct GlobalSequenceEdit
{
    uint32_t index;
    uint32_t length;

    GlobalSequenceEdit(uint32_t index, uint32_t length)
        : index(index), length(length) {}
};
}

#endif // GLOBALSEQUENCEEDIT_H
