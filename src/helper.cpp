#include "helper.h"
#include "tokenstream.h"

#include "model_edit.h"

namespace mdlx
{
Helper::Helper(BinaryStream &stream)
    : GenericObject(stream) {}

void Helper::write(BinaryStream &stream) const
{
    GenericObject::write(stream);
}

size_t Helper::getByteLength(uint32_t) const
{
    return GenericObject::getByteLength();
}

Helper::Helper(TokenStream &stream)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        stream.errors.fail(Errors::UnknownToken, "Helper", token);
    }
}

void Helper::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "Helper");
    writeGenericAnimations(stream);
    stream.endBlock();
}

Helper::Helper(const std::shared_ptr<HelperEdit> &helper)
    : GenericObject(helper) {}

std::shared_ptr<HelperEdit> Helper::edit(Errors *errors, ModelEdit &model)
{
    return std::make_shared<HelperEdit>(GenericObject::edit(errors, model, this));
}
}
