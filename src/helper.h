#ifndef HELPER_H
#define HELPER_H

#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
struct HelperEdit;

/**
 * @brief A helper.
 */
class Helper : public GenericObject
{
public:
    Helper(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    Helper(TokenStream &stream);
    void write(TokenStream &stream) const;

    Helper(const std::shared_ptr<HelperEdit> &helper);
    std::shared_ptr<HelperEdit> edit(Errors *errors, ModelEdit &model);
};
}

#endif // HELPER_H
