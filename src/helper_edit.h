#ifndef HELPER_EDIT_H
#define HELPER_EDIT_H

#include "genericobject_edit.h"

namespace mdlx
{
struct HelperEdit : public GenericObjectEdit
{
    HelperEdit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}
};
}

#endif // HELPER_EDIT_H
