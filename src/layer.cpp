#include "layer.h"
#include "animation.h"
#include "animationmap.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "model_options.h"

#include "model_edit.h"

namespace mdlx
{
std::unordered_map<std::string, uint32_t> Layer::filterModeToMdx = {
    { "None", None },
    { "Transparent", Transparent },
    { "Blend", Blend },
    { "Additive", Additive },
    { "AddAlpha", AddAlpha },
    { "Modulate", Modulate },
    { "Modulate2x", Modulate2x },
};

std::unordered_map<uint32_t, std::string> Layer::filterModeToMdl = {
    { None, "None" },
    { Transparent, "Transparent" },
    { Blend, "Blend" },
    { Additive, "Additive" },
    { AddAlpha, "AddAlpha" },
    { Modulate, "Modulate" },
    { Modulate2x, "Modulate2x" },
};

Layer::Layer(BinaryStream &stream)
{
    const size_t start = stream.position();
    const size_t size = stream.read<uint32_t>();

    filterMode = stream.read<uint32_t>();
    shadingFlags = stream.read<uint32_t>();
    textureId = stream.read<int32_t>();
    textureAnimationId = stream.read<int32_t>();
    coordId = stream.read<uint32_t>();
    alpha = stream.read<float>();

    // Note that even though these fields were introduced in versions 900 and 1000 separately, the game does not offer backwards compatibility.
    if (stream.version >= v900) {
        emissiveGain = stream.read<float>();
    }

    if (stream.version >= v1000) {
        fresnelColor = stream.read<vec3>();
        fresnelOpacity = stream.read<float>();
        fresnelTeamColor = stream.read<float>();
    }

    readAnimations(stream, size - (stream.position() - start));
}

void Layer::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength(stream.version));
    stream.write(filterMode);
    stream.write(shadingFlags);
    stream.write(textureId);
    stream.write(textureAnimationId);
    stream.write(coordId);
    stream.write(alpha);

    if (stream.version >= v900) {
        stream.write(emissiveGain);
    }

    if (stream.version >= v1000) {
        stream.write(fresnelColor);
        stream.write(fresnelOpacity);
        stream.write(fresnelTeamColor);
    }

    writeAnimations(stream);
}

size_t Layer::getByteLength(uint32_t version) const
{
    size_t size = baseByteLength + AnimatedObject::getByteLength();

    if (version >= v900) size += 4;
    if (version >= v1000) size += baseByteLengthv1000;

    return size;
}

Layer::Layer(TokenStream &stream)
{
    for (std::string token; !(token = iterAnimatedBlock(stream)).empty(); ) {
        if (token == "FilterMode") filterMode = filterModeToMdx[stream.read<std::string>()];
        else if (token == "Unshaded") shadingFlags |= Unshaded;
        else if (token == "SphereEnvMap") shadingFlags |= SphereEnvMap;
        else if (token == "TwoSided") shadingFlags |= TwoSided;
        else if (token == "Unfogged") shadingFlags |= Unfogged;
        else if (token == "NoDepthTest")  shadingFlags |= NoDepthTest;
        else if (token == "NoDepthSet") shadingFlags |= NoDepthSet;
        else if (token == "Unlit") shadingFlags |= Unlit;
        else if (token == "static TextureID") textureId = stream.read<int32_t>();
        else if (token == "TVertexAnimId") textureAnimationId = stream.read<int32_t>();
        else if (token == "CoordId") coordId = stream.read<uint32_t>();
        else if (token == "static Alpha") alpha = stream.read<float>();
        else if (token == "static EmissiveGain" || token == "static Emissive") emissiveGain = stream.read<float>(); // "Emissive": RMS 0.4
        else if (token == "static FresnelColor") fresnelColor = stream.read<vec3>(); // Fresnel tokens RMS 0.4?
        else if (token == "static FresnelOpacity") fresnelOpacity = stream.read<float>();
        else if (token == "static FresnelTeamColor") emissiveGain = stream.read<float>();
        else if (!readAnimation(stream, "Layer", token)) {
            stream.errors.fail(Errors::UnknownToken, "Layer", token);
        }
    }
}

void Layer::write(TokenStream &stream) const
{
    stream.startBlock("Layer");

    stream.writeFlag("FilterMode", filterModeToMdl[filterMode]);

    if (shadingFlags & Unshaded) stream.writeFlag("Unshaded");
    if (shadingFlags & SphereEnvMap) stream.writeFlag("SphereEnvMap");
    if (shadingFlags & TwoSided) stream.writeFlag("TwoSided");
    if (shadingFlags & Unfogged) stream.writeFlag("Unfogged");
    if (shadingFlags & NoDepthTest) stream.writeFlag("NoDepthTest");
    if (shadingFlags & NoDepthSet) stream.writeFlag("NoDepthSet");
    if (stream.version >= v900 && shadingFlags & Unlit) {
        stream.writeFlag("Unlit");
    }

    if (!writeAnimation(stream, KMTF)) {
        stream.writeAttr("static TextureID", textureId);
    }

    if (textureAnimationId != -1) {
        stream.writeAttr("TVertexAnimId", textureAnimationId);
    }

    if (coordId != 0) {
        stream.writeAttr("CoordId", coordId);
    }

    if (!writeAnimation(stream, KMTA) && alpha != 1) {
        stream.writeAttr("static Alpha", alpha);
    }

    if (stream.version >= v900 && !writeAnimation(stream, KMTE) && emissiveGain != 1.0F) {
        stream.writeAttr("static EmissiveGain", emissiveGain);
    }

    if (stream.version >= v1000) {
        if (!writeAnimation(stream, KFC3) && fresnelColor != vec3{ 1.0F, 1.0F, 1.0F }) {
            stream.writeAttr("static FresnelColor", fresnelColor);
        }
        if (!writeAnimation(stream, KFCA) && fresnelOpacity != 0.0F) {
            stream.writeAttr("static FresnelOpacity", fresnelOpacity);
        }
        if (!writeAnimation(stream, KFTC) && fresnelTeamColor != 0.0F) {
            stream.writeAttr("static FresnelTeamColor", fresnelTeamColor);
        }
    }

    stream.endBlock();
}

Layer::Layer(const std::shared_ptr<LayerEdit> &layer)
    : AnimatedObject(layer)
{
    filterMode = layer->filterMode();
    shadingFlags = layer->shadingFlags();

    if (layer->texture) {
        textureId = layer->texture->index;
    }
    if (layer->textureAnimation) {
        textureAnimationId = layer->textureAnimation->index;
    }

    coordId = layer->coordId;
    alpha = layer->alpha;
    emissiveGain = layer->emissiveGain;
    fresnelColor = layer->fresnelColor;
    fresnelOpacity = layer->fresnelOpacity;
    fresnelTeamColor = layer->fresnelTeamColor;
}

std::shared_ptr<LayerEdit> Layer::edit(Errors *errors, ModelEdit &model)
{
    auto layer = std::make_shared<LayerEdit>(errors, model, this);

    layer->filterMode(filterMode);
    layer->shadingFlags(shadingFlags);

    if (textureId >= 0 && textureId < static_cast<int32_t>(model.textures.size())) {
        layer->texture = model.textures[textureId];
    }
    if (textureAnimationId >= 0 && textureAnimationId < static_cast<int32_t>(model.textureAnimations.size())) {
        layer->textureAnimation = model.textureAnimations[textureAnimationId];
    }

    layer->coordId = coordId;
    layer->alpha = alpha;
    layer->emissiveGain = emissiveGain;
    layer->fresnelColor = fresnelColor;
    layer->fresnelOpacity = fresnelOpacity;
    layer->fresnelTeamColor = fresnelTeamColor;

    return layer;
}
}
