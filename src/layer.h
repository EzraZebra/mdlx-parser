#ifndef LAYER_H
#define LAYER_H

#include "util.h"
#include "animatedobject.h"

#include <unordered_map>

namespace mdlx
{
/** Forward declare */
class ModelEdit;
class LayerEdit;

/**
 * @brief A layer.
 */
class Layer : public AnimatedObject
{
    static const size_t baseByteLength = 28;
    static const size_t baseByteLengthv1000 = 20;

    enum FilterMode {
        None        = 0,
        Transparent = 1,
        Blend       = 2,
        Additive    = 3,
        AddAlpha    = 4,
        Modulate    = 5,
        Modulate2x  = 6,
    };

    static std::unordered_map<std::string, uint32_t> filterModeToMdx;
    static std::unordered_map<uint32_t, std::string> filterModeToMdl;

    uint32_t    filterMode = 0, shadingFlags = 0;
    int32_t     textureId = -1, textureAnimationId = -1;
    uint32_t    coordId = 0;
    float       alpha = 1.0F;

    /** @since 900 */
    float emissiveGain = 1.0F;

    /** @since 1000 */
    vec3    fresnelColor { 1.0F, 1.0F, 1.0F };
    float   fresnelOpacity = 0.0F, fresnelTeamColor = 0.0F;

public:
    enum ShadingFlags {
        Unshaded        = 0x1,
        SphereEnvMap    = 0x2,
        TwoSided        = 0x10,
        Unfogged        = 0x20,
        NoDepthTest     = 0x40,
        NoDepthSet      = 0x80,
        Unlit           = 0x100,
    };

    Layer(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t version) const;

    Layer(TokenStream &stream);
    void write(TokenStream &stream) const;

    Layer(const std::shared_ptr<LayerEdit> &layer);
    std::shared_ptr<LayerEdit> edit(Errors* errors, ModelEdit &model);
};
}

#endif // LAYER_H
