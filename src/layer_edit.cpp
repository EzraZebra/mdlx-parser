#include "layer_edit.h"
#include "layer.h"
#include "animation_edit.h"
#include "texture_edit.h"
#include "textureanimation_edit.h"
#include "errors.h"

namespace mdlx
{
bool LayerEdit::operator==(const LayerEdit &rhs) const
{
    return filterMode_ == rhs.filterMode() && shadingFlags_ == rhs.shadingFlags()
            && coordId == rhs.coordId && compf(alpha, rhs.alpha)
            && compf(emissiveGain, rhs.emissiveGain) && compf(fresnelColor, rhs.fresnelColor)
            && compf(fresnelOpacity, rhs.fresnelOpacity) && compf(fresnelTeamColor, rhs.fresnelTeamColor)
            && ((bool)textureAnimation == (bool)rhs.textureAnimation && (!textureAnimation || *textureAnimation == *rhs.textureAnimation))
            && ((bool)texture == (bool)rhs.texture && (!texture || *texture == *rhs.texture))
            && animations == rhs.animations;
}

bool operator==(const std::vector<std::shared_ptr<LayerEdit>> &lhs, const std::vector<std::shared_ptr<LayerEdit>> &rhs)
{
    if (lhs.size() == rhs.size()) {
        for (size_t i=0; i<lhs.size(); i++) {
            if (*lhs[i] != *rhs[i]) {
                return false;
            }
        }

        return true;
    }

    return false;
}

bool operator!=(const std::vector<std::shared_ptr<LayerEdit>> &lhs, const std::vector<std::shared_ptr<LayerEdit>> &rhs)
{
    return !operator==(lhs, rhs);
}

bool LayerEdit::filterMode(uint32_t mode)
{
    if (mode <= 6) {
        filterMode_ = mode;
        return true;
    }

    errors->add(Errors::Value, "Layer Filter Mode", std::to_string(mode));
    return false;
}

bool LayerEdit::shadingFlags(uint32_t flags)
{
    if (flags == 0 || flags & Layer::Unshaded || flags & Layer::SphereEnvMap || flags & Layer::TwoSided
            || flags & Layer::Unfogged || flags & Layer::NoDepthTest || flags & Layer::NoDepthSet || flags & Layer::Unlit)
    {
        shadingFlags_ = flags;
        return true;
    }

    errors->add(Errors::Value, "Layer Shading Flags", std::to_string(flags));
    return false;
}
}
