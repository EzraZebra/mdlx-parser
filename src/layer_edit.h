#ifndef LAYEREDIT_H
#define LAYEREDIT_H

#include "util.h"
#include "animatedobject_edit.h"

namespace mdlx
{
/** Forward declare */
class ModelEdit;
class TextureEdit;
class TextureAnimationEdit;
class Errors;

struct LayerEdit : public AnimatedObjectEdit
{
    uint32_t    filterMode() const { return filterMode_; }
    bool        filterMode(uint32_t mode);
    uint32_t    shadingFlags() const { return shadingFlags_; }
    bool        shadingFlags(uint32_t flags);

    std::shared_ptr<TextureEdit>            texture;
    std::shared_ptr<TextureAnimationEdit>   textureAnimation;

    uint32_t    coordId = 0;
    float       alpha = 1.0F;

    /** @since 900 */
    float emissiveGain = 1.0F;

    /** @since 1000 */
    vec3    fresnelColor { 1.0F, 1.0F, 1.0F };
    float   fresnelOpacity = 0.0F, fresnelTeamColor = 0.0F;

    LayerEdit(Errors *errors, ModelEdit &model, AnimatedObject *animatedObject)
        : AnimatedObjectEdit(errors, model, animatedObject) {}

    bool operator==(const LayerEdit &rhs) const;
    bool operator!=(const LayerEdit &rhs) const { return !operator==(rhs); }

private:
    uint32_t filterMode_ = 0, shadingFlags_ = 0;
};

extern bool operator==(const std::vector<std::shared_ptr<LayerEdit>> &lhs, const std::vector<std::shared_ptr<LayerEdit>> &rhs);
extern bool operator!=(const std::vector<std::shared_ptr<LayerEdit>> &lhs, const std::vector<std::shared_ptr<LayerEdit>> &rhs);
}

#endif // LAYEREDIT_H
