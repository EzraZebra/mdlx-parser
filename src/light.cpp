#include "light.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "animationmap.h"

#include "model_edit.h"

namespace mdlx
{
Light::Light(BinaryStream &stream)
    : GenericObject(ObjectFlags::Light)
{
    const size_t start = stream.position();
    const size_t size = stream.read<uint32_t>();

    read(stream);

    type = stream.read<uint32_t>();
    attenuation = stream.read<vec2>();
    color = stream.read<vec3>();
    intensity = stream.read<float>();
    ambientColor = stream.read<vec3>();
    ambientIntensity = stream.read<float>();

    readAnimations(stream, size - (stream.position() - start));
}

void Light::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength());

    GenericObject::write(stream);

    stream.write(type);
    stream.write(attenuation);
    stream.write(color);
    stream.write(intensity);
    stream.write(ambientColor);
    stream.write(ambientIntensity);

    writeNonGenericAnimationChunks(stream);
}

size_t Light::getByteLength(uint32_t) const
{
    return baseByteLength + GenericObject::getByteLength();
}

Light::Light(TokenStream &stream)
    : GenericObject(ObjectFlags::Light)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        if (token == "Omnidirectional") type = Type::Omni;
        else if (token == "Directional") type = Type::Directional;
        else if (token == "Ambient") type = Type::Ambient;
        else if (token == "static AttenuationStart") attenuation[0] = stream.read<float>();
        else if (token == "static AttenuationEnd") attenuation[1] = stream.read<float>();
        else if (token == "static Intensity") intensity = stream.read<float>();
        else if (token == "static Color") color = stream.readColor();
        else if (token == "static AmbIntensity") ambientIntensity = stream.read<float>();
        else if (token == "static AmbColor") ambientColor = stream.readColor();
        else if (!readAnimation(stream, "Light", token)) {
            stream.errors.fail(Errors::UnknownToken, "Light", token);
        }
    }
}

void Light::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "Light");

    switch(type) {
    case Type::Omni: stream.writeFlag("Omnidirectional");
        break;
    case Type::Directional: stream.writeFlag("Directional");
        break;
    case Type::Ambient: stream.writeFlag("Ambient");
        break;
    }

    if (!writeAnimation(stream, KLAS)) {
        stream.writeAttr("static AttenuationStart", attenuation[0]);
    }

    if (!writeAnimation(stream, KLAE)) {
        stream.writeAttr("static AttenuationEnd", attenuation[1]);
    }

    if (!writeAnimation(stream, KLAI)) {
        stream.writeAttr("static Intensity", intensity);
    }

    if (!writeAnimation(stream, KLAC)) {
        stream.writeColor("static Color", color);
    }

    if (!writeAnimation(stream, KLBI)) {
        stream.writeAttr("static AmbIntensity", ambientIntensity);
    }

    if (!writeAnimation(stream, KLBC)) {
        stream.writeColor("static AmbColor", ambientColor);
    }

    writeAnimation(stream, KLAV);

    writeGenericAnimations(stream);
    stream.endBlock();
}

Light::Light(const std::shared_ptr<LightEdit> &light)
    : GenericObject(light)
{
    type = light->type();
    attenuation = light->attenuation;
    color = light->color;
    intensity = light->intensity;
    ambientColor = light->ambientColor;
    ambientIntensity = light->ambientIntensity;
}

std::shared_ptr<LightEdit> Light::edit(Errors *errors, ModelEdit &model)
{
    auto light = std::make_shared<LightEdit>(GenericObject::edit(errors, model, this));

    light->type(type);
    light->attenuation = attenuation;
    light->color = color;
    light->intensity = intensity;
    light->ambientColor = ambientColor;
    light->ambientIntensity = ambientIntensity;

    return light;
}
}
