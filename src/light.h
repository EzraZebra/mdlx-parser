#ifndef LIGHT_H
#define LIGHT_H

#include "util.h"
#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
class LightEdit;

/**
 * @brief A light.
 */
class Light : public GenericObject
{
    static const size_t baseByteLength = 48;

    int32_t type = -1;
    vec2    attenuation{};
    vec3    color{};
    float   intensity = 0.0F;
    vec3    ambientColor{};
    float   ambientIntensity = 0.0F;

public:
    enum Type {
        Omni        = 0,
        Directional = 1,
        Ambient     = 2,
    };

    Light(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    Light(TokenStream &stream);
    void write(TokenStream &stream) const;

    Light(const std::shared_ptr<LightEdit> &light);
    std::shared_ptr<LightEdit> edit(Errors *errors, ModelEdit &model);
};
}

#endif // LIGHT_H
