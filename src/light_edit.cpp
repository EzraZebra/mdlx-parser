#include "light_edit.h"
#include "light.h"
#include "errors.h"

namespace mdlx
{
bool LightEdit::type(int32_t type)
{
    if (type >= Light::Omni && type <= Light::Ambient) {
        type_ = type;
        return true;
    }

    errors->add(Errors::Value, "Light Flags", std::to_string(type));
    return false;
}
}
