#ifndef LIGHTEDIT_H
#define LIGHTEDIT_H

#include "genericobject_edit.h"
#include "util.h"

namespace mdlx
{
struct LightEdit : public GenericObjectEdit
{
    int32_t type() const { return type_; }
    bool    type(int32_t type);

    vec2    attenuation{};
    vec3    color{};
    float   intensity = 0.0F;
    vec3    ambientColor{};
    float   ambientIntensity = 0.0F;

    LightEdit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}

private:
    int32_t type_ = -1;
};
}

#endif // LIGHTEDIT_H
