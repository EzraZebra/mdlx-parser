#include "material.h"
#include "layer.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "model_options.h"

#include "model_edit.h"

namespace mdlx
{
Material::Material(BinaryStream &stream)
{
    stream.skip(); // Don't care about the size.

    priorityPlane = stream.read<int32_t>();
    flags = stream.read<uint32_t>();


    if (stream.version >= v900) shader = stream.readString(strSizeSmall);

    stream.skip(); // LAYS
    const size_t count = stream.read<uint32_t>();
    layers.reserve(count);
    for (uint32_t i=0; i<count; i++) {
        layers.emplace_back(Layer(stream));
    }
}

void Material::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength(stream.version));
    stream.write(priorityPlane);
    stream.write(flags);

    if (stream.version >= v900) {
        stream.writeString(shader);
        stream.skip(strSizeSmall - shader.length());
    }

    stream.write(LAYS);
    stream.write<uint32_t>(layers.size());
    for (const auto &layer : layers) layer.write(stream);
}

size_t Material::getByteLength(uint32_t version) const
{
    size_t size = baseByteLength;
    if (version >= v900) size += baseByteLengthv900;
    for (const auto &layer : layers) size += layer.getByteLength(version);

    return size;
}

Material::Material(TokenStream &stream)
{
    for (std::string token; !(token = stream.iterBlock()).empty(); ) {
        if (token == "ConstantColor") flags |= Flags::ConstantColor;
        else if (token == "TwoSided") flags |= Flags::TwoSided;
        else if (token == "SortPrimsNearZ") flags |= Flags::SortPrimsNearZ;
        else if (token == "SortPrimsFarZ") flags |= Flags::SortPrimsFarZ;
        else if (token == "FullResolution") flags |= Flags::FullResolution;
        else if (token == "PriorityPlane") priorityPlane = stream.read<int32_t>();
        else if (token == "Shader") shader = stream.read<std::string>();
        else if (token == "Layer") layers.emplace_back(Layer(stream));
        else stream.errors.fail(Errors::UnknownToken, "Material", token);
    }
}

void Material::write(TokenStream &stream) const
{
    stream.startBlock("Material");

    if (flags & Flags::ConstantColor) stream.writeFlag("ConstantColor");
    if (stream.version >= v900 && flags & Flags::TwoSided) {
        stream.writeFlag("TwoSided");
    }
    if (flags & Flags::SortPrimsNearZ) stream.writeFlag("SortPrimsNearZ");
    if (flags & Flags::SortPrimsFarZ) stream.writeFlag("SortPrimsFarZ");
    if (flags & Flags::FullResolution) stream.writeFlag("FullResolution");

    if (priorityPlane != 0) stream.writeAttr("PriorityPlane", priorityPlane);

    if (stream.version >= v900) {
        stream.writeAttr("Shader", shader);
    }

    for (const auto &layer : layers) layer.write(stream);

    stream.endBlock();
}

Material::Material(const std::shared_ptr<MaterialEdit> &material)
{
    priorityPlane = material->priorityPlane;
    flags = material->flags();
    shader = material->shader();

    layers.reserve(material->layers.size());
    for (const auto &layer : material->layers) {
        layers.emplace_back(layer);
    }
}

std::shared_ptr<MaterialEdit> Material::edit(uint32_t index, Errors *errors, ModelEdit &model)
{
    auto material = std::make_shared<MaterialEdit>(index, errors);

    material->priorityPlane = priorityPlane;
    material->flags(flags);
    material->shader(shader);

    material->layers.reserve(layers.size());
    for (auto &layer : layers) {
        material->layers.emplace_back(layer.edit(errors, model));
    }

    return material;
}
}
