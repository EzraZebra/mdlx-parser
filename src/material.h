#ifndef MATERIAL_H
#define MATERIAL_H

#include <vector>
#include <string>
#include <memory>

namespace mdlx
{
/** Forward declare */
class BinaryStream;
class TokenStream;
class Layer;
class ModelEdit;
class MaterialEdit;
class Errors;

/**
 * @brief A material
 */
class Material
{
    static const size_t baseByteLength = 20;
    static const size_t baseByteLengthv900 = 80;

    static const uint32_t LAYS = 0x5359414c;

    int32_t     priorityPlane = 0;
    uint32_t    flags = 0;

    /** @since 900 */
    std::string shader;

    std::vector<Layer> layers;

public:
    enum Flags {
        ConstantColor   = 0x1,
        TwoSided        = 0x2,
        SortPrimsNearZ  = 0x8,
        SortPrimsFarZ   = 0x10,
        FullResolution  = 0x20,
    };

    Material(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t version) const;

    Material(TokenStream &stream);
    void write(TokenStream &stream) const;

    Material(const std::shared_ptr<MaterialEdit> &material);
    std::shared_ptr<MaterialEdit> edit(uint32_t index, Errors *errors, ModelEdit &model);
};
}

#endif // MATERIAL_H
