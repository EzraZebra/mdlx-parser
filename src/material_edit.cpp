#include "material_edit.h"
#include "material.h"
#include "errors.h"

namespace mdlx
{
bool MaterialEdit::operator==(const MaterialEdit &rhs) const
{
    return priorityPlane == rhs.priorityPlane && flags_ == rhs.flags() && shader_ == rhs.shader() && layers == rhs.layers;
}

bool MaterialEdit::flags(uint32_t flags)
{
    if (flags == 0 || flags & Material::ConstantColor || flags & Material::TwoSided
            || flags & Material::SortPrimsNearZ || flags & Material::SortPrimsFarZ || flags & Material::FullResolution)
    {
        flags_ = flags;
        return true;
    }

    errors->add(Errors::Value, "Material Flags", std::to_string(flags));
    return false;
}

bool MaterialEdit::shader(const std::string &shader)
{
    if (shader.length() <= strSizeSmall) {
        shader_ = shader;
        return true;
    }

    errors->add(Errors::Value, "Material Shader", shader);
    return false;
}
}
