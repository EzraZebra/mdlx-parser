#ifndef MATERIALEDIT_H
#define MATERIALEDIT_H

#include "layer_edit.h"

#include <string>
#include <vector>

namespace mdlx
{
/** Forward declare */
class Errors;

struct MaterialEdit
{
    uint32_t index;

    int32_t priorityPlane;

    uint32_t    flags() const { return flags_; }
    bool        flags(uint32_t flags);
    std::string shader() const { return shader_; }
    bool        shader(const std::string &shader);

    std::vector<std::shared_ptr<LayerEdit>> layers;

    MaterialEdit(uint32_t index, Errors *errors)
        : index(index), errors(errors) {}

    bool operator==(const MaterialEdit &rhs) const;
    bool operator!=(const MaterialEdit &rhs) const { return !operator==(rhs); }

private:
    Errors *errors;

    uint32_t    flags_;
    std::string shader_;
};
}

#endif // MATERIALEDIT_H
