#include "model.h"
#include "model_options.h"
#include "model_tags.h"
#include "binarystream.h"
#include "tokenstream.h"

namespace mdlx
{
Model::Model(const std::string &path)
{
    load(path);
}

std::string Model::getExt(const std::string &path)
{
    if (path.empty()) {
        errors.fail(Errors::File, "Empty path provided");
    }

    size_t s = path.find_last_of('.');
    size_t f = path.find_last_of("/\\");

    if (s == std::string::npos || s == path.length()-1 || (f != std::string::npos && s < f)) {
        errors.fail(Errors::File, "File without extension.");
    }

    std::string ext = path.substr(s+1);
    for (auto &c : ext) c = static_cast<char>(std::tolower(c));

    return ext;
}

void Model::load(const std::string &path)
{
    std::string ext = getExt(path);

    if (ext == "mdx") {
        BinaryStream stream(path, errors);

        if (stream.read<uint32_t>() != MDLX) {
            errors.fail(Errors::File, "Not a valid MDX file");
        }

        while (stream.remaining() > 0) {
            const auto tag = stream.read<uint32_t>();
            const auto size = stream.read<uint32_t>();

            switch(tag) {
            case VERS:
                version = stream.read<uint32_t>();
                stream.version = version;
                break;
            case MODL:
                name = stream.readString(strSizeSmall);
                animationFile = stream.readString(strSize);
                extent.read(stream);
                blendTime = stream.read<uint32_t>();
                break;
            case SEQS: loadStaticObjects(stream, sequences, size/Sequence::byteLength);
                break;
            case GLBS: globalSequences = stream.readVector<uint32_t>(size/4);
                break;
            case MTLS: loadDynamicObjects(stream, materials, size);
                break;
            case TEXS: loadStaticObjects(stream, textures, size/Texture::byteLength);
                break;
            case TXAN: loadDynamicObjects(stream, textureAnimations, size);
                break;
            case GEOS: loadDynamicObjects(stream, geosets, size);
                break;
            case GEOA: loadDynamicObjects(stream, geosetAnimations, size);
                break;
            case BONE: loadDynamicObjects(stream, bones, size);
                break;
            case LITE: loadDynamicObjects(stream, lights, size);
                break;
            case HELP: loadDynamicObjects(stream, helpers, size);
                break;
            case ATCH: loadDynamicObjects(stream, attachments, size);
                break;
            case PIVT: pivotPoints = stream.readVector<vec3>(size/12);
                break;
            case PREM: loadDynamicObjects(stream, particleEmitters, size);
                break;
            case PRE2: loadDynamicObjects(stream, particleEmitters2, size);
                break;
            case CORN: loadDynamicObjects(stream, particleEmittersPopcorn, size);
                break;
            case RIBB: loadDynamicObjects(stream, ribbonEmitters, size);
                break;
            case SNDS: loadStaticObjects(stream, soundEmitters, size/SoundEmitter::byteLength);
                break;
            case CAMS: loadDynamicObjects(stream, cameras, size);
                break;
            case EVTS: loadDynamicObjects(stream, eventObjects, size);
                break;
            case CLID: loadDynamicObjects(stream, collisionShapes, size);
                break;
            case FAFX:
                faceFx.emplace_back(std::array<std::string, 2>{ stream.readString(strSizeSmall), stream.readString(strSize) });
                break;
            case BPOS: bindPose = stream.readVector<vec12>(stream.read<uint32_t>());
                break;
            default: unknownChunks.emplace_back(UnknownChunk(stream, size, tag));
                break;
            }
        }
    }
    else if (ext == "mdl") {
        TokenStream stream(path, errors);

        for (std::string token; !(token = stream.readToken(false)).empty(); ) {
            if (token == "Version") {
                for (std::string token; !(token = stream.iterBlock()).empty(); ) {
                    if (token == "FormatVersion") {
                        version = stream.read<uint32_t>();
                        stream.version = version;
                    }
                    else errors.fail(Errors::UnknownToken, "Version", token);
                }
            }
            else if (token == "Model") {
                name = stream.read<std::string>();

                for (std::string token; !(token = stream.iterBlock()).empty(); ) {
                    if (token.rfind("Num", 0) == 0) {
                        const size_t count = stream.read<uint32_t>();

                        if (token == "NumGeosets") geosets.reserve(count);
                        else if (token == "NumGeosetAnims") geosetAnimations.reserve(count);
                        else if (token == "NumHelpers") helpers.reserve(count);
                        else if (token == "NumLights") lights.reserve(count);
                        else if (token == "NumBones") bones.reserve(count);
                        else if (token == "NumSoundEmitters") soundEmitters.reserve(count); // deprecated
                        else if (token == "NumAttachments") attachments.reserve(count);
                        else if (token == "NumParticleEmitters") particleEmitters.reserve(count);
                        else if (token == "NumParticleEmitters2") particleEmitters2.reserve(count);
                        else if (token == "NumParticleEmittersPopcorn" || token == "NumPopcornFxEmitters"){ // "NumPopcornFxEmitters": RMS 0.4
                            particleEmittersPopcorn.reserve(count);
                        }
                        else if (token == "NumRibbonEmitters") ribbonEmitters.reserve(count);
                        else if (token == "NumEvents") eventObjects.reserve(count);
                        else if (token == "NumFaceFX") faceFx.reserve(count);
                        else errors.fail(Errors::UnknownToken, "Num", token);
                    }
                    else if (token == "BlendTime") blendTime = stream.read<uint32_t>();
                    else if (!extent.read(stream, token)) {
                        if (token == "AnimationFile") animationFile = stream.read<std::string>();
                        else errors.fail(Errors::UnknownToken, "Model", token);
                    }
                }
            }
            else if (token == "Sequences") loadNumberedObjectBlock(stream, sequences, "Anim");
            else if (token == "GlobalSequences") {
                globalSequences.reserve(stream.read<uint32_t>());

                for (std::string token; !(token = stream.iterBlock()).empty(); ) {
                    if (token == "Duration") globalSequences.emplace_back(stream.read<uint32_t>());
                    else errors.fail(Errors::UnknownToken, "GlobalSequences", token);
                }
            }
            else if (token == "Textures") loadNumberedObjectBlock(stream, textures, "Bitmap");
            else if (token == "Materials") loadNumberedObjectBlock(stream, materials, "Material");
            else if (token == "TextureAnims") loadNumberedObjectBlock(stream, textureAnimations, "TVertexAnim");
            else if (token == "Geoset") geosets.emplace_back(Geoset(stream));
            else if (token == "GeosetAnim") geosetAnimations.emplace_back(GeosetAnimation(stream));
            else if (token == "Bone") bones.emplace_back(Bone(stream));
            else if (token == "Light") lights.emplace_back(Light(stream));
            else if (token == "Helper") helpers.emplace_back(Helper(stream));
            else if (token == "Attachment") attachments.emplace_back(Attachment(stream));
            else if (token == "PivotPoints") {
                auto count = stream.read<uint32_t>();
                pivotPoints.reserve(count);

                stream.readToken(); // {
                for (uint32_t i=0; i<count; i++) {
                    pivotPoints.emplace_back(stream.read<vec3>());
                }
                stream.readToken(); // }
            }
            else if (token == "ParticleEmitter") particleEmitters.emplace_back(ParticleEmitter(stream));
            else if (token == "ParticleEmitter2") particleEmitters2.emplace_back(ParticleEmitter2(stream));
            else if (token == "ParticleEmitterPopcorn" || token == "PopcornFxEmitter") { // "PopcornFxEmitter": RMS 0.4
                particleEmittersPopcorn.emplace_back(ParticleEmitterPopcorn(stream));
            }
            else if (token == "RibbonEmitter") ribbonEmitters.emplace_back(RibbonEmitter(stream));
            else if (token == "SoundTrack") soundEmitters.emplace_back(SoundEmitter(stream));
            else if (token == "Camera") cameras.emplace_back(Camera(stream));
            else if (token == "EventObject") eventObjects.emplace_back(EventObject(stream));
            else if (token == "CollisionShape") collisionShapes.emplace_back(CollisionShape(stream));
            else if (token == "FaceFX") {
                std::array<std::string, 2> newFx;
                newFx[0] = stream.read<std::string>();

                for (std::string token; !(token = stream.iterBlock()).empty(); ) {
                    if (token == "Path") newFx[1] = stream.read<std::string>();
                    else errors.fail(Errors::UnknownToken, "FaceFX", token);
                }

                faceFx.emplace_back(newFx);
            }
            else if (token == "BindPose") {
                if (stream.peekToken() != "{") stream.readToken(); // RMS 0.4: BindPose Count

                for (std::string token; !(token = stream.iterBlock()).empty(); ) {
                    if (token == "Matrices") {
                        stream.read<uint32_t>(); // count (12)
                        stream.readToken(); // {

                        bindPose.emplace_back(stream.read<vec12>());

                        stream.readToken(); // }
                    }
                    else if (token == "Matrix") { // RMS 0.4
                        stream.readToken(); // {

                        vec12 newBindPose;
                        for (int i=0, j=0; i<3; i++) {
                            auto arr = stream.read<vec4>();
                            for (const auto &val : arr) {
                                newBindPose[j] = val;
                                j++;
                            }
                        }
                        bindPose.emplace_back(newBindPose);

                        stream.readToken(); // }
                    }
                    else errors.fail(Errors::UnknownToken, name, token);
                }
            }
            else stream.errors.fail(Errors::UnsupportedBlock, token);
        }
    }
    else errors.fail(Errors::File, "Invalid input file extension: " + ext);
}

template<typename T>
void Model::loadStaticObjects(BinaryStream &stream, std::vector<T> &objects, size_t count) const
{
    objects.reserve(count);
    for (size_t i=0; i<count; i++) {
        objects.emplace_back(T(stream));
    }
}

template<typename T>
void Model::loadDynamicObjects(BinaryStream &stream, std::vector<T> &objects, size_t size) const
{
    for (size_t end = stream.position() + size; stream.position() < end; ) {
        objects.emplace_back(T(stream));
    }
}

template<typename T>
void Model::loadNumberedObjectBlock(TokenStream &stream, std::vector<T> &objects, const std::string &name)
{
    objects.reserve(stream.read<uint32_t>());

    for (std::string token; !(token = stream.iterBlock()).empty(); ) {
        if (token == name) objects.emplace_back(T(stream));
        else errors.fail(Errors::UnknownToken, name, token);
    }
}

void Model::save(const std::string &path, int options)
{
    this->options = options;

    int origVersion = this->version;
    uint32_t version = this->version;

    if (options & Flag::v800) version = v800;
    else if (options & Flag::v900) version = v900;
    else if (options & Flag::v1000) version = v1000;

    this->version = version;

    std::string ext = getExt(path);

    if (ext == "mdx") {
        BinaryStream stream(getByteLength(version), errors, version, options);

        stream.write(MDLX);

        stream.write(VERS);
        stream.write<uint32_t>(4);
        stream.write(version);

        stream.write<uint32_t>(MODL);
        stream.write<uint32_t>(modelChunkByteLength);
        stream.writeString(name);
        stream.skip(strSizeSmall - name.length());
        stream.writeString(animationFile);
        stream.skip(strSize - animationFile.length());
        extent.write(stream);
        stream.write(blendTime);

        saveStaticObjectChunk(stream, sequences, SEQS, Sequence::byteLength);

        if (!globalSequences.empty()) {
            stream.write(GLBS);
            stream.write<uint32_t>(vectorByteLength(globalSequences));
            stream.writeVector(globalSequences);
        }

        saveDynamicObjectChunk(stream, materials, MTLS);
        saveStaticObjectChunk(stream, textures, TEXS, Texture::byteLength);
        saveDynamicObjectChunk(stream, textureAnimations, TXAN);
        saveDynamicObjectChunk(stream, geosets, GEOS);
        saveDynamicObjectChunk(stream, geosetAnimations, GEOA);
        saveDynamicObjectChunk(stream, bones, BONE);
        saveDynamicObjectChunk(stream, lights, LITE);
        saveDynamicObjectChunk(stream, helpers, HELP);
        saveDynamicObjectChunk(stream, attachments, ATCH);

        if (!pivotPoints.empty()) {
            stream.write(PIVT);
            stream.write<uint32_t>(vectorByteLength(pivotPoints));
            stream.writeVector(pivotPoints);
        }

        saveDynamicObjectChunk(stream, particleEmitters, PREM);
        saveDynamicObjectChunk(stream, particleEmitters2, PRE2);

        if (version >= v900) saveDynamicObjectChunk(stream, particleEmittersPopcorn, CORN);

        saveDynamicObjectChunk(stream, ribbonEmitters, RIBB);
        saveStaticObjectChunk(stream, soundEmitters, SNDS, SoundEmitter::byteLength);
        saveDynamicObjectChunk(stream, cameras, CAMS);
        saveDynamicObjectChunk(stream, eventObjects, EVTS);
        saveDynamicObjectChunk(stream, collisionShapes, CLID);

        if (version >= v900) {
            for (const auto &fx : faceFx) {
                stream.write(FAFX);
                stream.write<uint32_t>(strSizeSmall + strSize);
                stream.writeString(fx[0]);
                stream.skip(strSizeSmall - fx[0].length());
                stream.writeString(fx[1]);
                stream.skip(strSize - fx[1].length());
            }

            if (!bindPose.empty()) {
                stream.write(BPOS);
                stream.write<uint32_t>(4 + vectorByteLength(bindPose));
                stream.write<uint32_t>(bindPose.size());
                stream.writeVector(bindPose);
            }
        }

        for (const auto &chunk : unknownChunks) chunk.write(stream);

        stream.writeFile(path);
    }
    else if (ext == "mdl") {
        TokenStream stream(errors, version, options);

        stream.startBlock("Version");
        stream.writeAttr("FormatVersion", version);
        stream.endBlock();

        stream.startBlock("Model", name);

        // Don't know if this list is complete.
        if (!geosets.empty()) {
            stream.writeAttr("NumGeosets", geosets.size());
        }
        if (!geosetAnimations.empty()) {
            stream.writeAttr("NumGeosetAnims", geosetAnimations.size());
        }
        if (!helpers.empty()) {
            stream.writeAttr("NumHelpers", helpers.size());
        }
        if (!lights.empty()) {
            stream.writeAttr("NumLights", lights.size());
        }
        if (!bones.empty()) {
            stream.writeAttr("NumBones", bones.size());
        }
        if (!attachments.empty()) {
            stream.writeAttr("NumAttachments", attachments.size());
        }
        if (!particleEmitters.empty()) {
            stream.writeAttr("NumParticleEmitters", particleEmitters.size());
        }
        if (!particleEmitters2.empty()) {
            stream.writeAttr("NumParticleEmitters2", particleEmitters2.size());
        }
        if (version >= v900 && !particleEmittersPopcorn.empty()) {
            stream.writeAttr("NumParticleEmittersPopcorn", particleEmittersPopcorn.size());
        }
        if (!ribbonEmitters.empty()) {
            stream.writeAttr("NumRibbonEmitters", ribbonEmitters.size());
        }
        if (!soundEmitters.empty()) {
            stream.writeAttr("NumSoundEmitters", soundEmitters.size());
        }
        if (!eventObjects.empty()) {
            stream.writeAttr("NumEvents", eventObjects.size());
        }
        if (version >= v900 && !faceFx.empty()) {
            stream.writeAttr("NumFaceFX", faceFx.size());
        }

        stream.writeAttr("BlendTime", blendTime);
        extent.write(stream);

        if (!animationFile.empty()) {
            stream.writeAttr("AnimationFile", animationFile);
        }

        stream.endBlock();

        saveStaticObjectsBlock(stream, sequences, "Sequences");

        if (!globalSequences.empty()) {
            stream.startBlock("GlobalSequences", globalSequences.size());
            for (const auto &globalSequence : globalSequences) {
                stream.writeAttr("Duration", globalSequence);
            }
            stream.endBlock();
        }

        saveStaticObjectsBlock(stream, textures, "Textures");
        saveStaticObjectsBlock(stream, materials, "Materials");
        saveStaticObjectsBlock(stream, textureAnimations, "TextureAnims");
        for (const auto &geoset : geosets) geoset.write(stream);
        for (const auto &geosetAnimation : geosetAnimations) geosetAnimation.write(stream);
        for (const auto &bone : bones) bone.write(stream);
        for (const auto &light : lights) light.write(stream);
        for (const auto &helper : helpers) helper.write(stream);
        for (const auto &attachment : attachments) attachment.write(stream);
        if (!pivotPoints.empty()) stream.writeVectorBlock("PivotPoints", pivotPoints);
        for (const auto &particleEmitter : particleEmitters) particleEmitter.write(stream);
        for (const auto &particleEmitter2 : particleEmitters2) particleEmitter2.write(stream);

        if (version >= v900) {
            for (const auto &particleEmitterPopcorn : particleEmittersPopcorn) {
                particleEmitterPopcorn.write(stream);
            }
        }

        for (const auto &ribbonEmitter : ribbonEmitters) ribbonEmitter.write(stream);
        for (const auto &soundEmitter : soundEmitters) soundEmitter.write(stream);
        for (const auto &camera : cameras) camera.write(stream);
        for (const auto &eventObject : eventObjects) eventObject.write(stream);
        for (const auto &collisionShape : collisionShapes) collisionShape.write(stream);

        if (version >= v900) {
            for (const auto &fx : faceFx) {
                stream.startBlock("FaceFX", fx[0]);
                stream.writeAttr("Path", fx[1]);
                stream.endBlock();
            }

            if (!bindPose.empty()) {
                stream.startBlock("BindPose");
                for (const auto &matrices : bindPose) {
                    stream.startBlock("Matrices", matrices.size());
                    stream.writeValue(matrices);
                    stream.endBlock();
                }
                stream.endBlock();
            }
        }

        stream.writeFile(path);
    }
    else errors.fail(Errors::File, "Invalid output file extension: " + ext);

    this->version = origVersion;
}

template<typename T>
void Model::saveStaticObjectChunk(BinaryStream &stream, const std::vector<T> &objects, uint32_t tag, size_t size) const
{
    if (!objects.empty()) {
        stream.write(tag);
        stream.write<uint32_t>(objects.size() * size);
        for (const auto &object : objects) object.write(stream);
    }
}

template<typename T>
void Model::saveDynamicObjectChunk(BinaryStream &stream, const std::vector<T> &objects, uint32_t tag) const
{
    if (!objects.empty()) {
        stream.write(tag);

        size_t size = 0;
        for (const auto &object : objects) size += object.getByteLength(version);
        stream.write<uint32_t>(size);

        for (const auto &object : objects) object.write(stream);
    }
}

template<typename T>
void Model::saveStaticObjectsBlock(TokenStream &stream, const std::vector<T> &objects, const std::string &name) const
{
    if (!objects.empty()) {
        stream.startBlock(name, objects.size());
        for (const auto &object: objects) object.write(stream);
        stream.endBlock();
    }
}

/**
 * @brief Calculate the size of the model as MDX.
 */
size_t Model::getByteLength(uint32_t version)
{
    size_t size = baseByteLength
            + getStaticObjectsChunkByteLength(sequences, Sequence::byteLength)
            + getStaticObjectsChunkByteLength(globalSequences, 4)
            + getDynamicObjectsChunkByteLength(materials)
            + getStaticObjectsChunkByteLength(textures, Texture::byteLength)
            + getDynamicObjectsChunkByteLength(textureAnimations)
            + getDynamicObjectsChunkByteLength(geosets)
            + getDynamicObjectsChunkByteLength(geosetAnimations)
            + getDynamicObjectsChunkByteLength(bones)
            + getDynamicObjectsChunkByteLength(lights)
            + getDynamicObjectsChunkByteLength(helpers)
            + getDynamicObjectsChunkByteLength(attachments)
            + getStaticObjectsChunkByteLength(pivotPoints, 12)
            + getDynamicObjectsChunkByteLength(particleEmitters)
            + getDynamicObjectsChunkByteLength(particleEmitters2)
            + getDynamicObjectsChunkByteLength(ribbonEmitters)
            + getStaticObjectsChunkByteLength(soundEmitters, SoundEmitter::byteLength)
            + getDynamicObjectsChunkByteLength(cameras)
            + getDynamicObjectsChunkByteLength(eventObjects)
            + getDynamicObjectsChunkByteLength(collisionShapes);

    if (version >= v900) {
        size += getDynamicObjectsChunkByteLength(particleEmittersPopcorn);
        if (!faceFx.empty()) size += faceFx.size() * 8 + strSizeSmall + strSize;
        if (!bindPose.empty()) size += 12 + vectorByteLength(bindPose);
    }

    size += getObjectsByteLength(unknownChunks);

    return size;
}

template<typename T>
size_t Model::getStaticObjectsChunkByteLength(const std::vector<T> &objects, size_t size) const
{
    return objects.empty() ? 0 : 8 + objects.size()*size;
}

template<typename T>
size_t Model::getObjectsByteLength(const std::vector<T> &objects) const
{
    size_t size = 0;

    for (const auto &object : objects) size += object.getByteLength(version);

    return size;
}

template<typename T>
size_t Model::getDynamicObjectsChunkByteLength(const std::vector<T> &objects) const
{
    return objects.empty() ? 0 : 8 + getObjectsByteLength(objects);
}

void Model::prepareEdit()
{
    edit.errors = &errors;

    edit.version(version);
    edit.name(name);
    edit.animationFile(animationFile);
    edit.extent = extent;
    edit.blendTime = blendTime;

    prepareObjects(sequences, edit.sequences);
    prepareIndexedValues(globalSequences, edit.globalSequences);
    prepareIndexedObjects(textures, edit.textures);
    prepareIndexedObjects(textureAnimations, edit.textureAnimations);
    prepareIndexedObjects(materials, edit.materials);
    prepareIndexedObjects(geosets, edit.geosets);
    prepareIndexedObjects(geosetAnimations, edit.geosetAnimations);
    prepareGenericObjects(bones, edit.bones);
    prepareGenericObjects(lights, edit.lights);
    prepareGenericObjects(helpers, edit.helpers);
    prepareGenericObjects(attachments, edit.attachments);
    prepareGenericObjects(particleEmitters, edit.particleEmitters);
    prepareGenericObjects(particleEmitters2, edit.particleEmitters2);
    prepareGenericObjects(particleEmittersPopcorn, edit.particleEmittersPopcorn);
    prepareGenericObjects(ribbonEmitters, edit.ribbonEmitters);
    prepareObjects(soundEmitters, edit.soundEmitters);
    prepareObjects(cameras, edit.cameras);
    prepareGenericObjects(eventObjects, edit.eventObjects);
    prepareGenericObjects(collisionShapes, edit.collisionShapes);

    // Fill object pointers in geosets
    for (size_t i=0; i<geosets.size(); i++) {
        std::vector<uint32_t> matrixGroups = geosets[i].getMatrixGroups();
        std::vector<uint32_t> matrixIndices = geosets[i].getMatrixIndices();

        for (size_t j=0, iInd=0; j<matrixGroups.size(); j++) {
            for (size_t end = iInd+matrixGroups[j]; iInd<end; iInd++) {
                edit.geosets[i]->matrixGroups[j]->objects.emplace_back(edit.getGenericObject(matrixIndices[iInd]));
            }
        }

        std::vector<u8vec8> skinWeights = geosets[i].getSkinWeights();
        for (size_t j=0; j<skinWeights.size() && j<edit.geosets[i]->vertices.size(); j++) {
            edit.geosets[i]->vertices[j]->skinWeight({
                                                         edit.getGenericObject(skinWeights[j][0]),
                                                         edit.getGenericObject(skinWeights[j][1]),
                                                         edit.getGenericObject(skinWeights[j][2]),
                                                         edit.getGenericObject(skinWeights[j][3]),
                                                     }, { skinWeights[j][4], skinWeights[j][5], skinWeights[j][6], skinWeights[j][7], });
        }
    }

    // Fill parent pointers in generic objects
    setGenericObjectParents(bones, edit.bones);
    setGenericObjectParents(lights, edit.lights);
    setGenericObjectParents(helpers, edit.helpers);
    setGenericObjectParents(attachments, edit.attachments);
    setGenericObjectParents(particleEmitters, edit.particleEmitters);
    setGenericObjectParents(particleEmitters2, edit.particleEmitters2);
    setGenericObjectParents(particleEmittersPopcorn, edit.particleEmittersPopcorn);
    setGenericObjectParents(ribbonEmitters, edit.ribbonEmitters);
    setGenericObjectParents(eventObjects, edit.eventObjects);
    setGenericObjectParents(collisionShapes, edit.collisionShapes);

    edit.faceFx = faceFx;
    edit.bindPose = bindPose;
}

template<typename T, typename TE>
void Model::prepareObjects(std::vector<T> &objects, std::vector<TE> &editObjects)
{
    editObjects.reserve(objects.size());
    for (auto &object : objects) {
        editObjects.emplace_back(object.edit(&errors, edit));
    }
}
template<typename T, typename TE>
void Model::prepareGenericObjects(std::vector<T> &objects, std::vector<TE> &editObjects)
{
    editObjects.reserve(objects.size());
    for (auto &object : objects) {
        auto newObject = editObjects.emplace_back(object.edit(&errors, edit));

        if (newObject->objectId > 0 && newObject->objectId < static_cast<int32_t>(pivotPoints.size())) {
            newObject->pivotPoint = pivotPoints[newObject->objectId];
        }
    }
}

template<typename T, typename TE>
void Model::setGenericObjectParents(std::vector<T> &objects, std::vector<TE> &editObjects)
{
    for (size_t i=0; i<objects.size(); i++) {
        editObjects[i]->parent = edit.getGenericObject(objects[i].getParentId());
    }
}

template<typename T, typename TE>
void Model::prepareIndexedObjects(std::vector<T> &objects, std::vector<TE> &editObjects)
{
    editObjects.reserve(objects.size());
    for (size_t i=0; i<objects.size(); i++) {
        editObjects.emplace_back(objects[i].edit(i, &errors, edit));
    }
}

template<typename T, typename TE>
void Model::prepareIndexedValues(std::vector<T> &objects, std::vector<std::shared_ptr<TE>> &editObjects)
{
    editObjects.reserve(objects.size());
    for (size_t i=0; i<objects.size(); i++) {
        editObjects.emplace_back(std::make_shared<TE>(i, objects[i]));
    }
}

void Model::saveEdit()
{
    version = edit.version();
    name = edit.name();
    animationFile = edit.animationFile();
    extent = edit.extent;
    blendTime = edit.blendTime;

    saveObjects(sequences, edit.sequences);

    globalSequences.clear();
    globalSequences.reserve(edit.globalSequences.size());
    for (const auto &globalSequence : edit.globalSequences) {
        globalSequences.emplace_back(globalSequence->length);
    }

    pivotPoints.clear();
    saveObjects(textures, edit.textures);
    saveObjects(textureAnimations, edit.textureAnimations);
    saveObjects(materials, edit.materials);
    saveObjects(geosets, edit.geosets);
    saveObjects(geosetAnimations, edit.geosetAnimations);
    saveGenericObjects(bones, edit.bones);
    saveGenericObjects(lights, edit.lights);
    saveGenericObjects(helpers, edit.helpers);
    saveGenericObjects(attachments, edit.attachments);
    saveGenericObjects(particleEmitters, edit.particleEmitters);
    saveGenericObjects(particleEmitters2, edit.particleEmitters2);
    saveGenericObjects(particleEmittersPopcorn, edit.particleEmittersPopcorn);
    saveGenericObjects(ribbonEmitters, edit.ribbonEmitters);
    saveObjects(soundEmitters, edit.soundEmitters);
    saveObjects(cameras, edit.cameras);
    saveGenericObjects(eventObjects, edit.eventObjects);
    saveGenericObjects(collisionShapes, edit.collisionShapes);

    faceFx = edit.faceFx;
    bindPose = edit.bindPose;
}

template<typename T, typename TE>
void Model::saveObjects(std::vector<T> &objects, std::vector<TE> &editObjects)
{
    objects.clear();
    objects.reserve(editObjects.size());
    for (const auto &editObject : editObjects) {
        objects.emplace_back(editObject);
    }
}

template<typename T, typename TE>
void Model::saveGenericObjects(std::vector<T> &objects, std::vector<TE> &editObjects)
{
    objects.clear();
    objects.reserve(editObjects.size());
    for (const auto &editObject : editObjects) {
        objects.emplace_back(editObject);

        if (editObject->objectId >= 0) {
            if (editObject->objectId >= static_cast<int32_t>(pivotPoints.size())) {
                pivotPoints.resize(editObject->objectId+1);
            }

            pivotPoints[editObject->objectId] = editObject->pivotPoint;
        }
    }
}

void Model::deleteUnknownChunks()
{
    unknownChunks.clear();
}
}
