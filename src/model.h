#ifndef MODEL_H
#define MODEL_H

#include "errors.h"

#include "sequence.h"
#include "layer.h"
#include "material.h"
#include "texture.h"
#include "textureanimation.h"
#include "geoset.h"
#include "geosetanimation.h"
#include "bone.h"
#include "light.h"
#include "helper.h"
#include "attachment.h"
#include "particleemitter.h"
#include "particleemitter2.h"
#include "particleemitterpopcorn.h"
#include "ribbonemitter.h"
#include "soundemitter.h"
#include "camera.h"
#include "eventobject.h"
#include "collisionshape.h"
#include "unknownchunk.h"

#include "model_edit.h"

namespace mdlx
{
/**
 * @brief A Warcraft 3 model.
 * Supports loading from the binary MDX file format.
 */
class Model
{
    static const size_t baseByteLength = 396;
    static const size_t modelChunkByteLength = 372;

    int options = 0;

    /**
     * 800 for Warcraft 3: RoC and TFT.
     * >800 for Warcraft 3: Reforged.
     */
    uint32_t    version = 800;
    std::string name;

    /** To the best of my [GhostWolf's] knowledge, this should always be left empty. */
    std::string animationFile;
    Extent      extent;

    /** This is only used by the now-defunct previewer that came with Art Tools. */
    uint32_t blendTime = 0;

    std::vector<Sequence>               sequences;
    std::vector<uint32_t>               globalSequences;
    std::vector<Material>               materials;
    std::vector<Texture>                textures;
    std::vector<TextureAnimation>       textureAnimations;
    std::vector<Geoset>                 geosets;
    std::vector<GeosetAnimation>        geosetAnimations;
    std::vector<Bone>                   bones;
    std::vector<Light>                  lights;
    std::vector<Helper>                 helpers;
    std::vector<Attachment>             attachments;
    std::vector<vec3>                   pivotPoints;
    std::vector<ParticleEmitter>        particleEmitters;
    std::vector<ParticleEmitter2>       particleEmitters2;

    /** @since 900 */
    std::vector<ParticleEmitterPopcorn> particleEmittersPopcorn;

    std::vector<RibbonEmitter>          ribbonEmitters;
    std::vector<SoundEmitter>           soundEmitters;
    std::vector<Camera>                 cameras;
    std::vector<EventObject>            eventObjects;
    std::vector<CollisionShape>         collisionShapes;

    /** @since 900 */
    /** @brief Targets and paths to face effect files, which is used by the FaceFX runtime */
    std::vector<std::array<std::string, 2>> faceFx; // It appears that multiple face effects can be present?
    std::vector<vec12>                      bindPose;

    /**
     * The MDX format is chunk based, and Warcraft 3 does not mind there being unknown chunks in there.
     * Some 3rd party tools use this to attach metadata to models.
     * When an unknown chunk is encountered, it will be added here.
     * These chunks will be saved when saving as MDX.
     */
    std::vector<UnknownChunk> unknownChunks;

    std::string getExt(const std::string &path);

    template<typename T>
    void loadStaticObjects(BinaryStream &stream, std::vector<T> &objects, size_t count) const;
    template<typename T>
    void loadDynamicObjects(BinaryStream &stream, std::vector<T> &objects, size_t size) const;

    template<typename T>
    void loadNumberedObjectBlock(TokenStream &stream, std::vector<T> &objects, const std::string &name);

    template<typename T>
    void saveStaticObjectChunk(BinaryStream &stream, const std::vector<T> &objects, uint32_t tag, size_t size) const;
    template<typename T>
    void saveDynamicObjectChunk(BinaryStream &stream, const std::vector<T> &objects, uint32_t tag) const;

    template<typename T>
    void saveStaticObjectsBlock(TokenStream &stream, const std::vector<T> &objects, const std::string &name) const;

    size_t getByteLength(uint32_t version);
    template<typename T>
    size_t getStaticObjectsChunkByteLength(const std::vector<T> &objects, size_t size) const;
    template<typename T>
    size_t getObjectsByteLength(const std::vector<T> &objects) const;
    template<typename T>
    size_t getDynamicObjectsChunkByteLength(const std::vector<T> &objects) const;

public:
    Errors errors;
    ModelEdit edit;

    Model() = default;
    Model(const std::string &path);

    /**
     * @brief Load the model.
     * The format is detected by the file extension.
     */
    void load(const std::string &path);

    /**
     * @brief Save the model. MDL only: `magosCompatible` determines whether global sequence IDs are written to event objects.
     * The format is detected by the file extension.
     */
    void save(const std::string &path, int options = 0);

    void prepareEdit();

    template<typename T, typename TE>
    void prepareObjects(std::vector<T> &objects, std::vector<TE> &editObjects);
    template<typename T, typename TE>
    void prepareGenericObjects(std::vector<T> &objects, std::vector<TE> &editObjects);
    template<typename T, typename TE>
    void setGenericObjectParents(std::vector<T> &objects, std::vector<TE> &editObjects);
    template<typename T, typename TE>
    void prepareIndexedObjects(std::vector<T> &objects, std::vector<TE> &editObjects);
    template<typename T, typename TE>
    void prepareIndexedValues(std::vector<T> &objects, std::vector<std::shared_ptr<TE>> &editObjects);

    void saveEdit();

    template<typename T, typename TE>
    void saveObjects(std::vector<T> &objects, std::vector<TE> &editObjects);
    template<typename T, typename TE>
    void saveGenericObjects(std::vector<T> &objects, std::vector<TE> &editObjects);

    void deleteUnknownChunks();
};
}

#endif // MODEL_H
