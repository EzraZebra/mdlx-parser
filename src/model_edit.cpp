﻿#include "model_edit.h"
#include "animation_edit.h"
#include "errors.h"

namespace mdlx
{
bool ModelEdit::version(uint32_t version)
{
    switch (version)
    {
    case v800: case v900: case v1000:
        version_ = version;
        return true;
    default:
        errors->add(Errors::Value, "Model Version", std::to_string(version));
        return false;
    }
}

bool ModelEdit::name(const std::string &name)
{
    if (name.length() <= strSizeSmall) {
        name_ = name;
        return true;
    }

    errors->add(Errors::Value, "Model Name", name);
    return false;
}

bool ModelEdit::animationFile(const std::string &animationFile)
{
    if (animationFile.length() <= strSize) {
        animationFile_ = animationFile;
        return true;
    }

    errors->add(Errors::Value, "Model Animation File", animationFile);
    return false;
}

GenericObjects ModelEdit::getGenericObjects() const
{
    GenericObjects genericObjects;

    // Reserve objects size
    genericObjects.reserve(bones.size() + lights.size() + helpers.size() + attachments.size()
                           + particleEmitters.size() + particleEmitters2.size() + particleEmittersPopcorn.size() + ribbonEmitters.size()
                           + eventObjects.size() + collisionShapes.size());

    genericObjects.insert(genericObjects.end(), bones.begin(),                      bones.end());
    genericObjects.insert(genericObjects.end(), lights.begin(),                     lights.end());
    genericObjects.insert(genericObjects.end(), helpers.begin(),                    helpers.end());
    genericObjects.insert(genericObjects.end(), attachments.begin(),                attachments.end());
    genericObjects.insert(genericObjects.end(), particleEmitters.begin(),           particleEmitters.end());
    genericObjects.insert(genericObjects.end(), particleEmitters2.begin(),          particleEmitters2.end());
    genericObjects.insert(genericObjects.end(), particleEmittersPopcorn.begin(),    particleEmittersPopcorn.end());
    genericObjects.insert(genericObjects.end(), ribbonEmitters.begin(),             ribbonEmitters.end());
    genericObjects.insert(genericObjects.end(), eventObjects.begin(),               eventObjects.end());
    genericObjects.insert(genericObjects.end(), collisionShapes.begin(),            collisionShapes.end());

    return genericObjects;
}

AnimatedObjects ModelEdit::getAnimatedObjects(GenericObjects genericObjects) const
{
    AnimatedObjects animatedObjects;
    if (genericObjects.empty()) genericObjects = getGenericObjects();

    // Get Layers
    for (const auto &material : materials) {
        animatedObjects.insert(animatedObjects.end(), material->layers.begin(), material->layers.end());
    }

    // Reserve rest of objects size
    animatedObjects.reserve(animatedObjects.size() + genericObjects.size() + textureAnimations.size() + geosetAnimations.size() + cameras.size());

    // Get rest of objects
    animatedObjects.insert(animatedObjects.end(), textureAnimations.begin(),    textureAnimations.end());
    animatedObjects.insert(animatedObjects.end(), geosetAnimations.begin(),     geosetAnimations.end());
    animatedObjects.insert(animatedObjects.end(), cameras.begin(),              cameras.end());

    // Insert generic objects
    animatedObjects.insert(animatedObjects.end(), genericObjects.begin(), genericObjects.end());

    return animatedObjects;
}

Animations ModelEdit::getAnimations(AnimatedObjects animatedObjects) const
{
    Animations animations;
    if (animatedObjects.empty()) animatedObjects = getAnimatedObjects();

    for (const auto &animatedObject : animatedObjects) {
        animations.insert(animations.end(), animatedObject->animations.begin(), animatedObject->animations.end());
    }

    return animations;
}

std::shared_ptr<GenericObjectEdit> ModelEdit::getGenericObject(int32_t objectId, GenericObjects genericObjects) const
{
    if (objectId >= 0) {
        if (genericObjects.empty()) genericObjects = getGenericObjects();

        if (objectId < static_cast<int32_t>(genericObjects.size())) {
            for (const auto &genericObject : genericObjects) {
                if(genericObject->objectId == objectId) {
                    return genericObject;
                }
            }
        }
    }

    return nullptr;
}

std::shared_ptr<GeosetAnimationEdit> ModelEdit::getGeosetAnimation(uint32_t geosetIndex) const
{
    if (geosetIndex < geosets.size()) {
        for (auto &geosetAnimation : geosetAnimations) {
            if (geosetAnimation->geoset == geosets[geosetIndex]) {
                return geosetAnimation;
            }
        }
    }

    return nullptr;
}

bool ModelEdit::isGenericObjectInUse(uint32_t objectId, GenericObjects genericObjects) const
{
    if (genericObjects.empty()) genericObjects = getGenericObjects();
    const auto genericObject = getGenericObject(objectId, genericObjects);

    if (genericObject == nullptr) {
        errors->add(Errors::Value, "isGenericObjectInUse objectId", std::to_string(objectId));
        return false;
    }

    for (const auto &object : genericObjects) {
        if (object->parent == genericObject) {
            return true;
        }
    }

    for (const auto &geoset : geosets) {
        for (const auto &matrixGroup : geoset->matrixGroups) {
            for (const auto &object : matrixGroup->objects) {
                if (object == genericObject) {
                    return true;
                }
            }
        }

        for (const auto &vertex : geoset->vertices) {
            for (const auto &object : vertex->skinWeightObjects()) {
                if (object == genericObject) {
                    return true;
                }
            }
        }
    }

    return false;
}

bool ModelEdit::deleteGlobalSequence(uint32_t index, bool skipCheck)
{
    if (index < globalSequences.size()) {
        if (!skipCheck) {
            for (auto &animation : getAnimations()) {
                if (animation->globalSequence && animation->globalSequence->index == globalSequences[index]->index) {
                    animation->globalSequence = nullptr;
                }
            }
        }

        return deleteIndexedObject(index, globalSequences);
    }

    errors->add(Errors::Index, "deleteGlobalSequence", std::to_string(index));
    return false;
}

bool ModelEdit::deleteMaterial(uint32_t index, bool skipCheck)
{
    if (index < materials.size()) {
        if (!skipCheck) {
            for (auto &geoset : geosets) {
                if (geoset->material && geoset->material->index == index) {
                    geoset->material = nullptr;
                }
            }

            for (auto &ribbonEmitter : ribbonEmitters) {
                if (ribbonEmitter->material && ribbonEmitter->material->index == index) {
                    ribbonEmitter->material = nullptr;
                }
            }
        }

        return deleteIndexedObject(index, materials);
    }

    errors->add(Errors::Index, "deleteMaterial", std::to_string(index));
    return false;
}

bool ModelEdit::deleteTexture(uint32_t index, bool skipCheck)
{
    if (index < textures.size()) {
        if (!skipCheck) {
            for (auto &material : materials) for (auto &layer : material->layers) {
                if (layer->texture && layer->texture->index == index) {
                    layer->texture = nullptr;
                }
            }

            for (auto &particleEmitter2 : particleEmitters2) {
                if (particleEmitter2->texture && particleEmitter2->texture->index == index) {
                    particleEmitter2->texture = nullptr;
                }
            }
        }

        return deleteIndexedObject(index, textures);
    }

    errors->add(Errors::Index, "deleteTexture", std::to_string(index));
    return false;
}

bool ModelEdit::deleteTextureAnimation(uint32_t index, bool skipCheck)
{
    if (index < textureAnimations.size()) {
        if (!skipCheck) {
            for (auto &material : materials) for (auto &layer : material->layers) {
                if (layer->textureAnimation && layer->textureAnimation->index == index) {
                    layer->textureAnimation = nullptr;
                }
            }
        }

        return deleteIndexedObject(index, textureAnimations);
    }

    errors->add(Errors::Index, "deleteTextureAnimation", std::to_string(index));
    return false;
}

bool ModelEdit::deleteGeoset(uint32_t index, bool skipCheck)
{
    if (index < geosets.size()) {
        if (!skipCheck) {
            for (auto &bone : bones) {
                if (bone->geoset && bone->geoset->index == index) {
                    bone->geoset = nullptr;
                }
            }

            for (auto &geosetAnimation : geosetAnimations) {
                if (geosetAnimation->geoset && geosetAnimation->geoset->index == index) {
                    geosetAnimation->geoset = nullptr;
                }
            }
        }

        return deleteIndexedObject(index, geosets);
    }

    errors->add(Errors::Index, "deleteGeoset", std::to_string(index));
    return false;
}

bool ModelEdit::deleteGeosetAnimation(uint32_t index, bool skipCheck)
{
    if (index < geosetAnimations.size()) {
        if (!skipCheck) {
            for (auto &bone : bones) {
                if (bone->geosetAnimation && bone->geosetAnimation->index == index) {
                    bone->geosetAnimation = nullptr;
                }
            }
        }

        return deleteIndexedObject(index, geosetAnimations);
    }

    errors->add(Errors::Index, "deleteGeosetAnimation", std::to_string(index));
    return false;
}

bool ModelEdit::deleteBone(uint32_t index, bool skipCheck)
{
    return deleteGenericObject(index, "Bone", bones, skipCheck);
}

bool ModelEdit::deleteHelper(uint32_t index, bool skipCheck)
{
    return deleteGenericObject(index, "Helper", helpers, skipCheck);
}

bool ModelEdit::deleteEventObject(uint32_t index, bool skipCheck)
{
    return deleteGenericObject(index, "Event Object", eventObjects, skipCheck);
}
}
