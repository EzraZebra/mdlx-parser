#ifndef MODELEDIT_H
#define MODELEDIT_H

#include "sequence_edit.h"
#include "globalsequence_edit.h"
#include "material_edit.h"
#include "texture_edit.h"
#include "textureanimation_edit.h"
#include "geoset_edit.h"
#include "geosetanimation_edit.h"
#include "bone_edit.h"
#include "light_edit.h"
#include "helper_edit.h"
#include "attachment_edit.h"
#include "particleemitter_edit.h"
#include "particleemitter2_edit.h"
#include "particleemitterpopcorn_edit.h"
#include "ribbonemitter_edit.h"
#include "soundemitter_edit.h"
#include "camera_edit.h"
#include "eventobject_edit.h"
#include "collisionshape_edit.h"

namespace mdlx
{
using GenericObjects = std::vector<std::shared_ptr<GenericObjectEdit>>;
using AnimatedObjects = std::vector<std::shared_ptr<AnimatedObjectEdit>>;
using Animations = std::vector<std::shared_ptr<AnimEditBase>>;

struct ModelEdit
{
    bool ready = false;

    Errors *errors;

    uint32_t    version() const { return version_; }
    bool        version(uint32_t version);
    std::string name() const { return name_; }
    bool        name(const std::string &name);
    std::string animationFile() const { return animationFile_; }
    bool        animationFile(const std::string &animationFile);

    Extent      extent;
    uint32_t    blendTime;

    //std::vector<vec3> pivotPoints;

    std::vector<std::shared_ptr<SequenceEdit>>                  sequences{};
    std::vector<std::shared_ptr<GlobalSequenceEdit>>            globalSequences;
    std::vector<std::shared_ptr<MaterialEdit>>                  materials;
    std::vector<std::shared_ptr<TextureEdit>>                   textures;
    std::vector<std::shared_ptr<TextureAnimationEdit>>          textureAnimations;
    std::vector<std::shared_ptr<GeosetEdit>>                    geosets;
    std::vector<std::shared_ptr<GeosetAnimationEdit>>           geosetAnimations;
    std::vector<std::shared_ptr<BoneEdit>>                      bones;
    std::vector<std::shared_ptr<LightEdit>>                     lights;
    std::vector<std::shared_ptr<HelperEdit>>                    helpers;
    std::vector<std::shared_ptr<AttachmentEdit>>                attachments;
    std::vector<std::shared_ptr<ParticleEmitterEdit>>           particleEmitters;
    std::vector<std::shared_ptr<ParticleEmitter2Edit>>          particleEmitters2;
    std::vector<std::shared_ptr<ParticleEmitterPopcornEdit>>    particleEmittersPopcorn;
    std::vector<std::shared_ptr<RibbonEmitterEdit>>             ribbonEmitters;
    std::vector<std::shared_ptr<SoundEmitterEdit>>              soundEmitters;
    std::vector<std::shared_ptr<CameraEdit>>                    cameras;
    std::vector<std::shared_ptr<EventObjectEdit>>               eventObjects;
    std::vector<std::shared_ptr<CollisionShapeEdit>>            collisionShapes;

    std::vector<std::array<std::string, 2>> faceFx;
    std::vector<vec12>                      bindPose;

    ModelEdit() = default;

    GenericObjects  getGenericObjects() const;
    AnimatedObjects getAnimatedObjects(GenericObjects genericObjects = {}) const;
    Animations      getAnimations(AnimatedObjects = {}) const;
    std::shared_ptr<GenericObjectEdit>      getGenericObject(int32_t objectId, GenericObjects genericObjects = {}) const;
    std::shared_ptr<GeosetAnimationEdit>    getGeosetAnimation(uint32_t geosetIndex) const;
    bool isGenericObjectInUse(uint32_t objectId, GenericObjects genericObjects = {}) const;

    template<class C>
    static bool deleteIndexedObject(uint32_t index, C &objects);

    bool deleteGlobalSequence(uint32_t index, bool skipCheck = false);
    bool deleteMaterial(uint32_t index, bool skipCheck = false);
    bool deleteTexture(uint32_t index, bool skipCheck = false);
    bool deleteTextureAnimation(uint32_t index, bool skipCheck = false);
    bool deleteGeoset(uint32_t index, bool skipCheck = false);
    bool deleteGeosetAnimation(uint32_t index, bool skipCheck = false);

    bool deleteBone(uint32_t index, bool skipCheck = false);
    bool deleteHelper(uint32_t index, bool skipCheck = false);
    bool deleteEventObject(uint32_t index, bool skipCheck = false);

private:
    uint32_t version_;
    std::string name_;
    std::string animationFile_;

    template<class C>
    bool deleteGenericObject(uint32_t objectId, const std::string &type, C &objects, bool skipCheck);
};
}

#include "model_edit.tcc"

#endif // MODELEDIT_H
