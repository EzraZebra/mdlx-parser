#ifndef MODEL_EDIT_TCC
#define MODEL_EDIT_TCC

#include "model_edit.h"
#include "errors.h"

namespace mdlx
{
template<class C>
bool ModelEdit::deleteIndexedObject(uint32_t index, C &objects)
{
    if (index < objects.size()) {
        for (size_t i=index+1; i<objects.size(); i++) {
            objects[i]->index--;
        }

        objects.erase(objects.begin()+index);

        return true;
    }

    return false;
}

template<class C>
bool ModelEdit::deleteGenericObject(uint32_t index, const std::string &type, C &objects, bool skipCheck)
{
    if (index < objects.size()) {
        auto genericObjects = getGenericObjects();

        if (!skipCheck) {
            if (isGenericObjectInUse(objects[index]->objectId, genericObjects)) {
                errors->add(Errors::ObjectInUse, type, objects[index]->name());
                return false;
            }
        }

        // Renumber subsequent objects
        for (auto &object : genericObjects) {
            if (object->objectId > objects[index]->objectId) {
                object->objectId--;
            }
        }

        objects.erase(objects.begin()+index);
        return true;
    }

    errors->add(Errors::Index, "deleteGenericObject", std::to_string(index));
    return false;
}
}

#endif // MODEL_EDIT_TCC
