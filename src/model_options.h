#ifndef MODEL_OPTIONS_H
#define MODEL_OPTIONS_H

#include <cstdint>

namespace mdlx
{
namespace Flag
{
constexpr uint8_t Magos      = 1 << 0;
constexpr uint8_t v800       = 1 << 1;
constexpr uint8_t v900       = 1 << 2;
constexpr uint8_t v1000      = 1 << 3;
}
}

#endif // MODEL_OPTIONS_H
