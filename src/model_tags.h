#ifndef MODEL_TAGS_H
#define MODEL_TAGS_H

#include <cstdint>

namespace mdlx
{
const uint32_t MDLX = 0x584c444d;
const uint32_t VERS = 0x53524556;
const uint32_t MODL = 0x4c444f4d;
const uint32_t SEQS = 0x53514553;
const uint32_t GLBS = 0x53424c47;
const uint32_t MTLS = 0x534c544d;
const uint32_t TEXS = 0x53584554;
const uint32_t TXAN = 0x4e415854;
const uint32_t GEOS = 0x534f4547;
const uint32_t GEOA = 0x414f4547;
const uint32_t BONE = 0x454e4f42;
const uint32_t LITE = 0x4554494c;
const uint32_t HELP = 0x504c4548;
const uint32_t ATCH = 0x48435441;
const uint32_t PIVT = 0x54564950;
const uint32_t PREM = 0x4d455250;
const uint32_t PRE2 = 0x32455250;
const uint32_t CORN = 0x4e524f43;
const uint32_t RIBB = 0x42424952;
const uint32_t SNDS = 0x53444e53;
const uint32_t CAMS = 0x534d4143;
const uint32_t EVTS = 0x53545645;
const uint32_t CLID = 0x44494c43;
const uint32_t BPOS = 0x534f5042;
const uint32_t FAFX = 0x58464146;
}

#endif // MODEL_TAGS_H
