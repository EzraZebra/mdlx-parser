#include "particleemitter.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "animationmap.h"

#include "model_edit.h"

namespace mdlx
{
ParticleEmitter::ParticleEmitter(BinaryStream &stream)
    : GenericObject(ObjectFlags::ParticleEmitter)
{
    const size_t start = stream.position();
    const size_t size = stream.read<uint32_t>();

    read(stream);

    emissionRate = stream.read<float>();
    gravity = stream.read<float>();
    longitude = stream.read<float>();
    latitude = stream.read<float>();
    path = stream.readString(strSize);
    lifeSpan = stream.read<float>();
    speed = stream.read<float>();

    readAnimations(stream, size - (stream.position() - start));
}

void ParticleEmitter::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength());

    GenericObject::write(stream);

    stream.write(emissionRate);
    stream.write(gravity);
    stream.write(longitude);
    stream.write(latitude);
    stream.writeString(path);
    stream.skip(strSize - path.length());
    stream.write(lifeSpan);
    stream.write(speed);

    writeNonGenericAnimationChunks(stream);
}

size_t ParticleEmitter::getByteLength(uint32_t) const
{
    return baseByteLength + GenericObject::getByteLength();
}

ParticleEmitter::ParticleEmitter(TokenStream &stream)
    : GenericObject(ObjectFlags::ParticleEmitter)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        if (token == "EmitterUsesMDL") flags |= Flags::EmitterUsesMDL;
        else if (token == "EmitterUsesTGA") flags |= Flags::EmitterUsesTGA;
        else if (token == "static EmissionRate") emissionRate = stream.read<float>();
        else if (token == "static Gravity") gravity = stream.read<float>();
        else if (token == "static Longitude") longitude = stream.read<float>();
        else if (token == "static Latitude") latitude = stream.read<float>();
        else if (token == "Particle") {
            while (!(token = iterAnimatedBlock(stream)).empty()) {
                if (token == "static LifeSpan") lifeSpan = stream.read<float>();
                else if (token == "static InitVelocity") speed = stream.read<float>();
                else if (token == "Path") path = stream.read<std::string>();
                else if (!readAnimation(stream, "ParticleEmitter", token)) {
                    stream.errors.fail(Errors::UnknownToken, "ParticleEmitter " + name, "Particle " + token);
                }
            }
        }
        else if (!readAnimation(stream, "ParticleEmitter", token)) {
            stream.errors.fail(Errors::UnknownToken, "ParticleEmitter " + name, token);
        }
    }
}

void ParticleEmitter::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "ParticleEmitter");

    if (flags & Flags::EmitterUsesMDL) stream.writeFlag("EmitterUsesMDL");

    if (flags & Flags::EmitterUsesTGA) stream.writeFlag("EmitterUsesTGA");

    if (!writeAnimation(stream, KPEE)) {
        stream.writeAttr("static EmissionRate", emissionRate);
    }

    if (!writeAnimation(stream, KPEG)) {
        stream.writeAttr("static Gravity", gravity);
    }

    if (!writeAnimation(stream, KPLN)) {
        stream.writeAttr("static Longitude", longitude);
    }

    if (!writeAnimation(stream, KPLT)) {
        stream.writeAttr("static Latitude", latitude);
    }

    writeAnimation(stream, KPEV);

    stream.startBlock("Particle");

    if (!writeAnimation(stream, KPEL)) {
        stream.writeAttr("static LifeSpan", lifeSpan);
    }

    if (!writeAnimation(stream, KPES)) {
        stream.writeAttr("static InitVelocity", speed);
    }

    if ((flags & Flags::EmitterUsesMDL) || (flags & Flags::EmitterUsesTGA)) {
        stream.writeAttr("Path", path);
    }

    stream.endBlock();

    writeGenericAnimations(stream);
    stream.endBlock();
}

ParticleEmitter::ParticleEmitter(const std::shared_ptr<ParticleEmitterEdit> &particleEmitter)
    : GenericObject(particleEmitter)
{
    emissionRate = particleEmitter->emissionRate;
    gravity = particleEmitter->gravity;
    longitude = particleEmitter->longitude;
    latitude = particleEmitter->latitude;
    path = particleEmitter->path();
    lifeSpan = particleEmitter->lifeSpan;
    speed = particleEmitter->speed;
}

std::shared_ptr<ParticleEmitterEdit> ParticleEmitter::edit(Errors *errors, ModelEdit &model)
{
    auto particleEmitter = std::make_shared<ParticleEmitterEdit>(GenericObject::edit(errors, model, this));

    particleEmitter->emissionRate = emissionRate;
    particleEmitter->gravity = gravity;
    particleEmitter->longitude = longitude;
    particleEmitter->latitude = latitude;
    particleEmitter->path(path);
    particleEmitter->lifeSpan = lifeSpan;
    particleEmitter->speed = speed;

    return particleEmitter;
}
}
