#ifndef PARTICLEEMITTER_H
#define PARTICLEEMITTER_H

#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
struct ParticleEmitterEdit;

/**
 * @brief A particle emitter.
 */
class ParticleEmitter : public GenericObject
{
    static const size_t baseByteLength = 288;

    float       emissionRate = 0.0F, gravity = 0.0F, longitude = 0.0F, latitude = 0.0F;
    std::string path;
    float       lifeSpan = 0.0F, speed = 0.0F;

public:
    enum Flags {
        EmitterUsesMDL = 0x8000,
        EmitterUsesTGA = 0x10000,
    };

    ParticleEmitter(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    ParticleEmitter(TokenStream &stream);
    void write (TokenStream &stream) const;

    ParticleEmitter(const std::shared_ptr<ParticleEmitterEdit> &helper);
    std::shared_ptr<ParticleEmitterEdit> edit(Errors *errors, ModelEdit &model);
};
}

#endif // PARTICLEEMITTER_H
