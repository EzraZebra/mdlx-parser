#include "particleemitter2.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "animationmap.h"

#include "model_edit.h"

namespace mdlx
{
ParticleEmitter2::ParticleEmitter2(BinaryStream &stream)
{
    const size_t start = stream.position();
    const size_t size = stream.read<uint32_t>();

    read(stream);

    speed = stream.read<float>();
    variation = stream.read<float>();
    latitude = stream.read<float>();
    gravity = stream.read<float>();
    lifeSpan = stream.read<float>();
    emissionRate = stream.read<float>();
    width = stream.read<float>();
    length = stream.read<float>();
    filterMode = stream.read<uint32_t>();
    rows = stream.read<uint32_t>();
    columns = stream.read<uint32_t>();
    headOrTail = stream.read<uint32_t>();
    tailLength = stream.read<float>();
    timeMiddle = stream.read<float>();

    segmentColors = { stream.read<vec3>(), stream.read<vec3>(), stream.read<vec3>() };
    segmentAlphas = stream.read<u8vec3>();
    segmentScaling = stream.read<vec3>();
    headIntervals = { stream.read<uvec3>(), stream.read<uvec3>() };
    tailIntervals = { stream.read<uvec3>(), stream.read<uvec3>() };

    textureId = stream.read<int32_t>();
    squirt = stream.read<uint32_t>();
    priorityPlane = stream.read<int32_t>();
    replaceableId = stream.read<uint32_t>();

    readAnimations(stream, size - (stream.position() - start));
}

void ParticleEmitter2::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength());

    GenericObject::write(stream);

    stream.write(speed);
    stream.write(variation);
    stream.write(latitude);
    stream.write(gravity);
    stream.write(lifeSpan);
    stream.write(emissionRate);
    stream.write(width);
    stream.write(length);
    stream.write(filterMode);
    stream.write(rows);
    stream.write(columns);
    stream.write(headOrTail);
    stream.write(tailLength);
    stream.write(timeMiddle);
    stream.write(segmentColors);
    stream.write(segmentAlphas);
    stream.write(segmentScaling);
    stream.write(headIntervals);
    stream.write(tailIntervals);
    stream.write(textureId);
    stream.write(squirt);
    stream.write(priorityPlane);
    stream.write(replaceableId);

    writeNonGenericAnimationChunks(stream);
}

size_t ParticleEmitter2::getByteLength(uint32_t) const
{
    return baseByteLength + GenericObject::getByteLength();
}

ParticleEmitter2::ParticleEmitter2(TokenStream &stream)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        if (token == "SortPrimsFarZ") flags |= Flags::SortPrimsFarZ;
        else if (token == "Unshaded") flags |= Flags::Unshaded;
        else if (token == "LineEmitter") flags |= Flags::LineEmitter;
        else if (token == "Unfogged") flags |= Flags::Unfogged;
        else if (token == "ModelSpace") flags |= Flags::ModelSpace;
        else if (token == "XYQuad") flags |= Flags::XYQuad;
        else if (token == "static Speed") speed = stream.read<float>();
        else if (token == "static Variation") variation = stream.read<float>();
        else if (token == "static Latitude") latitude = stream.read<float>();
        else if (token == "static Gravity") gravity = stream.read<float>();
        else if (token == "Squirt") squirt = 1;
        else if (token == "LifeSpan") lifeSpan = stream.read<float>();
        else if (token == "static EmissionRate") emissionRate = stream.read<float>();
        else if (token == "static Width") width = stream.read<float>();
        else if (token == "static Length") length = stream.read<float>();
        else if (token == "Blend") filterMode = FilterMode::Blend;
        else if (token == "Additive") filterMode = FilterMode::Additive;
        else if (token == "Modulate") filterMode = FilterMode::Modulate;
        else if (token == "Modulate2x") filterMode = FilterMode::Modulate2x;
        else if (token == "AlphaKey") filterMode = FilterMode::AlphaKey;
        else if (token == "Rows") rows = stream.read<uint32_t>();
        else if (token == "Columns") columns = stream.read<uint32_t>();
        else if (token == "Head") headOrTail = HeadOrTail::Head;
        else if (token == "Tail") headOrTail = HeadOrTail::Tail;
        else if (token == "Both") headOrTail = HeadOrTail::Both;
        else if (token == "TailLength") tailLength = stream.read<float>();
        else if (token == "Time") timeMiddle = stream.read<float>();
        else if (token == "SegmentColor") {
            stream.readToken(); // {
            for (size_t i=0; i<3; i++) {
                stream.readToken(); // Color
                segmentColors[i] = stream.readColor();
            }
            stream.readToken(); // }
        }
        else if (token == "Alpha") segmentAlphas = stream.read<u8vec3>();
        else if (token == "ParticleScaling") segmentScaling = stream.read<vec3>();
        else if (token == "LifeSpanUVAnim") headIntervals[0] = stream.read<uvec3>();
        else if (token == "DecayUVAnim") headIntervals[1] = stream.read<uvec3>();
        else if (token == "TailUVAnim") tailIntervals[0] = stream.read<uvec3>();
        else if (token == "TailDecayUVAnim") tailIntervals[1] = stream.read<uvec3>();
        else if (token == "TextureID") textureId = stream.read<int32_t>();
        else if (token == "ReplaceableId") replaceableId = stream.read<uint32_t>();
        else if (token == "PriorityPlane") priorityPlane = stream.read<int32_t>();
        else if (!readAnimation(stream, "ParticleEmitter2", token)) {
            stream.errors.fail(Errors::UnknownToken, "ParticleEmitter2 " + name, token);
        }
    }
}

void ParticleEmitter2::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "ParticleEmitter2");

    if (flags & Flags::SortPrimsFarZ) stream.writeFlag("SortPrimsFarZ");
    if (flags & Flags::Unshaded) stream.writeFlag("Unshaded");
    if (flags & Flags::LineEmitter) stream.writeFlag("LineEmitter");
    if (flags & Flags::Unfogged) stream.writeFlag("Unfogged");
    if (flags & Flags::ModelSpace) stream.writeFlag("ModelSpace");
    if (flags & Flags::XYQuad) stream.writeFlag("XYQuad");

    if (!writeAnimation(stream, KP2S)) {
        stream.writeAttr("static Speed", speed);
    }

    if (!writeAnimation(stream, KP2R)) {
        stream.writeAttr("static Variation", variation);
    }

    if (!writeAnimation(stream, KP2L)) {
        stream.writeAttr("static Latitude", latitude);
    }

    if (!writeAnimation(stream, KP2G)) {
        stream.writeAttr("static Gravity", gravity);
    }

    writeAnimation(stream, KP2V);
    if (squirt) stream.writeFlag("Squirt");
    stream.writeAttr("LifeSpan", lifeSpan);

    if (!writeAnimation(stream, KP2E)) {
        stream.writeAttr("static EmissionRate", emissionRate);
    }

    if (!writeAnimation(stream, KP2N)) {
        stream.writeAttr("static Width", width);
    }

    if (!writeAnimation(stream, KP2W)) {
        stream.writeAttr("static Length", length);
    }

    switch(filterMode) {
    case FilterMode::Blend: stream.writeFlag("Blend");
        break;
    case FilterMode::Additive: stream.writeFlag("Additive");
        break;
    case FilterMode::Modulate: stream.writeFlag("Modulate");
        break;
    case FilterMode::Modulate2x: stream.writeFlag("Modulate2x"); // Does this exist in any model?
        break;
    case FilterMode::AlphaKey: stream.writeFlag("AlphaKey");
        break;
    }

    stream.writeAttr("Rows", rows);
    stream.writeAttr("Columns", columns);

    switch(headOrTail) {
    case HeadOrTail::Head: stream.writeFlag("Head");
        break;
    case HeadOrTail::Tail: stream.writeFlag("Tail");
        break;
    case HeadOrTail::Both: stream.writeFlag("Both");
        break;
    }

    stream.writeAttr("TailLength", tailLength);
    stream.writeAttr("Time", timeMiddle);

    stream.startBlock("SegmentColor");
    stream.writeColor("Color", segmentColors[0]);
    stream.writeColor("Color", segmentColors[1]);
    stream.writeColor("Color", segmentColors[2]);
    stream.endBlock(true);

    stream.writeAttr("Alpha", segmentAlphas);
    stream.writeAttr("ParticleScaling", segmentScaling);
    stream.writeAttr("LifeSpanUVAnim", headIntervals[0]);
    stream.writeAttr("DecayUVAnim", headIntervals[1]);
    stream.writeAttr("TailUVAnim", tailIntervals[0]);
    stream.writeAttr("TailDecayUVAnim", tailIntervals[1]);
    stream.writeAttr("TextureID", textureId);

    if (replaceableId != 0) {
        stream.writeAttr("ReplaceableId", replaceableId);
    }

    if (priorityPlane != 0) {
        stream.writeAttr("PriorityPlane", priorityPlane);
    }

    writeGenericAnimations(stream);
    stream.endBlock();
}

ParticleEmitter2::ParticleEmitter2(const std::shared_ptr<ParticleEmitter2Edit> &particleEmitter2)
    : GenericObject(particleEmitter2)
{
    flags = particleEmitter2->flags();
    speed = particleEmitter2->speed;
    variation = particleEmitter2->variation;
    latitude = particleEmitter2->latitude;
    gravity = particleEmitter2->gravity;
    lifeSpan = particleEmitter2->lifeSpan;
    emissionRate = particleEmitter2->emissionRate;
    width = particleEmitter2->width;
    length = particleEmitter2->length;
    filterMode = particleEmitter2->filterMode();
    rows = particleEmitter2->rows;
    columns = particleEmitter2->columns;
    headOrTail = particleEmitter2->headOrTail();
    tailLength = particleEmitter2->tailLength;
    timeMiddle = particleEmitter2->timeMiddle;

    segmentColors = particleEmitter2->segmentColors;
    segmentAlphas = particleEmitter2->segmentAlphas;
    segmentScaling = particleEmitter2->segmentScaling;
    headIntervals = particleEmitter2->headIntervals;
    tailIntervals = particleEmitter2->tailIntervals;

    if (particleEmitter2->texture) {
        textureId = particleEmitter2->texture->index;
    }

    squirt = particleEmitter2->squirt;
    priorityPlane = particleEmitter2->priorityPlane;
    replaceableId = particleEmitter2->replaceableId();
}

std::shared_ptr<ParticleEmitter2Edit> ParticleEmitter2::edit(Errors *errors, ModelEdit &model)
{
    auto particleEmitter2 = std::make_shared<ParticleEmitter2Edit>(GenericObject::edit(errors, model, this));

    particleEmitter2->flags(flags);
    particleEmitter2->speed = speed;
    particleEmitter2->variation = variation;
    particleEmitter2->latitude = latitude;
    particleEmitter2->gravity = gravity;
    particleEmitter2->lifeSpan = lifeSpan;
    particleEmitter2->emissionRate = emissionRate;
    particleEmitter2->width = width;
    particleEmitter2->length = length;
    particleEmitter2->filterMode(filterMode);
    particleEmitter2->rows = rows;
    particleEmitter2->columns = columns;
    particleEmitter2->headOrTail(headOrTail);
    particleEmitter2->tailLength = tailLength;
    particleEmitter2->timeMiddle = timeMiddle;

    particleEmitter2->segmentColors = segmentColors;
    particleEmitter2->segmentAlphas = segmentAlphas;
    particleEmitter2->segmentScaling = segmentScaling;
    particleEmitter2->headIntervals = headIntervals;
    particleEmitter2->tailIntervals = tailIntervals;

    if (textureId >= 0 && textureId < static_cast<int32_t>(model.textures.size())) {
        particleEmitter2->texture = model.textures[textureId];
    }

    particleEmitter2->squirt = squirt;
    particleEmitter2->priorityPlane = priorityPlane;
    particleEmitter2->replaceableId(replaceableId);

    return particleEmitter2;
}
}
