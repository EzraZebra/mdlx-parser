#ifndef PARTICLEEMITTER2_H
#define PARTICLEEMITTER2_H

#include "util.h"
#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
class ParticleEmitter2Edit;

/**
 * @brief A particle emitter type 2.
 */
class ParticleEmitter2 : public GenericObject
{
    static const size_t baseByteLength = 175;

    float       speed = 0.0F, variation = 0.0F, latitude = 0.0F, gravity = 0.0F;
    float       lifeSpan = 0.0F, emissionRate = 0.0F, width = 0.0F, length = 0.0F;
    uint32_t    filterMode = 0, rows = 0, columns = 0, headOrTail = 0;
    float       tailLength = 0.0F, timeMiddle = 0.0F;

    mat3    segmentColors{};
    u8vec3  segmentAlphas{};
    vec3    segmentScaling{};
    umat2x3 headIntervals{}, tailIntervals{};

    int32_t     textureId = -1;
    uint32_t    squirt = 0;
    int32_t     priorityPlane = 0;
    uint32_t    replaceableId = 0;

public:
    enum Flags {
        Unshaded        = 0x8000,
        SortPrimsFarZ   = 0x10000,
        LineEmitter     = 0x20000,
        Unfogged        = 0x40000,
        ModelSpace      = 0x80000,
        XYQuad          = 0x100000,
    };
    enum FilterMode {
        Blend       = 0,
        Additive    = 1,
        Modulate    = 2,
        Modulate2x  = 3,
        AlphaKey    = 4,
    };
    enum HeadOrTail {
        Head = 0,
        Tail = 1,
        Both = 2,
    };

    ParticleEmitter2(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    ParticleEmitter2(TokenStream &stream);
    void write(TokenStream &stream) const;

    ParticleEmitter2(const std::shared_ptr<ParticleEmitter2Edit> &particleEmitter2);
    std::shared_ptr<ParticleEmitter2Edit> edit(Errors *errors, ModelEdit &model);

};
}

#endif // PARTICLEEMITTER2_H
