#include "particleemitter2_edit.h"
#include "particleemitter2.h"
#include "texture_edit.h"
#include "errors.h"

namespace mdlx
{
bool ParticleEmitter2Edit::flags(uint32_t flags)
{
    if (!GenericObjectEdit::flags(flags, false)) {
        if (flags & ParticleEmitter2::Unshaded || flags & ParticleEmitter2::SortPrimsFarZ || flags & ParticleEmitter2::LineEmitter
                || flags & ParticleEmitter2::Unfogged || flags & ParticleEmitter2::ModelSpace || flags & ParticleEmitter2::XYQuad) {
            flags_ = flags;
            return true;
        }
    }
    else return true;

    errors->add(Errors::Value, "ParticleEmitter2 Flags", std::to_string(flags));
    return false;
}

bool ParticleEmitter2Edit::filterMode(uint32_t filterMode)
{
    if (filterMode <= ParticleEmitter2::AlphaKey) {
        filterMode_ = filterMode;
        return true;
    }

    errors->add(Errors::Value, "ParticleEmitter2 FilterMode", std::to_string(filterMode));
    return false;
}

bool ParticleEmitter2Edit::headOrTail(uint32_t headOrTail)
{
    if (headOrTail <= ParticleEmitter2::Both) {
        headOrTail_ = headOrTail;
        return true;
    }

    errors->add(Errors::Value, "ParticleEmitter2 HeadOrTail", std::to_string(headOrTail));
    return false;
}

bool ParticleEmitter2Edit::replaceableId(uint32_t replaceableId)
{
    if (TextureEdit::validReplaceableId(replaceableId)) {
        replaceableId_ = replaceableId;
        return true;
    }

    errors->add(Errors::Value, "ParticleEmitter2 ReplaceableId", std::to_string(replaceableId));
    return false;
}
}
