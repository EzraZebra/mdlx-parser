#ifndef PARTICLEEMITTER2EDIT_H
#define PARTICLEEMITTER2EDIT_H

#include "genericobject_edit.h"
#include "util.h"

namespace mdlx
{
/** Forward declare */
class TextureEdit;

struct ParticleEmitter2Edit : public GenericObjectEdit
{
    float       speed = 0.0F, variation = 0.0F, latitude = 0.0F, gravity = 0.0F;
    float       lifeSpan = 0.0F, emissionRate = 0.0F, width = 0.0F, length = 0.0F;

    uint32_t    flags() const { return flags_; }
    bool        flags(uint32_t flags);
    uint32_t    filterMode() const { return filterMode_; }
    bool        filterMode(uint32_t filterMode);
    uint32_t    headOrTail() const { return headOrTail_; }
    bool        headOrTail(uint32_t headOrTail);
    uint32_t    replaceableId() const { return replaceableId_; }
    bool        replaceableId(uint32_t replaceableId);

    uint32_t    rows = 0, columns = 0;
    float       tailLength = 0.0F, timeMiddle = 0.0F;

    mat3    segmentColors{};
    u8vec3  segmentAlphas{};
    vec3    segmentScaling{};
    umat2x3 headIntervals{}, tailIntervals{};

    std::shared_ptr<TextureEdit> texture;
    uint32_t    squirt = 0;
    int32_t     priorityPlane = 0;

    ParticleEmitter2Edit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}

private:
    uint32_t filterMode_ = 0, headOrTail_ = 0, replaceableId_ = 0;
};
}

#endif // PARTICLEEMITTER2EDIT_H
