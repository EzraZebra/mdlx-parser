#include "particleemitter_edit.h"
#include "particleemitter.h"
#include "errors.h"

namespace mdlx
{
bool ParticleEmitterEdit::flags(uint32_t flags)
{
    if (!GenericObjectEdit::flags(flags, false)) {
        if (flags & ParticleEmitter::EmitterUsesMDL || flags & ParticleEmitter::EmitterUsesTGA) {
            flags_ = flags;
            return true;
        }
    }
    else return true;

    errors->add(Errors::Value, "ParticleEmitter Flags", std::to_string(flags));
    return false;
}

bool ParticleEmitterEdit::path(const std::string &path)
{
    if (path.length() <= strSize) {
        path_ = path;
        return true;
    }

    errors->add(Errors::Value, "ParticleEmitter Path", path);
    return false;
}
}
