#ifndef PARTICLEEMITTEREDIT_H
#define PARTICLEEMITTEREDIT_H

#include "genericobject_edit.h"

namespace mdlx
{
struct ParticleEmitterEdit : public GenericObjectEdit
{
    bool        flags(uint32_t flags);
    std::string path() const { return path_; }
    bool        path(const std::string &path);

    float       emissionRate = 0.0F, gravity = 0.0F, longitude = 0.0F, latitude = 0.0F;
    float       lifeSpan = 0.0F, speed = 0.0F;

    ParticleEmitterEdit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}

private:
    std::string path_;
};
}

#endif // PARTICLEEMITTEREDIT_H
