#include "particleemitterpopcorn.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "animationmap.h"

#include "model_edit.h"

namespace mdlx
{
ParticleEmitterPopcorn::ParticleEmitterPopcorn(BinaryStream &stream)
{
    const size_t start = stream.position();
    const size_t size = stream.read<uint32_t>();

    read(stream);

    lifeSpan = stream.read<float>();
    emissionRate = stream.read<float>();
    speed = stream.read<float>();
    color = stream.read<vec3>();
    alpha = stream.read<float>();
    replaceableId = stream.read<uint32_t>();
    path = stream.readString(strSize);
    animVisibilityGuide = stream.readString(strSize);

    readAnimations(stream, size - (stream.position() - start));
}

void ParticleEmitterPopcorn::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength());

    GenericObject::write(stream);

    stream.write(lifeSpan);
    stream.write(emissionRate);
    stream.write(speed);
    stream.write(color);
    stream.write(alpha);
    stream.write(replaceableId);
    stream.writeString(path);
    stream.skip(strSize - path.length());
    stream.writeString(animVisibilityGuide);
    stream.skip(strSize - animVisibilityGuide.length());

    writeNonGenericAnimationChunks(stream);
}

size_t ParticleEmitterPopcorn::getByteLength(uint32_t) const
{
    return baseByteLength + GenericObject::getByteLength();
}

ParticleEmitterPopcorn::ParticleEmitterPopcorn(TokenStream &stream)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        if (token == "SortPrimsFarZ") flags |= Flags::SortPrimsFarZ;
        else if (token == "Unshaded") flags |= Flags::Unshaded;
        else if (token == "Unfogged") flags |= Flags::Unfogged;
        else if (token == "static LifeSpan") lifeSpan = stream.read<float>();
        else if (token == "static EmissionRate") emissionRate = stream.read<float>();
        else if (token == "static Speed") speed = stream.read<float>();
        else if (token == "static Color") color = stream.read<vec3>();
        else if (token == "static Alpha") alpha = stream.read<float>();
        else if (token == "ReplaceableId") replaceableId = stream.read<uint32_t>();
        else if (token == "Path") path = stream.read<std::string>();
        else if (token == "AnimVisibilityGuide") animVisibilityGuide = stream.read<std::string>();
        else if (!readAnimation(stream, "ParticleEmitterPopcorn", token)) {
            stream.errors.fail(Errors::UnknownToken, "ParticleEmitterPopcorn " + name, token);
        }
    }
}

void ParticleEmitterPopcorn::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "ParticleEmitterPopcorn");

    if (flags & Flags::SortPrimsFarZ) stream.writeFlag("SortPrimsFarZ");
    if (flags & Flags::Unshaded) stream.writeFlag("Unshaded");
    if (flags & Flags::Unfogged) stream.writeFlag("Unfogged");

    if (!writeAnimation(stream, KPPL)) {
        stream.writeAttr("static LifeSpan", lifeSpan);
    }
    if (!writeAnimation(stream, KPPE)) {
        stream.writeAttr("static EmissionRate", emissionRate);
    }
    if (!writeAnimation(stream, KPPS)) {
        stream.writeAttr("static Speed", speed);
    }
    if (!writeAnimation(stream, KPPC)) {
        stream.writeAttr("static Color", color);
    }
    if (!writeAnimation(stream, KPPA)) {
        stream.writeAttr("static Alpha", alpha);
    }

    writeAnimation(stream, KPPV);

    if (replaceableId != 0) {
        stream.writeAttr("ReplaceableId", replaceableId);
    }

    if (!path.empty()) {
        stream.writeAttr("Path", path);
    }

    if (!animVisibilityGuide.empty()) {
        stream.writeAttr("AnimVisibilityGuide", animVisibilityGuide);
    }

    writeGenericAnimations(stream);

    stream.endBlock();
}

ParticleEmitterPopcorn::ParticleEmitterPopcorn(const std::shared_ptr<ParticleEmitterPopcornEdit> &particleEmitterPopcorn)
    : GenericObject(particleEmitterPopcorn)
{
    lifeSpan = particleEmitterPopcorn->lifeSpan;
    emissionRate = particleEmitterPopcorn->emissionRate;
    speed = particleEmitterPopcorn->speed;
    color = particleEmitterPopcorn->color;
    alpha = particleEmitterPopcorn->alpha;
    replaceableId = particleEmitterPopcorn->replaceableId();
    path = particleEmitterPopcorn->path();
    animVisibilityGuide = particleEmitterPopcorn->animVisibilityGuide();
}

std::shared_ptr<ParticleEmitterPopcornEdit> ParticleEmitterPopcorn::edit(Errors *errors, ModelEdit &model)
{
    auto particleEmitterPopcorn = std::make_shared<ParticleEmitterPopcornEdit>(GenericObject::edit(errors, model, this));

    particleEmitterPopcorn->lifeSpan = lifeSpan;
    particleEmitterPopcorn->emissionRate = emissionRate;
    particleEmitterPopcorn->speed = speed;
    particleEmitterPopcorn->color = color;
    particleEmitterPopcorn->alpha = alpha;
    particleEmitterPopcorn->replaceableId(replaceableId);
    particleEmitterPopcorn->path(path);
    particleEmitterPopcorn->animVisibilityGuide(animVisibilityGuide);

    return particleEmitterPopcorn;
}
}
