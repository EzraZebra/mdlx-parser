#ifndef PARTICLEEMITTERPOPCORN_H
#define PARTICLEEMITTERPOPCORN_H

#include "util.h"
#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
struct ParticleEmitterPopcornEdit;

/**
 * @brief A popcorn fx emitter.
 * Corns are particle emitters that reference pkfx files, which are used by the PopcornFX runtime.
 *
 * @since 900
 */
class ParticleEmitterPopcorn : public GenericObject
{
    static const size_t baseByteLength = 556;

    float       lifeSpan = 0.0F, emissionRate = 0.0F, speed = 0.0F;
    vec3        color = { 1.0F, 1.0F, 1.0F };
    float       alpha = 1.0F;
    uint32_t    replaceableId = 0;
    std::string path, animVisibilityGuide;

public:
    enum Flags {
        Unshaded        = 0x8000,
        SortPrimsFarZ   = 0x10000,
        Unfogged        = 0x40000,
    };

    ParticleEmitterPopcorn(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    ParticleEmitterPopcorn(TokenStream &stream);
    void write(TokenStream &stream) const;

    ParticleEmitterPopcorn(const std::shared_ptr<ParticleEmitterPopcornEdit> &particleEmitterPopcorn);
    std::shared_ptr<ParticleEmitterPopcornEdit> edit(Errors *errors, ModelEdit &model);
};
}

#endif // PARTICLEEMITTERPOPCORN_H
