#include "particleemitterpopcorn_edit.h"
#include "particleemitterpopcorn.h"
#include "texture_edit.h"
#include "errors.h"

namespace mdlx
{
bool ParticleEmitterPopcornEdit::flags(uint32_t flags)
{
    if (!GenericObjectEdit::flags(flags, false)) {
        if (flags & ParticleEmitterPopcorn::Unshaded || flags & ParticleEmitterPopcorn::SortPrimsFarZ
                || flags & ParticleEmitterPopcorn::Unfogged) {
            flags_ = flags;
            return true;
        }
    }
    else return true;

    errors->add(Errors::Value, "ParticleEmitterPopcorn Flags", std::to_string(flags));
    return false;
}

bool ParticleEmitterPopcornEdit::replaceableId(uint32_t replaceableId)
{
    if (TextureEdit::validReplaceableId(replaceableId)) {
        replaceableId_ = replaceableId;
        return true;
    }

    errors->add(Errors::Value, "ParticleEmitterPopcorn ReplaceableId", std::to_string(replaceableId));
    return false;
}

bool ParticleEmitterPopcornEdit::path(const std::string &path)
{
    if (path.length() <= strSize) {
        path_ = path;
        return true;
    }

    errors->add(Errors::Value, "ParticleEmitterPopcorn Path", path);
    return false;
}

bool ParticleEmitterPopcornEdit::animVisibilityGuide(const std::string &guide)
{
    if (guide.length() <= strSize) {
        animVisibilityGuide_ = guide;
        return true;
    }

    errors->add(Errors::Value, "ParticleEmitterPopcorn animVisibilityGuide", guide);
    return false;
}
}
