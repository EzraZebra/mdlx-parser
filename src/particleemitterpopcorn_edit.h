#ifndef PARTICLEEMITTERPOPCORNEDIT_H
#define PARTICLEEMITTERPOPCORNEDIT_H

#include "genericobject_edit.h"

namespace mdlx
{
struct ParticleEmitterPopcornEdit : public GenericObjectEdit
{
    float   lifeSpan = 0.0F, emissionRate = 0.0F, speed = 0.0F;
    vec3    color = { 1.0F, 1.0F, 1.0F };
    float   alpha = 1.0F;

    bool        flags(uint32_t flags);
    uint32_t    replaceableId() const { return replaceableId_; }
    bool        replaceableId(uint32_t);
    std::string path() const { return path_; }
    bool        path(const std::string &path);
    std::string animVisibilityGuide() const { return animVisibilityGuide_; }
    bool        animVisibilityGuide(const std::string &guide);

    ParticleEmitterPopcornEdit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}

private:
    uint32_t    replaceableId_ = 0;
    std::string path_, animVisibilityGuide_;
};
}

#endif // PARTICLEEMITTERPOPCORNEDIT_H
