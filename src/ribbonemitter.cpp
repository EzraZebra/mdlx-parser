#include "ribbonemitter.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "animationmap.h"

#include "model_edit.h"

namespace mdlx
{
RibbonEmitter::RibbonEmitter(BinaryStream &stream)
    : GenericObject(ObjectFlags::RibbonEmitter)
{
    const size_t start = stream.position();
    const size_t size = stream.read<uint32_t>();

    read(stream);

    heightAbove = stream.read<float>();
    heightBelow = stream.read<float>();
    alpha = stream.read<float>();
    color = stream.read<vec3>();
    lifeSpan = stream.read<float>();
    textureSlot = stream.read<uint32_t>();
    emissionRate = stream.read<uint32_t>();
    rows = stream.read<uint32_t>();
    columns = stream.read<uint32_t>();
    materialId = stream.read<int32_t>();
    gravity = stream.read<float>();

    readAnimations(stream, size - (stream.position() - start));
}

void RibbonEmitter::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength());

    GenericObject::write(stream);

    stream.write(heightAbove);
    stream.write(heightBelow);
    stream.write(alpha);
    stream.write(color);
    stream.write(lifeSpan);
    stream.write(textureSlot);
    stream.write(emissionRate);
    stream.write(rows);
    stream.write(columns);
    stream.write(materialId);
    stream.write(gravity);

    writeNonGenericAnimationChunks(stream);
}

size_t RibbonEmitter::getByteLength(uint32_t) const
{
    return baseByteLength + GenericObject::getByteLength();
}

RibbonEmitter::RibbonEmitter(TokenStream &stream)
    : GenericObject(ObjectFlags::RibbonEmitter)
{
    for (std::string token; !(token = iterGenericBlock(stream)).empty(); ) {
        if (token == "static HeightAbove") heightAbove = stream.read<float>();
        else if (token == "static HeightBelow") heightBelow = stream.read<float>();
        else if (token == "static Alpha") alpha = stream.read<float>();
        else if (token == "static Color") color = stream.readColor();
        else if (token == "static TextureSlot") textureSlot = stream.read<uint32_t>();
        else if (token == "EmissionRate") emissionRate = stream.read<uint32_t>();
        else if (token == "LifeSpan") lifeSpan = stream.read<float>();
        else if (token == "Gravity") gravity = stream.read<float>();
        else if (token == "Rows") rows = stream.read<uint32_t>();
        else if (token == "Columns") columns = stream.read<uint32_t>();
        else if (token == "MaterialID") materialId = stream.read<int32_t>();
        else if (!readAnimation(stream, "RibbonEmitter", token)) {
            stream.errors.fail(Errors::UnknownToken, "RibbonEmitter " + name, token);
        }
    }
}

void RibbonEmitter::write(TokenStream &stream) const
{
    writeGenericHeader(stream, "RibbonEmitter");

    if (!writeAnimation(stream, KRHA)) {
        stream.writeAttr("static HeightAbove", heightAbove);
    }

    if (!writeAnimation(stream, KRHB)) {
        stream.writeAttr("static HeightBelow", heightBelow);
    }

    if (!writeAnimation(stream, KRAL)) {
        stream.writeAttr("static Alpha", alpha);
    }

    if (!writeAnimation(stream, KRCO)) {
        stream.writeColor("static Color", color);
    }

    if (!writeAnimation(stream, KRTX)) {
        stream.writeAttr("static TextureSlot", textureSlot);
    }

    writeAnimation(stream, KRVS);
    stream.writeAttr("EmissionRate", emissionRate);
    stream.writeAttr("LifeSpan", lifeSpan);
    if (gravity != 0) stream.writeAttr("Gravity", gravity);
    stream.writeAttr("Rows", rows);
    stream.writeAttr("Columns", columns);
    stream.writeAttr("MaterialID", materialId);

    writeGenericAnimations(stream);
    stream.endBlock();
}

RibbonEmitter::RibbonEmitter(const std::shared_ptr<RibbonEmitterEdit> &ribbonEmitter)
    : GenericObject(ribbonEmitter)
{
    heightAbove = ribbonEmitter->heightAbove;
    heightBelow = ribbonEmitter->heightBelow;
    alpha = ribbonEmitter->alpha;
    color = ribbonEmitter->color;
    lifeSpan = ribbonEmitter->lifeSpan;
    textureSlot = ribbonEmitter->textureSlot;
    emissionRate = ribbonEmitter->emissionRate;
    rows = ribbonEmitter->rows;
    columns = ribbonEmitter->columns;

    if (ribbonEmitter->material) {
        materialId = ribbonEmitter->material->index;
    }

    gravity = ribbonEmitter->gravity;
}

std::shared_ptr<RibbonEmitterEdit> RibbonEmitter::edit(Errors *errors, ModelEdit &model)
{
    auto ribbonEmitter = std::make_shared<RibbonEmitterEdit>(GenericObject::edit(errors, model, this));

    ribbonEmitter->heightAbove = heightAbove;
    ribbonEmitter->heightBelow = heightBelow;
    ribbonEmitter->alpha = alpha;
    ribbonEmitter->color = color;
    ribbonEmitter->lifeSpan = lifeSpan;
    ribbonEmitter->textureSlot = textureSlot;
    ribbonEmitter->emissionRate = emissionRate;
    ribbonEmitter->rows = rows;
    ribbonEmitter->columns = columns;

    if (materialId >= 0 && materialId < static_cast<int32_t>(model.materials.size())) {
        ribbonEmitter->material = model.materials[materialId];
    }

    ribbonEmitter->gravity = gravity;

    return ribbonEmitter;
}
}
