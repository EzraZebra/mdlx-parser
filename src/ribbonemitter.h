#ifndef RIBBONEMITTER_H
#define RIBBONEMITTER_H

#include "util.h"
#include "genericobject.h"

namespace mdlx
{
/** Forward declare */
class ModelEdit;
class RibbonEmitterEdit;
class Errors;

/**
 * @brief A ribbon emitter.
 */
class RibbonEmitter : public GenericObject
{
    static const size_t baseByteLength = 56;

    float       heightAbove = 0.0F, heightBelow = 0.0F, alpha = 0.0F;
    vec3        color{};
    float       lifeSpan = 0.0F;
    uint32_t    textureSlot = 0, emissionRate = 0, rows = 0, columns = 0;
    int32_t     materialId = 0;
    float       gravity = 0.0F;

public:
    RibbonEmitter(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    RibbonEmitter(TokenStream &stream);
    void write(TokenStream &stream) const;

    RibbonEmitter(const std::shared_ptr<RibbonEmitterEdit> &ribbonEmitter);
    std::shared_ptr<RibbonEmitterEdit> edit(Errors *errors, ModelEdit &model);
};
}

#endif // RIBBONEMITTER_H
