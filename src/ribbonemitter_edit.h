#ifndef RIBBONEMITTER_EDIT_H
#define RIBBONEMITTER_EDIT_H

#include "genericobject_edit.h"
#include "util.h"

namespace mdlx
{
/** Forward declare */
class MaterialEdit;

struct RibbonEmitterEdit : public GenericObjectEdit
{
    float       heightAbove = 0.0F, heightBelow = 0.0F, alpha = 0.0F;
    vec3        color{};
    float       lifeSpan = 0.0F;
    uint32_t    textureSlot = 0, emissionRate = 0, rows = 0, columns = 0;
    float       gravity = 0.0F;

    std::shared_ptr<MaterialEdit> material;

    RibbonEmitterEdit(const GenericObjectEdit &genericObject)
        : GenericObjectEdit(genericObject) {}
};
}

#endif // RIBBONEMITTER_EDIT_H
