#include "sequence.h"
#include "binarystream.h"
#include "tokenstream.h"

#include "sequence_edit.h"

namespace mdlx
{
Sequence::Sequence(BinaryStream &stream)
    : name(stream.readString(strSizeSmall)),
      interval(stream.read<uvec2>()),
      moveSpeed(stream.read<float>()),
      flags(stream.read<uint32_t>()),
      rarity(stream.read<float>()),
      syncPoint(stream.read<uint32_t>())
{
    extent.read(stream);
}

void Sequence::write(BinaryStream &stream) const
{
    stream.writeString(name);
    stream.skip(strSizeSmall - name.length());
    stream.write(interval);
    stream.write(moveSpeed);
    stream.write(flags);
    stream.write(rarity);
    stream.write(syncPoint);
    extent.write(stream);
}

Sequence::Sequence(TokenStream &stream)
    : name(stream.read<std::string>())
{
    for (std::string token; !(token = stream.iterBlock()).empty(); ) {
        if (token == "Interval") interval = stream.read<uvec2>();
        else if (token == "NonLooping") flags = NonLooping;
        else if (token == "MoveSpeed") moveSpeed = stream.read<float>();
        else if (token == "Rarity") rarity = stream.read<float>();
        else if (!extent.read(stream, token)) {
            stream.errors.fail(Errors::UnknownToken, "Sequence", token);
        }
    }
}

void Sequence::write(TokenStream &stream) const
{
    stream.startBlock("Anim", name);

    stream.writeAttr("Interval", interval);
    if (flags == NonLooping) stream.writeFlag("NonLooping");
    if (moveSpeed != 0) stream.writeAttr("MoveSpeed", moveSpeed);
    if (rarity != 0) stream.writeAttr("Rarity", rarity);
    extent.write(stream);

    stream.endBlock();
}

Sequence::Sequence(const std::shared_ptr<SequenceEdit> &sequence)
{
    name = sequence->name();
    interval = sequence->interval();
    moveSpeed = sequence->moveSpeed;
    flags = sequence->flags();
    rarity = sequence->rarity;
    syncPoint = sequence->syncPoint;
    extent = sequence->extent;
}

std::shared_ptr<SequenceEdit> Sequence::edit(Errors *errors, ModelEdit&)
{
    auto sequence = std::make_shared<SequenceEdit>(errors);

    sequence->name(name);
    sequence->interval(interval);
    sequence->moveSpeed = moveSpeed;
    sequence->flags(flags);
    sequence->rarity = rarity;
    sequence->syncPoint = syncPoint;
    sequence->extent = extent;

    return sequence;
}
}
