#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "extent.h"

#include <memory>

namespace mdlx
{
/** Forward declare */
class ModelEdit;
class SequenceEdit;
class Errors;

/**
 * @brief A sequence.
 */
struct Sequence
{
    static const size_t byteLength = 132;
    enum Flags {
        NonLooping = 1,
    };

    std::string name;
    uvec2       interval{};
    float       moveSpeed = 0.0F;
    uint32_t    flags = 0;
    float       rarity = 0.0F;
    uint32_t    syncPoint = 0;
    Extent      extent;

    Sequence(BinaryStream &stream);
    void write(BinaryStream &stream) const;

    Sequence(TokenStream &stream);
    void write(TokenStream &stream) const;

    Sequence(const std::shared_ptr<SequenceEdit> &sequence);
    std::shared_ptr<SequenceEdit> edit(Errors *errors, ModelEdit&);
};
}

#endif // SEQUENCE_H
