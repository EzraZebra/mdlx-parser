#include "sequence_edit.h"
#include "sequence.h"
#include "errors.h"

namespace mdlx
{
bool SequenceEdit::name(const std::string &name)
{
    if (!name.empty() && name.length() <= strSizeSmall) {
        name_ = name;
        return true;
    }

    errors->add(Errors::Value, "Sequence Name", name);
    return false;
}

bool SequenceEdit::interval(const uvec2 &interval)
{
    if (interval[0] < interval[1]) {
        interval_ = interval;
        return true;
    }

    errors->add(Errors::Value, "Sequence interval", std::to_string(interval[0]) + ", " + std::to_string(interval[1]));
    return false;
}

bool SequenceEdit::flags(uint32_t flags)
{
    if (flags == 0 || flags & Sequence::NonLooping) {
        flags_ = flags;
        return true;
    }

    errors->add(Errors::Value, "Sequence flags", std::to_string(flags));
    return false;
}
}
