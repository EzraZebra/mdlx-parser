#ifndef SEQUENCEEDIT_H
#define SEQUENCEEDIT_H

#include "util.h"
#include "extent.h"

namespace mdlx
{
/** Forward declare */
class Errors;

struct SequenceEdit
{
    std::string name() const { return name_; };
    bool        name(const std::string &name);
    uvec2       interval() const { return interval_; }
    bool        interval(const uvec2 &interval);
    uint32_t    flags() const { return flags_; }
    bool        flags(uint32_t flags);

    float       moveSpeed = 0.0F;
    float       rarity = 0.0F;
    uint32_t    syncPoint = 0;
    Extent      extent;

    SequenceEdit(Errors *errors)
        : errors(errors) {}

private:
    Errors *errors;

    std::string name_;
    uvec2       interval_{};
    uint32_t    flags_ = 0;
};
}

#endif // SEQUENCEEDIT_H
