#include "soundemitter.h"
#include "binarystream.h"
#include "tokenstream.h"

#include "model_edit.h"

namespace mdlx
{
SoundEmitter::SoundEmitter(BinaryStream &stream)
    : path(stream.readString(strSize)),
      volume(stream.read<float>()),
      pitch(stream.read<float>()),
      flags(stream.read<uint32_t>()) {}

void SoundEmitter::write(BinaryStream &stream) const
{
    stream.writeString(path);
    stream.skip(strSize - path.length());
    stream.write(volume);
    stream.write(pitch);
    stream.write(flags);
}

SoundEmitter::SoundEmitter(TokenStream &stream)
{
    // This is a guess
    for (std::string token; !(token = stream.iterBlock()).empty(); ) {
        if (token == "FileName") path = stream.read<std::string>();
        else if (token == "Volume") volume = stream.read<float>();
        else if (token == "Pitch") pitch = stream.read<float>();
        // I don't know what the flags are/were
        else {
            stream.errors.fail(Errors::UnknownToken, "SoundTrack", token);
        }
    }
}

void SoundEmitter::write(TokenStream &stream) const
{
    // This is a guess
    stream.startBlock("SoundTrack");

    stream.writeAttr("FileName", path);
    stream.writeAttr("Volume", volume);
    stream.writeAttr("Pitch", pitch);
    // I don't know what the flags are/were

    stream.endBlock();
}

SoundEmitter::SoundEmitter(const std::shared_ptr<SoundEmitterEdit> &soundEmitter)
{
    path = soundEmitter->path();
    volume = soundEmitter->volume;
    pitch = soundEmitter->pitch;
    flags = soundEmitter->flags;
}

std::shared_ptr<SoundEmitterEdit> SoundEmitter::edit(Errors *errors, ModelEdit&)
{
    auto soundEmitter = std::make_shared<SoundEmitterEdit>(errors);

    soundEmitter->path(path);
    soundEmitter->volume = volume;
    soundEmitter->pitch = pitch;
    soundEmitter->flags = flags;

    return soundEmitter;
}
}
