#ifndef SOUNDTRACK_H
#define SOUNDTRACK_H

#include <string>
#include <memory>

namespace mdlx
{
/** Forward declare */
class BinaryStream;
class TokenStream;
struct SoundEmitterEdit;
class Errors;
class ModelEdit;

/**
 * @brief A sound track.
 *
 * Note that this is here for completeness' sake.
 * These objects were only used at some point before Warcraft 3 released.
 */
class SoundEmitter
{
    std::string path;
    float       volume, pitch;
    uint32_t    flags = 0;

public:
    static const size_t byteLength = 272;

    SoundEmitter(BinaryStream &stream);
    void write(BinaryStream &stream) const;

    SoundEmitter(TokenStream &stream);
    void write(TokenStream &stream) const;

    SoundEmitter(const std::shared_ptr<SoundEmitterEdit> &texture);
    std::shared_ptr<SoundEmitterEdit> edit(Errors *errors, ModelEdit&);
};
}

#endif // SOUNDTRACK_H
