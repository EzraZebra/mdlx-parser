#include "soundemitter_edit.h"
#include "errors.h"
#include "util.h"

namespace mdlx
{
bool SoundEmitterEdit::path(const std::string &path)
{
    if (path.length() <= strSize) {
        path_ = path;
        return true;
    }

    errors->add(Errors::Value, "SoundEmitter Path", path);
    return false;
}
}
