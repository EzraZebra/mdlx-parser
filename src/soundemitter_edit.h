#ifndef SOUNDEMITTEREDIT_H
#define SOUNDEMITTEREDIT_H

#include <string>

namespace mdlx
{
/** Forward declare */
class Errors;

struct SoundEmitterEdit
{
    std::string path() const { return path_; }
    bool        path(const std::string &path);
    float       volume, pitch;
    uint32_t    flags = 0;

    SoundEmitterEdit(Errors *errors)
        : errors(errors) {}

private:
    Errors *errors;

    std::string path_;
};
}

#endif // SOUNDEMITTEREDIT_H
