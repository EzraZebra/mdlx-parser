#include "streambase.h"
#include "errors.h"

#include <fstream>

namespace mdlx
{
StreamBase::StreamBase(const std::string &path, Errors &errors)
    : errors(errors)
{
    std::ifstream file(path, std::ios_base::binary);

    if (file.is_open()) {
        file.seekg(0, std::ios::end);
        size_t size = static_cast<size_t>(file.tellg());
        file.seekg(0, std::ios::beg);
        buffer = std::vector<uint8_t>(size);
        file.read(reinterpret_cast<char*>(buffer.data()), size);
        file.close();

        if (buffer.empty()) {
            errors.fail(Errors::FileIO, "Failed to read file or file is empty.");
        }
    }
    else errors.fail(Errors::FileIO, "Failed to open file.");
}

StreamBase::StreamBase(size_t size, Errors &errors, uint32_t version, int options)
    : buffer(size),
      errors(errors),
      version(version),
      options(options) {}

StreamBase::StreamBase(Errors &errors, uint32_t version, int options)
    : buffer({}),
      errors(errors),
      version(version),
      options(options) {}

size_t StreamBase::position() const
{
    return position_;
}

size_t StreamBase::remaining() const
{
    return buffer.size() - position_;
}

void StreamBase::writeFile(const std::string &path)
{
    std::ofstream file(path, std::ios_base::binary);

    if (!file.is_open() || !file.good()) {
        errors.fail(Errors::FileIO, "Failed to write to file.");
    }

    file.write(reinterpret_cast<char*>(&buffer[0]), buffer.size());
    file.close();
}
}
