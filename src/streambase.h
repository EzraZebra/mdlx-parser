#ifndef STREAMBASE_H
#define STREAMBASE_H

#include <vector>
#include <string>

namespace mdlx
{
/** Forward declare */
class Errors;

class StreamBase
{
protected:
    std::vector<uint8_t>    buffer;
    size_t                  position_ = 0;

    StreamBase(const std::string &path, Errors &errors);
    StreamBase(size_t size, Errors &errors, uint32_t version, int options = 0);
    StreamBase(Errors &errors, uint32_t version, int options = 0);

public:
    Errors &errors;

    uint32_t    version = 0;
    int         options = 0;

    size_t position() const;
    size_t remaining() const;

    void writeFile(const std::string &path);
};
}

#endif // STREAMBASE_H
