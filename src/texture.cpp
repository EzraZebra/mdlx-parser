#include "texture.h"
#include "binarystream.h"
#include "tokenstream.h"

#include "texture_edit.h"

namespace mdlx
{
Texture::Texture(BinaryStream &stream)
    : replaceableId(stream.read<uint32_t>()),
      path(stream.readString(strSize)),
      flags(stream.read<uint32_t>()) {}

void Texture::write(BinaryStream &stream) const
{
    stream.write(replaceableId);
    stream.writeString(path);
    stream.skip(strSize - path.length());
    stream.write(flags);
}

Texture::Texture(TokenStream &stream)
{
    for (std::string token; !(token = stream.iterBlock()).empty(); ) {
        if (token == "Image") path = stream.read<std::string>();
        else if (token == "ReplaceableId") replaceableId = stream.read<uint32_t>();
        else if (token == "WrapWidth") flags |= Flags::WrapWidth;
        else if (token == "WrapHeight") flags |= Flags::WrapHeight;
        else stream.errors.fail(Errors::UnknownToken, "Texture", token);
    }
}

void Texture::write(TokenStream &stream) const
{
    stream.startBlock("Bitmap");

    if (!path.empty()) stream.writeAttr("Image", path);
    if (path.empty() || replaceableId != 0) stream.writeAttr("ReplaceableId", replaceableId);
    if (flags & Flags::WrapWidth) stream.writeFlag("WrapWidth");
    if (flags & Flags::WrapHeight) stream.writeFlag("WrapHeight");

    stream.endBlock();
}

Texture::Texture(const std::shared_ptr<TextureEdit> &texture)
{
    replaceableId = texture->replaceableId();
    path = texture->path();
    flags = texture->flags();
}

std::shared_ptr<TextureEdit> Texture::edit(uint32_t index, Errors *errors, ModelEdit&)
{
    auto texture = std::make_shared<TextureEdit>(index, errors);
    texture->replaceableId(replaceableId);
    texture->path(path);
    texture->flags(flags);

    return texture;
}
}
