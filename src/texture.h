#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <memory>

namespace mdlx
{
/** Forward declare */
class BinaryStream;
class TokenStream;
class ModelEdit;
class TextureEdit;
class Errors;

/**
 * @brief A texture.
 */
class Texture
{
    uint32_t    replaceableId = 0;
    std::string path;
    uint32_t    flags = 0;

public:
    static const size_t byteLength = 268;
    enum ReplaceableId {
        TeamColor       = 1,
        TeamGlow        = 2,
        Cliff           = 11,
        LordsTree       = 31,
        AshenTree       = 32,
        BarrensTree     = 33,
        NorthTree       = 34,
        MushroomTree    = 35,
        RuinsTree       = 36,
        OutlandTree     = 37,
    };
    enum Flags {
        WrapWidth   = 0x1,
        WrapHeight  = 0x2,
    };

    Texture(BinaryStream &stream);
    void write(BinaryStream &stream) const;

    Texture(TokenStream &stream);
    void write(TokenStream &stream) const;

    Texture(const std::shared_ptr<TextureEdit> &texture);
    std::shared_ptr<TextureEdit> edit(uint32_t index, Errors *errors, ModelEdit&);
};
}

#endif // TEXTURE_H
