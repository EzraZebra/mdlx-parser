#include "texture_edit.h"
#include "texture.h"
#include "errors.h"
#include "util.h"

namespace mdlx
{
bool TextureEdit::operator==(const TextureEdit &rhs) const
{
    return replaceableId_ == rhs.replaceableId() && path_ == rhs.path() && flags_ == rhs.flags();
}

bool TextureEdit::validReplaceableId(uint32_t replaceableId)
{
    return replaceableId <= Texture::TeamGlow || replaceableId == Texture::Cliff
            || (replaceableId >= Texture::LordsTree && replaceableId <= Texture::OutlandTree);
}

bool TextureEdit::replaceableId(uint32_t replaceableId)
{
    if (validReplaceableId(replaceableId)) {
        replaceableId_ = replaceableId;
        return true;
    }

    errors->add(Errors::Value, "Texture ReplaceableId", std::to_string(replaceableId));
    return false;
}

bool TextureEdit::path(const std::string &path)
{
    if (path.length() <= strSize) {
        path_ = path;
        return true;
    }

    errors->add(Errors::Value, "Texture Path", path);
    return false;
}

bool TextureEdit::flags(uint32_t flags)
{
    if (flags == 0 || flags & Texture::WrapWidth || flags & Texture::WrapHeight) {
        flags_ = flags;
        return true;
    }

    errors->add(Errors::Value, "Texture Flags", std::to_string(flags));
    return false;
}
}
