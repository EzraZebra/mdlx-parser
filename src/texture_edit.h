#ifndef TEXTUREEDIT_H
#define TEXTUREEDIT_H

#include <string>

namespace mdlx
{
/** Forward declare */
class Errors;

struct TextureEdit
{
    uint32_t    index;

    static bool validReplaceableId(uint32_t replaceableId);
    uint32_t    replaceableId() const { return replaceableId_; }
    bool        replaceableId(uint32_t replaceableId);
    std::string path() const { return path_; }
    bool        path(const std::string &path);
    uint32_t    flags() const { return flags_; }
    bool        flags(uint32_t flags);

    TextureEdit(uint32_t index, Errors *errors)
        : index(index), errors(errors) {}

    bool operator==(const TextureEdit &rhs) const;
    bool operator!=(const TextureEdit &rhs) const { return !operator==(rhs); }

private:
    Errors *errors;

    uint32_t    replaceableId_ = 0;
    std::string path_;
    uint32_t    flags_ = 0;
};
}

#endif // TEXTUREEDIT_H
