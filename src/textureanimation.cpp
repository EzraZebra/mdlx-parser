#include "textureanimation.h"
#include "binarystream.h"
#include "tokenstream.h"
#include "animationmap.h"

#include "model_edit.h"

namespace mdlx
{
TextureAnimation::TextureAnimation(BinaryStream &stream)
    : AnimatedObject(stream, stream.read<uint32_t>()-4) {}

void TextureAnimation::write(BinaryStream &stream) const
{
    stream.write<uint32_t>(getByteLength());
    writeAnimations(stream);
}

size_t TextureAnimation::getByteLength(uint32_t) const
{
    return 4 + AnimatedObject::getByteLength();
}

TextureAnimation::TextureAnimation(TokenStream &stream)
{
    for (std::string token; !(token = stream.iterBlock()).empty(); ) {
        if (!readAnimation(stream, "TextureAnimation", token)) {
            stream.errors.fail(Errors::UnknownToken, "TextureAnimation", token);
        }
    }
}

void TextureAnimation::write(TokenStream &stream) const
{
    stream.startBlock("TVertexAnim");
    writeAnimation(stream, KTAT);
    writeAnimation(stream, KTAR);
    writeAnimation(stream, KTAS);
    stream.endBlock();
}

TextureAnimation::TextureAnimation(const std::shared_ptr<TextureAnimationEdit> &textureAnimation)
    : AnimatedObject(textureAnimation) {}

std::shared_ptr<TextureAnimationEdit> TextureAnimation::edit(uint32_t index, Errors *errors, ModelEdit &model)
{
    return std::make_shared<TextureAnimationEdit>(index, errors, model, this);
}
}
