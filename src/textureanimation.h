#ifndef TEXTUREANIMATION_H
#define TEXTUREANIMATION_H

#include "animatedobject.h"

namespace mdlx
{
/** Forward declare */
class ModelEdit;
class TextureAnimationEdit;

/**
 * @brief A texture animation.
 */
class TextureAnimation : public AnimatedObject
{
public:
    TextureAnimation(BinaryStream &stream);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;

    TextureAnimation(TokenStream &stream);
    void write(TokenStream &stream) const;

    TextureAnimation(const std::shared_ptr<TextureAnimationEdit> &textureAnimation);
    std::shared_ptr<TextureAnimationEdit> edit(uint32_t index,  Errors* errors, ModelEdit &model);
};
}

#endif // TEXTUREANIMATION_H
