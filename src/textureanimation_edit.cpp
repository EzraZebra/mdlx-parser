#include "textureanimation_edit.h"

namespace mdlx
{
bool TextureAnimationEdit::operator==(const TextureAnimationEdit &rhs) const
{
    return animations == rhs.animations;
}
}
