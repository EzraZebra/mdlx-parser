#ifndef TEXTUREANIMATIONEDIT_H
#define TEXTUREANIMATIONEDIT_H

#include "animatedobject_edit.h"

namespace mdlx
{
struct TextureAnimationEdit : public AnimatedObjectEdit
{
    uint32_t index;

    TextureAnimationEdit(uint32_t index, Errors *errors, ModelEdit &model, AnimatedObject *animatedObject)
        : AnimatedObjectEdit(errors, model, animatedObject),
          index(index) {}

    bool operator==(const TextureAnimationEdit &rhs) const;
    bool operator!=(const TextureAnimationEdit &rhs) const { return !operator==(rhs); }
};
}

#endif // TEXTUREANIMATIONEDIT_H
