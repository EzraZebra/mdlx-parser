#include "tokenstream.h"

namespace mdlx
{
TokenStream::TokenStream(const std::string &path, Errors &errors)
    : StreamBase(path, errors) {}

std::string TokenStream::readToken(bool safe)
{
    bool inComment = false, inString = false;

    for (std::string token; position_ < buffer.size(); )
    {
        const char c = buffer[position_++];

        if (inComment) inComment = c != '\n';
        else if (inString) {
            switch(c) {
            case '"': return token;
                break;
            default: token += c;
                break;
            }
        }
        else {
            switch(c) {
            case ' ': case ',': case '\t': case '\n': case ':': case '\r':
                if (!token.empty()) return token;
                break;
            case '{': case '}':
                if (!token.empty()) {
                    position_--;
                    return token;
                }
                else return { c };
                break;
            case '/':
                if(buffer[position_] == '/') {
                    if (!token.empty()) {
                        position_--;
                        return token;
                    }

                    inComment = true;
                }
                else token += c;
                break;
            case '"':
                if (!token.empty()) {
                    position_--;
                    return token;
                }
                else inString = true;
                break;
            default:
                token += c;
                break;
            }
        }
    }

    if (safe) errors.fail(Errors::Stream, "End of token stream reached prematurely.");
    return {};
}

std::string TokenStream::peekToken()
{
    size_t start = position_;
    std::string token = readToken();
    position_ = start;
    return token;
}

vec3 TokenStream::readColor()
{
    readToken(); // {

    vec3 color;
    color[2] = read<float>();
    color[1] = read<float>();
    color[0] = read<float>();

    readToken(); // }

    return color;
}

std::string TokenStream::iterBlock()
{
    std::string token = readToken();
    if (token == "{") token = readToken();
    if (token == "}") return {};
    return token;
}

TokenStream::TokenStream(Errors &errors, uint32_t version, int options)
    : StreamBase(errors, version, options) {}

void TokenStream::writeLine(std::string line)
{
    line.insert(0, ident, '\t');
    line.append("\n");

    std::copy(line.begin(), line.end(), std::back_inserter(buffer));
}

void TokenStream::indent(bool incr)
{
    if (incr) ident++;
    else ident--;
}

void TokenStream::startBlock(std::string header, size_t size, size_t size2)
{
    if (size != 0) {
        header.append(" " + std::to_string(size));
        if (size2 != 0) header.append(" " + std::to_string(size2));
    }

    writeLine(header + " {");
    ident++;
}

void TokenStream::startBlock(const std::string &header, const std::string &name)
{
    startBlock(header + " \"" + name + "\"");
}

void TokenStream::endBlock(bool comma)
{
    ident--;
    if (comma) writeLine("},");
    else writeLine("}");
}

void TokenStream::writeFlag(std::string flag, const std::string &val)
{
    if (!val.empty()) flag.append(" " + val);
    writeLine(flag + ",");
}

void TokenStream::writeColor(const std::string &name, const vec3 &color)
{
    writeAttr(name, vec3{ color[2], color[1], color[0] });
}
}
