#ifndef TOKENSTREAM_H
#define TOKENSTREAM_H

#include "util.h"
#include "streambase.h"

namespace mdlx
{
class TokenStream : public StreamBase
{
    size_t ident = 0;

    void writeLine(std::string line);

    /** @brief Integral, float or string to string. */
    template<typename T>
    static std::string to_string(const T &val);
    /** @brief Join container. */
    template<typename T>
    std::string join(const T &cont, const std::string &join = ", ");

public:
    TokenStream(const std::string &path, Errors &errors);

    std::string readToken(bool safe=true);
    std::string peekToken();
    vec3 readColor();
    std::string iterBlock();

    template<typename T>
    T read();
    template<typename T>
    void read(T &val);
    template<typename T>
    std::vector<T> readVector(const size_t size);
    template<typename T>
    std::vector<T> readVectorMulti(const size_t size);
    template<typename T>
    void readVectorBlind(std::vector<T> &vect);

    TokenStream(Errors &errors, uint32_t version, int options = 0);

    void indent(bool incr=true);
    void startBlock(std::string header, size_t size=0, size_t size2=0);
    void startBlock (const std::string &header, const std::string &name);
    void endBlock(bool comma=false);

    void writeFlag(std::string flag, const std::string &val = {});
    void writeColor(const std::string &name, const vec3 &color);

    template<typename T>
    void writeAttr(const std::string &name, const T &attr);
    template<typename T>
    void writeValue(const T &val);
    template<typename T>
    void writeVectorBlock(const std::string &name, const std::vector<T> &vect, bool writeSize = true);
    template<typename T>
    void writeVectorBlockMulti(const std::string &name, const std::vector<T> &vect);
};
}

#include "tokenstream.tcc"

#endif // TOKENSTREAM_H
