#ifndef TOKENSTREAM_TCC
#define TOKENSTREAM_TCC

#include "errors.h"
#include "tokenstream.h"

#include <sstream>

namespace mdlx
{
template<typename T>
T TokenStream::read()
{
    if constexpr (std::is_same<T, uint8_t>() || std::is_same<T, uint16_t>() || std::is_same<T, uint32_t>() || std::is_same<T, int32_t>()) {
        return static_cast<T>(std::stoi(readToken()));
    }
    else if constexpr (std::is_same<T, float>()) {
        return std::stof(readToken());
    }
    else if constexpr (std::is_same<T, std::string>()) {
        return readToken();
    }
    else {
        constexpr bool isUint8 = std::is_same<T, u8vec3>() || std::is_same<T, u8vec8>();
        constexpr bool isUint32 = std::is_same<T, uvec2>() || std::is_same<T, uvec3>();
        constexpr bool isFloat = std::is_same<T, vec2>() || std::is_same<T, vec3>() || std::is_same<T, vec4>() || std::is_same<T, vec12>();

        if constexpr (isUint8 || isUint32 || isFloat) {
            readToken(); // {

            T arr;
            for (auto &val : arr) {
                if constexpr (isUint8) val = read<uint8_t>();
                else if constexpr (isUint32) val = read<uint32_t>();
                else if constexpr (isFloat) val = read<float>();
            }

            readToken(); // }

            return arr;
        }
        else errors.fail(Errors::Type, "TokenStream::read()");
    }
}

template<typename T>
void TokenStream::read(T &val)
{
    val = read<T>();
}

template<typename T>
std::vector<T> TokenStream::readVector(const size_t size)
{
    readToken(); // {

    std::vector<T> vect(size);

    for (size_t i=0; i<size; i++) {
        vect[i] = read<T>();
    }

    readToken(); // }

    return vect;
}

template<typename T>
std::vector<T> TokenStream::readVectorMulti(const size_t size)
{
    readToken(); // {

    std::vector<T> vect(size);

    for (size_t i=0; i<size; i++) {
        for (auto &val : vect[i]) {
            read(val);
        }
    }

    readToken(); // }

    return vect;
}

template<typename T>
void TokenStream::readVectorBlind(std::vector<T> &vect)
{
    readToken(); // {

    while (peekToken() != "}") {
        vect.emplace_back(read<T>());
    }

    readToken(); // }
}

template<typename T>
std::string TokenStream::to_string(const T &val)
{
    if constexpr (std::is_integral<T>()) {
        return std::to_string(val);
    }
    else if constexpr (std::is_same<T, float>()) {
        std::ostringstream ss;
        ss << std::defaultfloat << val;
        return ss.str();
    }
    else if constexpr (std::is_same<T, std::string>()) {
        return val;
    }

    return {};
}

template<typename T>
std::string TokenStream::join(const T &cont, const std::string &join)
{
    std::string str;

    for (auto it = cont.cbegin(); it != cont.cend(); it++) {
        std::string itStr = to_string(*it);

        if (!itStr.empty()) str.append(itStr);
        else errors.fail(Errors::Type, "TokenStream::join()");

        if (it+1 != cont.cend()) str.append(join);
    }

    return str;
}

template<typename T>
void TokenStream::writeAttr(const std::string &name, const T &attr)
{
    std::string line;
    if (!name.empty()) line = name + " ";

    if constexpr (std::is_same<T, std::string>()) {
        line.append("\"" + attr + "\"");
    }
    else {
        std::string attrStr = to_string(attr);

        if (!attrStr.empty()) {
            line.append(attrStr);
        }
        else if constexpr (std::is_same<T, u8vec3>() || std::is_same<T, u8vec8>()
                || std::is_same<T, uvec2>() || std::is_same<T, uvec3>()
                || std::is_same<T, vec2>() || std::is_same<T, vec3>()
                || std::is_same<T, vec4>() || std::is_same<T, vec12>()
                || std::is_same<T, std::vector<uint32_t>>()
                || std::is_same<T, std::vector<uint16_t>>())
        {
            line.append("{ " + join(attr) + " }");
        }
        else errors.fail(Errors::Type, "TokenStream::writeAttr(" + name + ")");
    }

    writeLine(line + ",");
}

template<typename T>
void TokenStream::writeValue(const T &val)
{
    writeAttr<T>({}, val);
}

template<typename T>
void TokenStream::writeVectorBlock(const std::string &name, const std::vector<T> &vect, bool writeSize)
{
    if (writeSize) startBlock(name, vect.size());
    else startBlock(name);
    for (const T &val : vect) writeValue<T>(val);
    endBlock();
}

template<typename T>
void TokenStream::writeVectorBlockMulti(const std::string &name, const std::vector<T> &vect)
{
    startBlock(name, vect.size());
    for (const T &arr : vect) {
        writeLine(join(arr) + ",");
    }
    endBlock();
}
}

#endif // TOKENSTREAM_TCC
