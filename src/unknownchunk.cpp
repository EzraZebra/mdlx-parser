#include "unknownchunk.h"
#include "binarystream.h"

namespace mdlx
{
UnknownChunk::UnknownChunk(BinaryStream &stream, size_t size, uint32_t tag)
    : tag(tag),
      chunk(stream.readVector<uint8_t>(size)) {}

void UnknownChunk::write(BinaryStream &stream) const
{
    stream.write(tag);
    stream.write<uint32_t>(chunk.size());
    stream.writeVector(chunk);
}

size_t UnknownChunk::getByteLength(uint32_t) const
{
    return 8 + vectorByteLength(chunk);
}
}
