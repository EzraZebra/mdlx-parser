#ifndef UNKNOWNCHUNK_H
#define UNKNOWNCHUNK_H

#include <vector>
#include <cstdint>

namespace mdlx
{
/** Forward declare */
class BinaryStream;

/**
 * @brief An unknown chunk.
 */
class UnknownChunk
{
    uint32_t                tag;
    std::vector<uint8_t>    chunk;

public:
    UnknownChunk(BinaryStream &stream, size_t size, uint32_t tag);
    void write(BinaryStream &stream) const;
    size_t getByteLength(uint32_t=0) const;
};
}

#endif // UNKNOWNCHUNK_H
