#include "util.h"

#include <cmath>
#include <algorithm>

namespace mdlx
{
bool compf(float x, float y)
{
    return std::fabs(x-y) <= std::numeric_limits<float>::epsilon()*std::max({ 1.0F, std::fabs(x), std::fabs(y) });
}
}
