#ifndef UTIL_H
#define UTIL_H

#include <array>
#include <vector>

namespace mdlx
{
/** Aliases for arrays */
using u8vec3 = std::array<uint8_t, 3>;
using u8vec4 = std::array<uint8_t, 4>;
using u8vec8 = std::array<uint8_t, 8>;
using uvec2 = std::array<uint32_t, 2>;
using uvec3 = std::array<uint32_t, 3>;
using umat2x3 = std::array<uvec3, 2>;

using vec2 = std::array<float, 2>;
using vec3 = std::array<float, 3>;
using vec4 = std::array<float, 4>;
using vec12 = std::array<float, 12>;
using mat2x3 = std::array<vec3, 2>;
using mat3 = std::array<vec3, 3>;
using mat2x4 = std::array<vec4, 2>;

/** Constant */
static const size_t strSize = 260;
static const size_t strSizeSmall = 80;
static const uint32_t v800 = 800;
static const uint32_t v900 = 900;
static const uint32_t v1000 = 1000;

extern bool compf(float x, float y);
template<size_t S>
extern bool compf(const std::array<float, S> &x, const std::array<float, S> &y);
template<typename T>
extern bool compf(const std::vector<T> &x, const std::vector<T> &y);

/** @brief Get bytelength of integral, float, or std::array. */
template<typename T>
extern size_t byteLength(const T &val = T{});
/** @brief Get bytelength of std::array. */
template<typename T, std::size_t S>
extern size_t arrayByteLength(const std::array<T, S>&);
/** @brief Get bytelength of std::vector. */
template<typename T>
extern size_t vectorByteLength(const std::vector<T> &vect);
}

#include "util.tcc"

#endif // UTIL_H
