#ifndef UTIL_TCC
#define UTIL_TCC

#include "util.h"

namespace mdlx
{
template<size_t S>
bool compf(const std::array<float, S> &x, const std::array<float, S> &y)
{
    for (size_t i=0; i<S; i++) {
        if (!compf(x[i], y[i])) return false;
    }

    return true;
}

template<typename T>
bool compf(const std::vector<T> &x, const std::vector<T> &y)
{
    if (x.size() != y.size()) return false;

    for (size_t i=0; i<x.size(); i++) {
        if (!compf(x[i], y[i])) return false;
    }

    return true;
}

template<typename T>
size_t byteLength(const T &val)
{
    if constexpr (std::is_integral<T>() || std::is_same<T, float>()) {
        return sizeof(T);
    }
    else return arrayByteLength(val);
}

template<typename T, size_t S>
size_t arrayByteLength(const std::array<T, S>&)
{
    return sizeof(T) * S;
}

template<typename T>
size_t vectorByteLength(const std::vector<T> &vect)
{
    return byteLength<T>() * vect.size();
}
}

#endif // UTIL_TCC
